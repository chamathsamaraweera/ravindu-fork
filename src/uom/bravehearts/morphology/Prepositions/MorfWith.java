package uom.bravehearts.morphology.Prepositions;

import java.util.ArrayList;
import java.util.List;

import uom.bravehearts.grammer.IsAnimate;
import uom.bravehearts.morphology.MorfDeterminant;
import uom.bravehearts.morphology.MorfRule;

public class MorfWith implements MorfRule {

	IsAnimate animate = new IsAnimate();
	int count;
	int countNxt;
	ArrayList<Character> verbHandler = new ArrayList<Character>();

	public List performRule(List<String> phrase) {
		System.out.println("With class executed...");
		System.out.println("With class input: " + phrase);

		for (int x = 0; x < phrase.size(); x++) {
			if ((phrase.get(x).equals("NP"))
					&& (phrase.get(x + 1).equals("NP"))) {
				phrase.remove(x + 1);

			}
		}

		for (int x = 0; x <= phrase.size() - 1; x++) {// System.err.println("+++++++++++++++++++++++++++++"+x);

			if (phrase.get(x).equals("with")) {
				for (int y = x; y <= phrase.size() - 1; y++) {
					if (((phrase.get(y).equals("NP"))
							|| (phrase.get(y).equals("VP"))
							|| (phrase.get(y).equals("ADVP")) || (phrase.get(y)
							.equals("PP")))) {
						count++;
						if ((count == 2)
								&& ((phrase.get(y).equals("NP"))
										|| (phrase.get(y).equals("VP"))
										|| (phrase.get(y).equals("ADVP")) || (phrase
											.get(y).equals("PP"))))

						{

							count++;

							String befChange = (phrase.get(y - 1));
							String change = (phrase.get(y - 1)).trim();
							char type = animate.perform(change);
							

							if ((phrase.get(y - 2).equals("PRP"))) {// 	IDENTIFY PERSONAL PRONOUN
								// I WENT WITH HIM (with pro nouns)
								phrase.add((y), "සමඟ");
							}

							else if ((type == 'f') || (type == 'm')) {// IF IS IS FEMININE OR MASCULINE
								// I WENT WITH MY SISTER
								phrase.add((y), "සමඟ");
							}

							else if ((type == 'i')
									&& (phrase.get(y - 2).equals("NN"))
									|| (phrase.get(y - 2).equals("PRP"))) {
								// i ate rice with curries
								countNxt = 0;
								System.out.println("(phrase.get(y-1))"
										+ (phrase.get(y - 1)));
								for (int rev = x; rev >= 0; rev--) {
									if ((phrase.get(rev).equals("PP"))) {
										if (((countNxt == 0)
												&& ((phrase.get(rev - 2)
														.equals("NN"))) || ((countNxt == 0) && ((phrase
												.get(rev - 1)
												.equals("sbjBoundry")) && ((phrase
												.get(rev - 3).equals("NN"))))))) {
											countNxt++;
											phrase.add((y), "සමඟ");
											break;
										}

										else {

											for (int z = 0; z <= change
													.length() - 1; z++)

											{// I WROTE WITH PEN
												verbHandler.add(change
														.charAt(z));
											}

											for (int z = 0; z <= change
													.length() - 1; z++) {
												if ((z == verbHandler.size() - 1)
														&& (verbHandler.get(z) == '්')) {
													verbHandler.set(z, 'ි');
													verbHandler.add(z + 1, 'න');
													verbHandler.set(z + 1, '්');
													System.out.println("z" + z);
													String WordToPrint = verbHandler
															.toString()
															.replaceAll(
																	", |\\[|\\]",
																	"");
													for (int index = 0; index <= phrase
															.size() - 1; index++) {
														if (phrase
																.get(index)
																.equals(befChange)) {
															phrase.set(index,
																	WordToPrint);
														}
													}
													break;
												}

												else if (z == verbHandler
														.size() - 1) {
													verbHandler.add(z + 1, '්');
													verbHandler.add(z + 1, 'න');
													verbHandler.add(z + 1, 'ි');
													verbHandler.add(z + 1, 'ක');
													System.out.println("z" + z);
													String WordToPrint = verbHandler
															.toString()
															.replaceAll(
																	", |\\[|\\]",
																	"");
													for (int index = 0; index <= phrase
															.size() - 1; index++) {
														if (phrase
																.get(index)
																.equals(befChange)) {
															phrase.set(index,
																	WordToPrint);
														}

													}

												}
												break;
											}
										}
									}
								}
							}

							else if ((type == 'i')
									&& (phrase.get(y - 2).equals("NNS"))) {
								// System.out.println("(phrase.get(y-1))+NNS"+(phrase.get(y-1)));
								for (int rev = x; rev >= 0; rev--) {
									if ((phrase.get(rev).equals("PP"))) {
										if ((countNxt == 0)
												&& ((phrase.get(rev - 2)
														.equals("NNS")))) {
											count++;
											phrase.add((y), "සමඟ");
											break;
										}

										else {
											// I WROTE WITH PENS
											for (int z = 0; z <= change
													.length() - 1; z++)

											{
												verbHandler.add(change
														.charAt(z));
											}

											for (int z = 0; z <= change
													.length() - 1; z++)

											{

												if (z == verbHandler.size() - 1) {
													if ((verbHandler.get(z)
															.equals('ය'))
															|| (verbHandler
																	.get(z)
																	.equals('ව'))) {
														{
															verbHandler
																	.remove(z);
														}
														verbHandler.add(z + 1,
																'්');
														verbHandler.add(z + 1,
																'න');
														verbHandler.add(z + 1,
																'ි');
														verbHandler.add(z + 1,
																'ල');
														verbHandler.add(z + 1,
																'ව');

														System.out.println("");
														String WordToPrint = verbHandler
																.toString()
																.replaceAll(
																		", |\\[|\\]",
																		"");
														for (int index = 0; index <= phrase
																.size() - 1; index++) {
															if (phrase
																	.get(index)
																	.equals(befChange)) {
																phrase.set(
																		index,
																		WordToPrint);
															}

														}
													}

													else if (!(verbHandler
															.get(z).equals('්'))
															|| (verbHandler
																	.get(z)
																	.equals('ා'))
															|| (verbHandler
																	.get(z)
																	.equals('ැ'))
															|| (verbHandler
																	.get(z)
																	.equals('ෑ'))
															|| (verbHandler
																	.get(z)
																	.equals('ි'))
															|| (verbHandler
																	.get(z)
																	.equals('ී'))
															|| (verbHandler
																	.get(z)
																	.equals('ු'))
															|| (verbHandler
																	.get(z)
																	.equals('ූ')))

													{
														verbHandler.add(z + 1,
																'්');
														verbHandler.add(z + 1,
																'න');
														verbHandler.add(z + 1,
																'ි');
														verbHandler.add(z + 1,
																'ල');
														verbHandler.add(z + 1,
																'ව');
														verbHandler.add(z + 1,
																'්');
														System.out.println("");
														String WordToPrint = verbHandler
																.toString()
																.replaceAll(
																		", |\\[|\\]",
																		"");
														for (int index = 0; index <= phrase
																.size() - 1; index++) {
															if (phrase
																	.get(index)
																	.equals(befChange)) {
																phrase.set(
																		index,
																		WordToPrint);
															}

														}

													}

													else {
														verbHandler.add(z + 1,
																'්');
														verbHandler.add(z + 1,
																'න');
														verbHandler.add(z + 1,
																'ි');
														verbHandler.add(z + 1,
																'ල');
														verbHandler.add(z + 1,
																'ව');

														System.out.println("");
														String WordToPrint = verbHandler
																.toString()
																.replaceAll(
																		", |\\[|\\]",
																		"");
														for (int index = 0; index <= phrase
																.size() - 1; index++) {
															if (phrase
																	.get(index)
																	.equals(befChange)) {
																phrase.set(
																		index,
																		WordToPrint);
															}

														}

													}
												}

											}
											break;
										}
									}
								}

							}
						}
					}
				}

			}
		}
		System.out.println("With class output: " + phrase);
		return phrase;
	}
}
