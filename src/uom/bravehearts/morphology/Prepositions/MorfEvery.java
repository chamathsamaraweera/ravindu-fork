package uom.bravehearts.morphology.Prepositions;

import java.util.ArrayList;
import java.util.List;

import uom.bravehearts.grammer.IsAnimate;
import uom.bravehearts.morphology.MorfRule;

public class MorfEvery implements MorfRule {

	public List performRule(List<String> phrase) {
		IsAnimate chkAnimate=new IsAnimate();
		ArrayList<Character> verbHandler=new ArrayList<Character>();
		System.err.println("Every class executed...");
		System.out.println("Every class input: " + phrase);

		for (int x = 0; x < phrase.size(); x++) {
			if ((phrase.get(x).equals("NP"))
					&& (phrase.get(x + 1).equals("NP"))) {
				phrase.remove(x + 1);

			}
		}

	
		int countSec = 0;

		for (int x = 0; x < phrase.size(); x++) {

			if ((phrase.get(x).equals("every"))) {
				
				
				for (int sec = x; sec <= phrase.size() - 1; sec++) {
					if ((phrase.get(sec).equals("NP"))
							|| (phrase.get(sec).equals("VP"))
							|| (phrase.get(sec).equals("sbjBoundry"))
							|| (phrase.get(sec).equals("NP-TMP"))
							|| (phrase.get(sec).equals("PP"))
							|| (phrase.get(sec).equals("ADVP"))
							|| (sec == phrase.size() - 1) && (countSec == 0)) {

						countSec++;

						if (sec == phrase.size() - 1)
						{
							System.err.println("here here...");
							
							String evry1 = (phrase.get(sec));
							String evry = (phrase.get(sec).trim());
							char type=chkAnimate.perform(evry);
							
							for(int z=0; z<evry.length(); z++)
 							{
 							verbHandler.add(evry.charAt(z));
 							}
							
							if(type=='f')
								{
								
								for(int y=0;y<=evry.length()-1;y++)
									{
									if((y==verbHandler.size()-1)&&(verbHandler.get(y)!='්')&&(verbHandler.get(y-1)!='ක'))
		 							{
										phrase.set(x, ("සෑම"));
										phrase.set(sec, ((evry + "ක්ම").trim()));
										break;
		 							}
									}
								}
							
							if(type=='m')
								{
							
								for(int y=0;y<=evry.length()-1;y++)
									{
									if((y==verbHandler.size()-1)&&(verbHandler.get(y)!='ම'))
									{
										phrase.set(x, ("සෑම"));
										
										verbHandler.set(y, 'ෙ');
 										verbHandler.add(y,'ක');
 										verbHandler.add(y,'්');
 										verbHandler.add(y,'ම');
 									
 										System.out.println("ttttttttttttttttttttttt");
 										String  WordToPrint = verbHandler.toString().replaceAll(", |\\[|\\]", "");
 										
 								
 										for(int index=0;index<=phrase.size()-1;index++ )
 											{
 												if(phrase.get(index).equals(evry1))
 												{
 													phrase.set(index, WordToPrint);
 													
 												}
 											}
										
										
									}
									}
								}
							
							if(type=='i')
							{
						
							for(int y=0;y<=evry.length()-1;y++)
								{
								if((y==verbHandler.size()-1)&&(verbHandler.get(y)!='ම'))
								{
									phrase.set(x, ("සෑම"));
									phrase.set(sec, ((evry + "කම").trim()));
									break;
								}
								}
							}
							
						}
						
							
							
							
							
							
							

						else if (sec != phrase.size() - 1)
						{
							System.err.println(" here...");
							String evry1 = (phrase.get(sec - 1));
							String evry = (phrase.get(sec - 1).trim());
							char type=chkAnimate.perform(evry);
							
							for(int z=0; z<evry.length(); z++)
 							{
 							verbHandler.add(evry.charAt(z));
 							}
							
							if(type=='f')
								{
								
								for(int y=0;y<=evry.length()-1;y++)
									{
									if((y==verbHandler.size()-1)&&(verbHandler.get(y)!='්')&&(verbHandler.get(y-1)!='ක'))
		 							{
										phrase.set(x, ("සෑම"));
										phrase.set(sec-1, ((evry + "ක්ම").trim()));
										break;
		 							}
									}
								}
							
							if(type=='m')
								{
							
								for(int y=0;y<=evry.length()-1;y++)
									{
									if((y==verbHandler.size()-1)&&(verbHandler.get(y)!='ම'))
									{
										phrase.set(x, ("සෑම"));
										
										verbHandler.set(y, 'ෙ');
 										verbHandler.add(y,'ක');
 										verbHandler.add(y,'්');
 										verbHandler.add(y,'ම');
 									
 										System.out.println("");
 										String  WordToPrint = verbHandler.toString().replaceAll(", |\\[|\\]", "");
 										
 								
 										for(int index=0;index<=phrase.size()-1;index++ )
 											{
 												if(phrase.get(index).equals(evry1))
 												{
 													phrase.set(index, WordToPrint);
 													
 												}
 											}
										
										
									}
									}
								}
							
							if(type=='i')
							{
						
							for(int y=0;y<=evry.length()-1;y++)
								{
								if((y==verbHandler.size()-1)&&(verbHandler.get(y)!='ම'))
								{
									phrase.set(x, ("සෑම"));
									phrase.set(sec-1, ((evry + "කම").trim()));
									break;
								}
								}
							}
							
							
						}
					}

				}break;
			}
		}

		// return phrase;
		System.out.println("Every class output: " + phrase);

		return phrase;
	}
}
