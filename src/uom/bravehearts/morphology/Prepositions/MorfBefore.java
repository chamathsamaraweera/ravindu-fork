package uom.bravehearts.morphology.Prepositions;


import java.util.ArrayList;
import java.util.List;

import uom.bravehearts.morphology.MorfRule;

public class MorfBefore implements MorfRule {
	
public List performRule (List<String> phrase) {
		
		System.out.println("MorfBefore class executed...");
		System.out.println("MorfBefore class input: "+phrase);
		
		int listcount =0;
		String ya = null;
		String waSitiya = null;
		int phraseSize = phrase.size();
		for(int x=0;x<phraseSize;x++)
		  {
			  if((phrase.get(x).equals("NP"))&&(phrase.get(x+1).equals("NP")))
	 			{
	 				phrase.remove(x+1);	
	 				--phraseSize;
	 			}
			  if(phrase.get(x).equals("ය"))
	 			{
				  ya = phrase.get(x).toString();
				  phrase.remove(x);	
				  --phraseSize;
	 			}
			  if(phrase.get(x).equals("ව සිටියා"))
	 			{
				  waSitiya = phrase.get(x).toString();
				  phrase.remove(x);	
				  --phraseSize;
	 			}
		  }
		System.out.println(" input After removing : "+phrase);
		
		for(int x=0;x<phraseSize;x++){
			
			if(phrase.get(x).equalsIgnoreCase("before")){
				if(phrase.get(x+1).equals("NP")){
					if(phrase.get(x+2).equals("VBG")||phrase.get(x+2).equals("CD")||phrase.get(x+2).equals("PRP")||phrase.get(x+2).equals("NN")||phrase.get(x+2).equals("DT")){
						System.out.println("(x+2)th element "+phrase.get(x+2));
						if(phrase.get(x+2).equals("VBG")){
							System.out.println("in VBG loop");
							String change = phrase.get(x+3);
							System.out.println("change  "+change);
							
						   	change=change.trim();
							ArrayList<Character> chngeArray = new ArrayList<Character>();
							int a = change.length() - 1;
							for (int y = 0; y <= a ; y++) 
								{
									chngeArray.add(change.charAt(y));
								}
							int b = chngeArray.size() - 1;
							for (int y = 0; y <= b; y++) 
							{
								if ((y == b)&& (chngeArray.get(y).equals('ා'))&& (chngeArray.get(y-1).equals('ව'))&& (chngeArray.get(y-2).equals('න')))
								{//කනවා - කන්නට
									chngeArray.set(y, 'ට');
									chngeArray.set(y- 1, 'න');
									chngeArray.add(y-1, '්');
									
									System.out.println("");
									String wordToPrint = chngeArray.toString().replaceAll(", |\\[|\\]", "");
									System.out.println("wordToPrint "+wordToPrint);
									phrase.set(x+2, wordToPrint); // Replace before tag with noun
									phrase.set(x+3, "පෙර");
									break;
								
							
								}
							}
								
						}//////////////////////////////////////////////////////////
						else if(phrase.get(x+2).equals("NN")||phrase.get(x+2).equals("PRP")){
							System.out.println("NN PRP loop");
							String change = phrase.get(x+3);
							System.out.println("change  "+change);
							
							
							
						   	change=change.trim();
							ArrayList<Character> chngeArray = new ArrayList<Character>();
							int a = change.length() - 1;
							
							for (int y = 0; y <= a ; y++) 
								{
									chngeArray.add(change.charAt(y));
								}
							System.out.println("Character Array"+chngeArray);
							int b = chngeArray.size() - 1;
							
							for (int y = 0; y <= b; y++) 
							{ 
								System.out.println("Index of y "+y);
								System.out.println("Character "+chngeArray.get(y));
								
								if (chngeArray.get(y).equals('ග'))
								{
									
									if(chngeArray.get(y-1).equals('ඇ'))
									{//////////////  ඇගේ  ////////////
									chngeArray.set(y, 'ය');
									chngeArray.set(y+1, 'ට');
									System.out.println("NEW Character Array"+chngeArray);
									
									String wordToPrint = chngeArray.toString().replaceAll(", |\\[|\\]", "");
									System.out.println("wordToPrint "+wordToPrint);
									phrase.set(x, wordToPrint); // Replace before tag with noun
									phrase.set(x+3, "පෙර");
									break;
									}
									
									if(chngeArray.get(y-1).equals('ම'))
									{///////////  මගේ     ///////
										chngeArray.set(y, 'ට');
										chngeArray.remove(y+1);
										--b;
										System.out.println("NEW Character Array"+chngeArray);
										
										String wordToPrint = chngeArray.toString().replaceAll(", |\\[|\\]", "");
										System.out.println("wordToPrint "+wordToPrint);
										phrase.set(x, wordToPrint); // Replace before tag with noun
										phrase.set(x+3, "පෙර");
										break;
										}
						
								}
								
								else if (chngeArray.get(y).equals('ය')&& y==b)
								{////////  ඇය 
									chngeArray.add(y+1, 'ට');
									System.out.println("NEW Character Array"+chngeArray);
									
									String wordToPrint = chngeArray.toString().replaceAll(", |\\[|\\]", "");
									System.out.println("wordToPrint "+wordToPrint);
									phrase.set(x, wordToPrint); // Replace before tag with noun
									phrase.set(x+3, "පෙර");
									break;
									
								}
								
								
								
								//////////////////////////
								else if ((!chngeArray.get(y).equals('ට'))&& y==b)
								{////////  any noun without "ට"
									chngeArray.add('ට');
									System.out.println("NEW Character Array"+chngeArray);
									
									String wordToPrint = chngeArray.toString().replaceAll(", |\\[|\\]", "");
									System.out.println("wordToPrint "+wordToPrint);
									phrase.set(x, wordToPrint); // Replace before tag with noun
									phrase.set(x+3, "පෙර");
									break;
								}
								
								else if((chngeArray.get(y).equals('ට'))&& y==b)
								{////////  any noun WITH "ට"
									System.out.println("NEW Character Array"+chngeArray);
									
									String wordToPrint = chngeArray.toString().replaceAll(", |\\[|\\]", "");
									System.out.println("wordToPrint "+wordToPrint);
									phrase.set(x, wordToPrint); // Replace before tag with noun
									phrase.set(x+3, "පෙර");
									break;
								}
							}
							
								
						}		
						
					/*	
						String nounToChange = phrase.get(x+3);
						
						
						String replaceValue =nounToChange+"ට"; ////////////////////// TAAAAAA \\\\
						phrase.add(x-1, replaceValue); // Replace before tag with noun
						phrase.add(x, "පෙර");*/
					}
				}
			}
			listcount ++;
		}
		System.out.println("MorfBefore class Output: "+phrase);
		return phrase;
	}
}

//////////////////////////////////////////////////////////////







	