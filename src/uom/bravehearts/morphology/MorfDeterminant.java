package uom.bravehearts.morphology;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import uom.bravehearts.passiveVoice.PassiveIdentify;

public class MorfDeterminant implements MorfRule {
	
	public List performRule(List<String> phrase) {
		System.err.println("MorfDeterminent executed...");
		System.out.println("MorfDeterminent input: " + phrase);

		// ADD WORDS FOR NP & VP

		if (phrase.contains("a") || phrase.contains("an")) {

			for (int x = 0; x <= phrase.size() - 1; x++) {
				if ((phrase.get(x).equals("a")) || (phrase.get(x).equals("an"))) {
					
					if((x-3>=0 &&!(phrase.get(x-3).equals("in")))||PassiveIdentify.boolPassive)
					{
					int countNP = 0;

					for (int y = x; y <= phrase.size() - 1; y++) {
						if ((phrase.get(y).equals("NP") || (phrase.get(y)
								.equals("VP") ||(phrase.get(y).equals("CC") ||(phrase.get(y).equals("ADVP")||(phrase.get(y).equals("NP-TMP") ||(phrase.get(y).equals("PP") || (y == phrase.size() - 1)))))))) {
							countNP++;
							for (int z = y; z >= x; z--) {
								if (phrase.get(z).equals("NN")
										&& (countNP == 1)) {
									countNP++;
									String addWord = phrase.get(z + 1);
									
									addWord=addWord.trim();
									
									ArrayList<Character> addDetToCharArray = new ArrayList<Character>();

									for (int Y = 0; Y <= addWord.length() - 1; Y++) // GET THE NOUN TO CHANGE TO CHAR ARRAY
									{
										addDetToCharArray.add(addWord.charAt(Y));
									}
									
									for (int Y = 0; Y <= addDetToCharArray.size() - 1; Y++) {
										if ((Y == addDetToCharArray.size() - 1)
												&& (addDetToCharArray.get(Y).equals('ා')))

										{// words like බල්ලෙක්

											addDetToCharArray.set(Y, '්');
											addDetToCharArray.add(Y, 'ෙ');
											addDetToCharArray.add(Y + 1, 'ක');
											System.out.println("");
											String wordToPrint = addDetToCharArray
													.toString().replaceAll(
															", |\\[|\\]", "");
											phrase.set(z + 1, wordToPrint);
											break;
										}

										else if (Y == addDetToCharArray.size() - 1
												&& (addDetToCharArray.get(Y)
														.equals('ි'))) {
											// words like කුමාරි
											addDetToCharArray.add(Y + 1, 'ය');
											addDetToCharArray.add(Y + 2, 'ක');
											addDetToCharArray.add(Y + 3, '්');

											System.out.println("");
											String wordToPrint = addDetToCharArray
													.toString().replaceAll(
															", |\\[|\\]", "");

											// System.out.println(WordToPrint);
											phrase.set(z + 1, wordToPrint);
											break;
										}

										else if (Y == addDetToCharArray.size() - 1
												&& (addDetToCharArray.get(Y)
														.equals('ට'))) {

											
											break;
										}

										
										else if (Y == addDetToCharArray.size() - 1
												&& (addDetToCharArray.get(Y)
														.equals('ී'))) {
											// words like කිරිල්ලී
											addDetToCharArray.remove(Y);
											addDetToCharArray.add(Y, 'ි');
											addDetToCharArray.add(Y + 1, 'ය');
											addDetToCharArray.add(Y + 2, 'ක');
											addDetToCharArray.add(Y + 3, '්');

											System.out.println("");
											String wordToPrint = addDetToCharArray
													.toString().replaceAll(
															", |\\[|\\]", "");

											phrase.set(z + 1, wordToPrint);
											break;
										}

										

										else if (Y == addDetToCharArray.size() - 1
												&& (addDetToCharArray.get(Y)
														.equals('ූ'))) {
											
											addDetToCharArray.remove(Y);
											addDetToCharArray.add(Y, 'ු');
											addDetToCharArray.add(Y + 1, 'ය');
											addDetToCharArray.add(Y + 2, 'ක');
											addDetToCharArray.add(Y + 3, '්');

											System.out.println("");
											String wordToPrint = addDetToCharArray
													.toString().replaceAll(
															", |\\[|\\]", "");

											phrase.set(z + 1, wordToPrint);
											break;
										}

										// plural to singular

										// plural to singular
										else if ((Y == addDetToCharArray.size() - 1 && (addDetToCharArray
												.get(Y).equals('ැ')))) {
											addDetToCharArray.add(Y + 1, 'ව');
											addDetToCharArray.add(Y + 2, 'ක');
											addDetToCharArray.add(Y + 3, '්');

											System.out.println("");
											String wordToPrint = addDetToCharArray
													.toString().replaceAll(
															", |\\[|\\]", "");

											phrase.set(z + 1, wordToPrint);
											break;
										} else if ((Y == addDetToCharArray
												.size() - 1 && (addDetToCharArray
												.get(Y).equals('ෑ')))) {

											addDetToCharArray.remove(Y);
											addDetToCharArray.add(Y, 'ැ');
											addDetToCharArray.add(Y + 1, 'ය');
											addDetToCharArray.add(Y + 2, 'ක');
											addDetToCharArray.add(Y + 3, '්');

											System.out.println("");
											String wordToPrint = addDetToCharArray
													.toString().replaceAll(
															", |\\[|\\]", "");

											phrase.set(z + 1, wordToPrint);
											break;
										}
										else if ((Y == addDetToCharArray.size() - 1)&& (!(addDetToCharArray.get(Y).equals('ක')) && (!(addDetToCharArray.get(Y).equals('්'))
														
														|| addDetToCharArray
																.get(Y).equals(
																		'ා')
														|| addDetToCharArray
																.get(Y).equals(
																		'ැ')
														|| addDetToCharArray
																.get(Y).equals(
																		'ෑ')
														|| addDetToCharArray
																.get(Y).equals(
																		'ි')
														|| addDetToCharArray
																.get(Y).equals(
																		'ී')
														|| addDetToCharArray
																.get(Y).equals(
																		'ු') || addDetToCharArray
														.get(Y).equals('ූ')))) {
											addDetToCharArray.add(Y + 1, 'ක');
											addDetToCharArray.add(Y + 2, '්');

											System.out.println("");
											String WordToPrint = addDetToCharArray.toString().replaceAll(", |\\[|\\]", "");
											phrase.set(z + 1, WordToPrint);

											break;

										}
										else if ((Y == addDetToCharArray.size() - 1)
												&& (!(addDetToCharArray.get(Y)
														.equals('්')
														|| addDetToCharArray
																.get(Y).equals(
																		'ා')
														|| addDetToCharArray
																.get(Y).equals(
																		'ැ')
														|| addDetToCharArray
																.get(Y).equals(
																		'ෑ')
														|| addDetToCharArray
																.get(Y).equals(
																		'ි')
														|| addDetToCharArray
																.get(Y).equals(
																		'ී')
														|| addDetToCharArray
																.get(Y).equals(
																		'ු') || addDetToCharArray
														.get(Y).equals('ූ')))) {
										//	addDetToCharArray.add(Y + 1, 'ක');
											addDetToCharArray.add(Y + 1, '්');

											System.out.println("");
											String WordToPrint = addDetToCharArray
													.toString().replaceAll(
															", |\\[|\\]", "");
											phrase.set(z + 1, WordToPrint);

											break;

										}
									}

									int isCount = 0;
									if (phrase.contains("යනු")) {
										for (int r = 0; r < phrase.size(); r++)

										{
											if (((phrase.get(r).equals("යනු") && (phrase
													.get(r + 1).equals("NP") && (phrase
													.get(r + 3).equals("a")))))) {
												for (int p = r + 2; p < phrase
														.size(); p++) {
													if ((!(phrase.get(p)
															.equals("NP")))
															&& (p == phrase
																	.size() - 1))

													{
														String detAtEnd = phrase
																.get(p);
														ArrayList<Character> addDetToEndArray = new ArrayList<Character>();
														for (int Y = 0; Y < detAtEnd
																.length(); Y++) {
															addDetToEndArray
																	.add(detAtEnd
																			.charAt(Y));
														}

														int lastIndex = addDetToEndArray
																.size() - 1;
														// for(int change=0;
														// change<addDetToEndArray.size();change++)
														// {if(isCount<1)
														// {if((change==lastIndex)&&
														// (addDetToEndArray.get(change).equals('්')))
														// {
														char character = addDetToEndArray
																.get(lastIndex);
														char characterNew = 'ි';
														if (character == '්')

														{
															addDetToEndArray
																	.set(lastIndex,
																			characterNew);
														}
														System.out.println("");
														String WordToPrint = addDetToEndArray
																.toString()
																.replaceAll(
																		", |\\[|\\]",
																		"");

														phrase.set(
																(phrase.size() - 1),
																WordToPrint);
														isCount++;
														// System.out.println("phrase"+phrase);

														break;
													}
												}
											}

										}
									}
								}

							}
						}
					}
				}

			}}
		}
		for (int detCount = 0; detCount < phrase.size(); detCount++)

		{
			if (((phrase.get(detCount).equals("a") || phrase.get(detCount)
					.equals("an")))) {
				phrase.remove(detCount);
			}

		}

		// return phrase;

		return phrase;
	}
}
