package uom.bravehearts.morphology.Verbs;

import java.util.ArrayList;
import java.util.List;

import uom.bravehearts.grammer.IsAnimate;
import uom.bravehearts.morphology.MorfRule;

public class PastTenseFinerlizer implements MorfRule {

	SubjectIdentifier idnty = new SubjectIdentifier();
	WordType wType = new WordType();
	IsAnimate chkAnimate = new IsAnimate();

	public List performRule(List<String> phrase) {
		System.err.println("PastTenseFinalizer executed...");
		System.out.println("PastTenseFinalizer input: " + phrase);
		{

			int count = 0;
			int countNxt = 1;
			String wordToChange = "";

			for (int x = 0; x <= phrase.size() - 1; x++) {
				String verbToArrange = "";
				ArrayList<Character> verbHandler = new ArrayList<Character>();
				if ((phrase.get(x).equals("VP")) && (x + 2 <= phrase.size())) {
					verbToArrange = ((phrase.get(x + 2).trim()));

					for (int sub = (x + 2); sub > 0; sub--) {
						if ((phrase.get(sub).equals("sbjBoundry")))

						{
							wordToChange = ((phrase.get(sub - 1)).trim());
							System.out.println("GOT THE OUTPUT IS..........."
									+ wordToChange);
							// break;

						}

					}

				}

				for (int z = 0; z < verbToArrange.length(); z++) {
					verbHandler.add(verbToArrange.charAt(z));

					// CHANGE THE VERB WHEN THE SUBJECT IS (I)

					if ((z == verbHandler.size() - 1)
							&& (verbHandler.get(z) == 'ා')
							&& ((verbHandler.get(z - 1) == 'ව') || (verbHandler
									.get(z - 1) == 'න'))) {
						if (wordToChange.equals("මම")) {
							verbHandler.add(z - 1, 'ෙ');
							verbHandler.set(z, 'ම');
							verbHandler.add(z - 1, 'ි');

							System.out.println("");
							String WordToPrint = verbHandler.toString()
									.replaceAll(", |\\[|\\]", "");

							for (int index = 0; index <= phrase.size() - 1; index++) {
								if (phrase.get(index).equals(verbToArrange)) {
									phrase.set(index, WordToPrint);
								}
							}
						} else if (wordToChange.equals("අපි")) {
							verbHandler.set(z, 'ු');
							verbHandler.set(z - 1, 'ම');
							verbHandler.remove(z - 2);

							System.out.println("");
							String WordToPrint = verbHandler.toString()
									.replaceAll(", |\\[|\\]", "");

							for (int index = 0; index <= phrase.size() - 1; index++) {
								if (phrase.get(index).equals(verbToArrange)) {
									phrase.set(index, WordToPrint);
								}
							}
						} else if (wordToChange.equals("ඔහු")) {
							verbHandler.set(z, 'ය');
							verbHandler.set(z - 1, 'ේ');
							verbHandler.set(z - 2, 'න');
							verbHandler.add(z - 1, '්');
							verbHandler.add(z, 'න');

							System.out.println("");
							String WordToPrint = verbHandler.toString()
									.replaceAll(", |\\[|\\]", "");

							for (int index = 0; index <= phrase.size() - 1; index++) {
								if (phrase.get(index).equals(verbToArrange)) {
									phrase.set(index, WordToPrint);

								}
							}
						} else if (wordToChange.equals("ඇය")) {
							verbHandler.set(z, 'ය');
							verbHandler.set(z - 1, 'ී');
							verbHandler.set(z - 2, 'න');
							verbHandler.add(z - 1, '්');
							verbHandler.add(z, 'න');

							System.out.println("");
							String WordToPrint = verbHandler.toString()
									.replaceAll(", |\\[|\\]", "");

							for (int index = 0; index <= phrase.size() - 1; index++) {
								if (phrase.get(index).equals(verbToArrange)) {
									phrase.set(index, WordToPrint);

								}
							}
						} else if (wordToChange.equals("ඔවුහු")) {
							verbHandler.set(z, 'ි');
							verbHandler.set(z - 1, 'ත');
							verbHandler.remove(z - 2);

							System.out.println("");
							String WordToPrint = verbHandler.toString()
									.replaceAll(", |\\[|\\]", "");
							for (int index = 0; index <= phrase.size() - 1; index++) {
								if (phrase.get(index).equals(verbToArrange)) {
									phrase.set(index, WordToPrint);

								}
							}
						}

						if (!((wordToChange.equals("මම")) || (wordToChange
								.equals("අපි") || (wordToChange.equals("ඔහු") || (wordToChange
								.equals("ඇය") || (wordToChange.equals("ඔවුහු"))))))) {

							for (int y = (x - 2); y <= (x - 2) && y > 0; y--) {

								if (phrase.get(y).equals("NNS")
										&& (countNxt == 1)) // if noun is plural
															// set subject to
															// plural
								{
									char type = chkAnimate
											.perform(wordToChange);

									if (type == 'f' || type == 'm') {
										String changedWord = wType
												.testWord(wordToChange); // verb
																			// change
																			// to
																			// plural
										phrase.set(x - 2, changedWord);
										wordToChange = changedWord;
										countNxt++;

										verbHandler.set(z, 'හ');

										System.out.println("");
										String WordToPrint = verbHandler
												.toString().replaceAll(
														", |\\[|\\]", "");

										for (int index = 0; index <= phrase
												.size() - 1; index++) {
											if (phrase.get(index).equals(
													verbToArrange)) {
												phrase.set(index, WordToPrint);

											}
										}
									}
								}

								if (phrase.get(y).equals("NNS")
										&& (countNxt > 1)) // if noun is plural
															// set subject to
															// plural
								{
									phrase = ChangeObjectPlural
											.turnToPlura(phrase);
								}

								if (phrase.get(y).equals("NN")
										&& (countNxt == 1)) {
									// System.out.println("Do nothing....!!!");
									countNxt++;

									char type = chkAnimate
											.perform(wordToChange);

									if (type == 'f') {
										verbHandler.set(z, 'ය');
										verbHandler.set(z - 1, 'ී');
										verbHandler.set(z - 2, 'න');
										verbHandler.add(z - 1, '්');
										verbHandler.add(z, 'න');

										System.out.println("");
										String WordToPrint = verbHandler
												.toString().replaceAll(
														", |\\[|\\]", "");

										for (int index = 0; index <= phrase
												.size() - 1; index++) {
											if (phrase.get(index).equals(
													verbToArrange)) {
												phrase.set(index, WordToPrint);

											}
										}
									} else if (type == 'm') {
										verbHandler.set(z, 'ය');
										verbHandler.set(z - 1, 'ේ');
										verbHandler.set(z - 2, 'න');
										verbHandler.add(z - 1, '්');
										verbHandler.add(z, 'න');

										System.out.println("");
										String WordToPrint = verbHandler
												.toString().replaceAll(
														", |\\[|\\]", "");
										// System.out.println(WordToPrint);
										// System.out.println(sinhalaWordList);

										for (int index = 0; index <= phrase
												.size() - 1; index++) {
											if (phrase.get(index).equals(
													verbToArrange)) {
												phrase.set(index, WordToPrint);
												// System.out.println(sinhalaWordList);
											}
										}
									}

									else if (type == 'i') {
										verbHandler.remove(z);
										verbHandler.set(z - 1, 'ි');
										verbHandler.set(z - 2, 'ය');

										System.out.println("");
										String WordToPrint = verbHandler
												.toString().replaceAll(
														", |\\[|\\]", "");
										// System.out.println(WordToPrint);
										// System.out.println(sinhalaWordList);

										for (int index = 0; index <= phrase
												.size() - 1; index++) {
											if (phrase.get(index).equals(
													verbToArrange)) {
												phrase.set(index, WordToPrint);
												// System.out.println(sinhalaWordList);
											}
										}
									}
								}

							}
						}
					}
				}
			}

			System.out.println("Past Tense Finalizer output :" + phrase);
			return phrase;
		}
	}
}
