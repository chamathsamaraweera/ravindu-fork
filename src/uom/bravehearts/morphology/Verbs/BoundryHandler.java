package uom.bravehearts.morphology.Verbs;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import uom.bravehearts.grammer.IsAnimate;
import uom.bravehearts.morphology.MorfBaseForm;
import uom.bravehearts.morphology.MorfRule;
import uom.bravehearts.passiveVoice.*;

public class BoundryHandler implements MorfRule {

	static IsAnimate checkAnimate = new IsAnimate();
	
	public static char res02 = 'i';
	

	public static boolean NNSexecuted;
	//	CHANGE THE "NN" AND "NNS" TAGS IN FEMININE AND MASCULINE NOT INANIMATE
	/*
	 * public static void main(String [] args) {
	 * Morf_CheckDeterminant(passWord); }
	 */
	// public static List Morf_CheckDeterminant(String passWord)
	public List performRule(List<String> phrase) {
		System.err.println("BoundryHandler executed...");
		System.out.println("BoundryHandler input: " + phrase);
		
		List<String> newPhrase = new ArrayList<String>();

		if (!(phrase.contains("යනු"))) {
			
//			if(PassiveIdentify.boolPassive)
//			{
//				newPhrase = PassiveSinguPlural.HandlePlural((ArrayList<String>) phrase);
//				return newPhrase;
//			}
			
			for (int boundry = 0; boundry <= phrase.size() - 1; boundry++) {
				if (phrase.get(boundry).equals("sbjBoundry"))

					for (int x = boundry; x <= phrase.size() - 1; x++) {

						int countNP = 0;

						for (int y = x; y <= phrase.size() - 1; y++) {
							if ((phrase.get(y).equals("NP") || (phrase.get(y)
									.equals("VP") || (y == phrase.size() - 1)))) {
								countNP++;
								for (int z = y; z >= x; z--) {
									if (phrase.get(z).equals("NN")
											&& (countNP == 1)) {
										String addWord = phrase.get(z + 1)
												.trim();

										ArrayList<Character> addDetToCharArray = new ArrayList<Character>();

										for (int Y = 0; Y <= addWord.length() - 1; Y++) {
											addDetToCharArray.add(addWord
													.charAt(Y));
										}

										char res = checkAnimate.perform(addWord);
										res02 = res;
										if (res == 'f') {
											for (int Y = 0; Y <= addDetToCharArray
													.size() - 1; Y++) {
												if ((Y == addDetToCharArray
														.size() - 1)) {

													addDetToCharArray.add(
															Y + 1, 'ක');
													System.out.println("");
													String wordToPrint = addDetToCharArray
															.toString()
															.replaceAll(
																	", |\\[|\\]",
																	"");
													phrase.set(z + 1,
															wordToPrint);
													break;

												}
											}
										}

										else if (res == 'm') {
											for (int Y = 0; Y <= addDetToCharArray
													.size() - 1; Y++) {
												if ((Y == addDetToCharArray
														.size() - 1)) {

													addDetToCharArray.set(Y,
															'ක');
													addDetToCharArray.add(
															Y + 1, 'ු');
													System.out.println("");
													String wordToPrint = addDetToCharArray
															.toString()
															.replaceAll(
																	", |\\[|\\]",
																	"");
													phrase.set(z + 1,
															wordToPrint);
													break;

												}
											}
										}
									}
								}
							}
						}
					}

			}
		}
		// }
		//	IDENTIFYING NNS -  BETWEEN NP AND VP - GO NP TO VP THEN COME BACK VP TO NP
		if (phrase.contains("NNS") && !(phrase.contains("යනු"))) {
			//if(PassiveIdentify.boolPassive)
			//{
				phrase = PassiveSinguPlural.HandlePlural((ArrayList<String>) phrase);
				//return newPhrase;
			//}
			for (int boundry = 0; boundry <= phrase.size() - 1; boundry++) 
			{
				if (phrase.get(boundry).equals("sbjBoundry"))

					for (int x = boundry; x <= phrase.size() - 1; x++) {
						if (phrase.get(x).equals("NNS")) {
							int countNP = 0;

							for (int y = x; y <= phrase.size() - 1; y++) {
								if ((phrase.get(y).equals("NP") || (phrase.get(
										y).equals("VP") || (phrase.get(y)
										.equals("ADVP") || (phrase.get(y)
										.equals("RB") || (y == phrase.size() - 1)))))) {
									countNP++;
									for (int z = y; z >= x; z--) {
										if (phrase.get(z).equals("NNS")
												&& (countNP == 1)) {
											String addWord = phrase.get(z + 1).trim();

											ArrayList<Character> addDetToCharArray = new ArrayList<Character>();

											for (int Y = 0; Y <= addWord
													.length() - 1; Y++) {
												addDetToCharArray.add(addWord
														.charAt(Y));
											}

											char res = checkAnimate.perform(addWord);
											res02 = res;

											if (res == 'f') {
												for (int Y = 0; Y <= addDetToCharArray
														.size() - 1; Y++) {
													if ((Y == addDetToCharArray
															.size() - 1)
															&& ((addDetToCharArray
																	.get(Y) != '්') || (addDetToCharArray
																	.get(Y) != 'ට'))) {

														addDetToCharArray.add(
																Y + 1, 'න');
														addDetToCharArray.add(
																Y + 2, '්');

														System.out.println("");
														String wordToPrint = addDetToCharArray
																.toString()
																.replaceAll(
																		", |\\[|\\]",
																		"");
														phrase.set(z + 1,
																wordToPrint);
														break;

													}
												}
											}

											else if (res == 'm') {
												for (int Y = 0; Y <= addDetToCharArray
														.size() - 1; Y++) {
													if ((Y == addDetToCharArray
															.size() - 1)
															&& ((addDetToCharArray
																	.get(Y) != '්') || (addDetToCharArray
																	.get(Y) != 'ට'))) {

														addDetToCharArray.set(
																Y, 'න');
														addDetToCharArray.add(
																Y + 1, '්');
														System.out.println("");
														String wordToPrint = addDetToCharArray
																.toString()
																.replaceAll(
																		", |\\[|\\]",
																		"");
														phrase.set(z + 1,
																wordToPrint);
														break;

													}
												}
											}

											else if (res == 'i') {
												for (int Y = 0; Y <= addDetToCharArray.size() - 1; Y++) {
													if ((Y == addDetToCharArray
															.size() - 1)
															&& ((addDetToCharArray
																	.get(Y)
																	.equals('ය')) || addDetToCharArray
																	.get(Y)
																	.equals('ව')))

													{

														// addDetToCharArray.add(Y
														// + 1, 'ක');
														//int charSize = addDetToCharArray.size()-1;
														addDetToCharArray
																.remove(Y);
//														if(charSize>=4 && addDetToCharArray.get(charSize-1).equals('ල'))
//														{
//															addDetToCharArray.add('්');
//														}

														System.out.println("");
														String WordToPrint = addDetToCharArray
																.toString()
																.replaceAll(
																		", |\\[|\\]",
																		"");
														phrase.set(z + 1,
																WordToPrint);

														break;

													} else if ((Y == addDetToCharArray
															.size() - 1)
															&& (!(addDetToCharArray
																	.get(Y)
																	.equals('්')
																	|| addDetToCharArray
																			.get(Y)
																			.equals('ට')
																	|| addDetToCharArray
																			.get(Y)
																			.equals('ා')
																	|| addDetToCharArray
																			.get(Y)
																			.equals('ැ')
																	|| addDetToCharArray
																			.get(Y)
																			.equals('ෑ')
																	|| addDetToCharArray
																			.get(Y)
																			.equals('ි')
																	|| addDetToCharArray
																			.get(Y)
																			.equals('ී')
																	|| addDetToCharArray
																			.get(Y)
																			.equals('ු') || addDetToCharArray
																	.get(Y)
																	.equals('ූ'))))

													{

														// addDetToCharArray.add(Y
														// + 1, 'ක');
														addDetToCharArray.add(
																Y + 1, '්');

														System.out.println("");
														String WordToPrint = addDetToCharArray
																.toString()
																.replaceAll(
																		", |\\[|\\]",
																		"");
														phrase.set(z + 1,
																WordToPrint);

														break;

													}

												}
											}
										}

									}
								}
							}
						}
					}
			}
		}
		// return phrase;

		System.out.println("BoundryHandler output: " + phrase);
		NNSexecuted = true;
		return phrase;
	}
}
