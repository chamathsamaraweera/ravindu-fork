package uom.bravehearts.morphology.Verbs;

import java.util.ArrayList;
import java.util.List;

import uom.bravehearts.morphology.MorfRule;
import uom.bravehearts.morphology.Passive.PassivePaCo;
import uom.bravehearts.morphology.Passive.PassivePrCo;
import uom.bravehearts.passiveVoice.PassiveIdentify;
import uom.bravehearts.passiveVoice.PassiveContinuous;

public class PresentContinuous implements MorfRule {
	
	public List performRule(List<String> phrase) {
		
		SubjectIdentifier idnty = new SubjectIdentifier();
		PassivePrCo  passivePresentContinuous= new PassivePrCo();
		PassivePaCo  passivePastContinuous = new PassivePaCo();
		int phraseSize=phrase.size();
		
		System.out.println("Present Continuous executed...");
		System.out.println("Present Continuous input: " + phrase);
		{	
			
			String verb = null;
			int count = 0;
			int countNxt = 1;
			String wordToChange = "";
			PresentContinuous ad = new PresentContinuous();
			
			for (int x = 0; x <= phrase.size() - 1; x++) {
				String verbToArrange = "";
				ArrayList<Character> verbHandler = new ArrayList<Character>();

				if (((phrase.get(x).equals("VP")) && (x + 2 <= phrase.size())&& ((phrase.get(x + 1).equals("VBG"))||(phrase.get(x + 1).equals("VBGi"))))||((phrase.get(x).equals("CC")) && (x + 3 <= phrase.size())&& ((phrase.get(x + 2).equals("VBG"))||(phrase.get(x + 2).equals("VBGi"))))) 
				{
					{
				
					if(phrase.get(x).equals("VP"))
					{
					verb = (phrase.get(x + 2).trim());
					verbToArrange = ((phrase.get(x + 2).trim()));
					if(PassiveIdentify.boolPassive)
					{
						verbToArrange = PassiveContinuous.ContinuousVerbToModify(phrase, x);
						verb = verbToArrange;
					}
					System.out.println("Got the verb : ..."+verb);
					
					for (int z = 0; z < verbToArrange.length(); z++) {
						verbHandler.add(verbToArrange.charAt(z));

					}
					}
					else if(phrase.get(x).equals("CC"))
					{
					verb = (phrase.get(x + 3));
					verbToArrange = ((phrase.get(x + 3).trim()));
					System.out.println("Got the verb : ..."+verb);
					for (int z = 0; z < verbToArrange.length(); z++) {
						verbHandler.add(verbToArrange.charAt(z));

					}
					}
					

					

						for (int z = 0; z < verbToArrange.length(); z++) {
							if ((z == verbHandler.size() - 1)
									&& (verbHandler.get(z).equals('ා'))
									&& (verbHandler.get(z - 1).equals('ව'))
									&& (verbHandler.get(z - 2).equals('න'))) {
								verbHandler.set(z, '්');
								verbHandler.add(z, 'න');
								verbHandler.set(z - 1, 'ි');
								verbHandler.set(z - 2, 'ම');

								System.out.println("");
								String WordToPrint = verbHandler.toString()
										.replaceAll(", |\\[|\\]", "");

								for (int index = 0; index <= phrase.size() - 1; index++) {
									if (phrase.get(index).trim().equals(verb)) {
										phrase.set(index, WordToPrint);
									}
								}
							}

							else

								for (z = 0; z < verbToArrange.length(); z++) {
									if ((z == verbHandler.size() - 1)
											&& (verbHandler.get(z).equals('ා'))
											&& ((verbHandler.get(z - 1)
													.equals('ව')) || (verbHandler
													.get(z - 1).equals('න')))) {
										verbHandler.set(z, '්');
										verbHandler.add(z, 'න');
										verbHandler.set(z - 1, 'ි');
										verbHandler.set(z - 2, 'ම');

										System.out.println("");
										String WordToPrint = verbHandler
												.toString().replaceAll(
														", |\\[|\\]", "");

										for (int index = 0; index <= phrase
												.size() - 1; index++) {
											if (phrase.get(index).trim().equals(verb)) {
												phrase.set(index, WordToPrint);
											}
										}
									}
								}
						}
					}

				}
			
		}
		
	}
		System.out.println("Continuous output :" + phrase);
		
		
		if(PassiveIdentify.boolPassive && phrase.get(phraseSize-2).trim().equals("PaPrCo"))
		{
			phrase = passivePresentContinuous.passiveVerbHandler(phrase);
		}
		
		if(PassiveIdentify.boolPassive && phrase.get(phraseSize-2).trim().equals("PaPaCo"))
		{
			phrase = passivePastContinuous.passiveVerbHandler(phrase);
		}
		return phrase;
	}

	
}
