package uom.bravehearts.morphology.Verbs;

import java.awt.Checkbox;
import java.util.ArrayList;
import java.util.List;

import uom.bravehearts.grammer.IsAnimate;
import uom.bravehearts.morphology.CheckFromDB;
import uom.bravehearts.morphology.MorfRule;
import uom.bravehearts.morphology.Passive.PassivePrSi;
import uom.bravehearts.morphology.Prepositions.MorfWhen;
import uom.bravehearts.passiveVoice.PassiveIdentify;

public class VerbFinalizer implements MorfRule{

	//SubjectIdentifier idnty=new SubjectIdentifier();
	MorfWhen chkWhen=new MorfWhen();
	IsAnimate chkAnimate=new IsAnimate();
	CheckFromDB chk=new CheckFromDB();
	WordType wType=new WordType();
	PassivePrSi passive = new PassivePrSi();
	
	public  List performRule(List<String>phrase)
	{ 
		System.err.println("VerbFinalizer executed...");
		System.out.println("verbFinalizer input: "+phrase);
		 {
			 				int countIs=0;
			 				
			 				int countNxt=1;
			 				String wordToChange="";

			 						for (int x = 0; x <= phrase.size() - 1; x++) {// LOOP THROUGH THE PHRASE 
			 							
			 							int count=0;

			 								String verbToArrange="";
 					 						String verbToArrangeMat="";
					 						ArrayList<Character> wrdHandler=new ArrayList<Character>();
					 						ArrayList<Character> verbHandler=new ArrayList<Character>();
			 							if(((phrase.get(x).equals("VP"))&&(x+2<=phrase.size())&&((phrase.get(x+1).equals("VBZ"))||(phrase.get(x+1).equals("VBP"))))//	PRESENT OR PRESENT CONTINUOUS TENSE
						 						||((phrase.get(x).equals("CC"))&&(x+2<=phrase.size())&&((phrase.get(x+2).equals("VBZ"))||(phrase.get(x+2).equals("VBP")))))
						 						 {
			 										// moving to PassivePrSi for handling verbs for Passive Voice  ////////////
			 									String tenseTag = phrase.get(phrase.size()-1);
			 									if(PassiveIdentify.boolPassive && "PaPrSi".equals(tenseTag)){ 
		 									
			 									System.out.println("Passive loop executed");
			 									phrase = passive.passiveVerbHandler(phrase);
			 										
			 									break;
			 									}
			 									///////////////
			 									System.out.println("Outer if VP executed");
			 									//	HANDLING PRESENT CONTINUOUS TENSE(AM/IS/ARE)
			 									// handling "was" / "were"
			 									if ((phrase.get(x + 2).equals("is"))|| (phrase.get(x + 2).equals("am"))|| (phrase.get(x + 2).equals("are"))) 
			 									{
			 										
			 										System.out.println("IF VP executed");
			 											if(!(phrase.get(phrase.size() - 1).equals("සිටිනවා"))||(phrase.get(phrase.size() - 1).equals("සිටියි"))||(phrase.get(phrase.size() - 1).equals("සිටින්නීය"))||(phrase.get(phrase.size() - 1).equals("සිටින්නේය"))||(phrase.get(phrase.size() - 1).equals("සිටින්නෝය"))||(phrase.get(phrase.size() - 1).equals("සිටිමි"))||(phrase.get(phrase.size() - 1).equals("සිටිමු")))
			 											{
			 												System.out.println("if vp siti executed");
			 											
			 											for (int y = x + 2; y <= phrase.size() - 1; y++) {

			 												if ((y+1<=(phrase.size()-1))&&(phrase.get(y + 1).equals("VP"))&& (y + 2 <= phrase.size())&& (phrase.get(y + 2).equals("VBG"))) 
			 												{
			 													verbToArrangeMat= "සිටිනවා";
			 													verbToArrange = "සිටිනවා";
			 													count++;
			 													
			 													if(count==1)
			 													{
			 													phrase.add(y+4,verbToArrange);
			 													phrase.set(y+2,"VBGi");
			 													System.out.println("සිටිනවා add..........."+(y + 2));
			 													System.out.println("phrase..........."+phrase);
			 													count++;
			 													break;
						 										
			 													}
			 													
			 													for(int sub=(y+2); sub>0;sub--)
										 							{
			 														System.out.println("sbjBoundry equas "+phrase.get(sub));
				 													
										 							if((phrase.get(sub).equals("sbjBoundry")))
										 								
										 										{
										 								
										 										wordToChange=((phrase.get(sub-1)).trim());
										 										System.out.println("GOT THE OUTPUT IS..........."+wordToChange);
										 										break;
										 										
										 										}
										 							}}
			 													
			 													//break;
			 												}

			 											}}

			 									//}//////////////////// am   is    are  /////////////

			 						
			 						
			 									 									
			 											 						
			 									 									
			 											 						else if (!(phrase.get(x + 2).equals("is"))|| (phrase.get(x + 2).equals("am"))|| (phrase.get(x + 2).equals("are"))) 
			 											 						{	System.out.println("else VP executed");
			 											 							if(phrase.get(x).equals("VP"))
			 											 							{
			 											 							verbToArrangeMat=(phrase.get(x+2));
			 											 							verbToArrange=(phrase.get(x+2).trim());
			 											 							
			 											 							
			 											 						 	for(int sub=(x+2); sub>0;sub--)
			 											 							{
			 											 							if((phrase.get(sub).equals("sbjBoundry")))
			 											 								
			 											 										{
			 											 										wordToChange=((phrase.get(sub-1)).trim());
			 											 										System.out.println("GOT THE OUTPUT IS..........."+wordToChange);
			 											 										break;
			 											 										
			 											 										}
			 											 							}
			 											 							}
			 											 						 	
			 											 						 	else if(phrase.get(x).equals("CC"))
			 											 							{
			 											 							verbToArrangeMat=(phrase.get(x+3));
			 											 							verbToArrange=(phrase.get(x+3).trim());
			 											 							
			 											 							
			 											 						 	for(int sub=(x+3); sub>0;sub--)
			 											 							{
			 											 							if((phrase.get(sub).equals("sbjBoundry")))
			 											 								
			 											 										{
			 											 										wordToChange=((phrase.get(sub-2)).trim());
			 											 										System.out.println("GOT THE OUTPUT IS..........."+wordToChange);
			 											 										break;
			 											 										
			 											 										}
			 											 							}
			 											 									
			 											 							
			 											 							}
			 										 						}
			 										 							
			 										 							
			 																	System.out.println("GOT THE OUTPUT IS (phrase.get(x+2)..........."+(phrase.get(x+2)));
			 										 							
			 										 							//	IF FOLLOWING WORDS COME INSTEAD OF THE "IS" SET "I" TO "ME"
			 										 							if((phrase.get(x+2).trim().equals("අවශ්‍යව පවතිනවා"))||(phrase.get(x+2).trim().equals("පුළුවන්"))||(phrase.get(x+2).trim().equals("තියෙනවා"))||(phrase.get(x+2).trim().equals("අවශ්‍යයි"))||(phrase.get(x+2).trim().equals("උවමනා කරනවා")))	
			 											 							
			 											 						{count=0;
			 										 								
			 											 									for(int n=(x+2); n>=0;n--)
			 													 					{
			 													 					if(phrase.get(n).equals("sbjBoundry"))//||(phrase.get(n).equals("NP")|| phrase.get(n).equals("VP"))||((phrase.get(n).equals("PP"))||((phrase.get(n).equals("ADVP"))||((phrase.get(n).equals("CC"))))))
			 													 						{
			 													 							count++;
			 													 							
			 															
			 													 								
			 													 							if ((count==1) && ((phrase.get(n).equals("sbjBoundry"))))//||(phrase.get(n).equals("NP"))||((phrase.get(n).equals("VP"))||((phrase.get(n).equals("PP"))||((phrase.get(n).equals("ADVP"))||((phrase.get(n).equals("CC"))))))))
			 													 							{count++;
			 													 								//wordToChange=((phrase.get(n-2)));
			 													 							String change=(phrase.get(n-1).trim());	
			 													 							for(int z=0; z<change.length(); z++)
			 										 			 							{
			 													 								wrdHandler.add(change.charAt(z));
			 										 			 							}
			 													 								//System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+(phrase.get(n-2)));
			 													 								if(change.trim().equals("මම"))
			 														 							{
			 													 									phrase.set((n-1),"මට");
			 													 									break;
			 													 								}
			 													 								else
			 													 								{
			 													 									for(int z=0; z<change.length(); z++)
			 												 			 							{	
			 													 										 if((z==wrdHandler.size()-1)&&!(wrdHandler.get(z).equals('ට')))
			 													 										 {
			 													 											 //	System.out.println("!===================================="+(phrase.get(n-2)));
			 													 											 String add=change.trim();
			 													 											 phrase.set((n-1),(add+"ට").trim());
			 													 											 break;
			 													 										 }
			 													 								
			 												 			 							}
			 													 								}	
			 													 						}
			 													 						}}
			 											 									
			 										 								
			 										 								
			 													 					//}	
			 											 							
			 											 							
			 											 							
			 											 						}	
			 										 					
			 										 							for(int sub=(x+2); sub>0;sub--)
			 										 							{
			 										 							if((phrase.get(sub).equals("sbjBoundry")))
			 										 								
			 										 										{
			 										 										wordToChange=((phrase.get(sub-1)).trim());
			 										 										System.out.println("GOT THE OUTPUT IS..........."+wordToChange);
			 										 										break;
			 										 										
			 										 										}
			 										 									
			 										 							
			 										 							}
			 										 							
			 										 						for(int z=0; z<verbToArrange.length(); z++)
			 									 							{
			 									 							verbHandler.add(verbToArrange.charAt(z));// ADD LASTLY ADDEED SINHALA WORD(සිටිනවා) INTO A CHAR ARRAY
			 									 							}
			 										 						System.out.println("verb to array"+verbHandler);
			 										 						
			 										 						
			 										 						
			 										 						//System.out.println("verb to array"+phrase);
			 										 						System.out.println("wordToChange to array"+wordToChange);
			 										 						
			 										 						
			 										 						//CHANGE THE VERB WHEN THE SUBJECT IS (I)
			 										 						for(int z=0; z<verbToArrange.length(); z++)
			 										 						{
			 										 							if((z==verbHandler.size()-1)&&(verbHandler.get(z)=='ා')&& (verbHandler.get(z-1)=='ව')&& (verbHandler.get(z-2)=='න')) ///////// නවා loop   ////////////
			 										 							{
			 										 								if(wordToChange.equals("මම"))
			 										 								{
			 										 									if(verbHandler.get(z-3).equals('්'))
			 										 									{
			 										 										verbHandler.set(z-3, 'ි');
			 										 									}
			 										 									
			 										 									
			 										 									verbHandler.set(z, 'ි');
			 										 									verbHandler.set(z-1, 'ම');
			 										 									verbHandler.remove(z-2);
			 														
			 										 									System.out.println("");
			 										 									String WordToPrint = verbHandler.toString().replaceAll(", |\\[|\\]", "");
			 										 									System.out.println("verbToArrange"+verbToArrange);
			 											           
			 										 									System.out.println("WordToPrint"+WordToPrint);
			 													
			 										 									for(int index=0;index<=phrase.size()-1;index++ )
			 										 									{
			 										 										if(phrase.get(index).equals(verbToArrangeMat))
			 										 										{
			 										 											phrase.set(index, WordToPrint);
			 										 											System.out.println("sitieeee "+phrase);
			 														
			 										 											
			 										 								}
			 										 							}
			 										 						}
			 										 								else if(wordToChange.equals("අපි"))
			 										 								{
			 										 									if(verbHandler.get(z-3).equals('්'))
			 										 									{
			 										 										verbHandler.set(z-3, 'ි');
			 										 									}
			 										 									//System.out.println("Yes you are");
			 										 									verbHandler.set(z, 'ු');
			 										 									verbHandler.set(z-1, 'ම');
			 										 									verbHandler.remove(z-2);
			 													
			 										 									System.out.println("");
			 										 									String  WordToPrint = verbHandler.toString().replaceAll(", |\\[|\\]", "");
			 										 									//System.out.println(WordToPrint);
			 										 									//System.out.println(sinhalaWordList);
			 												
			 										 									for(int index=0;index<=phrase.size()-1;index++ )
			 										 									{
			 										 										if(phrase.get(index).equals(verbToArrangeMat))
			 										 										{
			 										 											phrase.set(index, WordToPrint);
			 																	//System.out.println(sinhalaWordList);
			 									
			 																	
			 														}
			 													}
			 												}
			 												else if((wordToChange.equals("ඔහු"))&&(verbHandler.get(z-1)=='ව'))
			 												{
			 													if((verbHandler.get(z-3)=='්')&&(verbHandler.get(z-4)=='න'))
			 													{
			 														verbHandler.set(z, 'ය');
			 														verbHandler.set(z-1, 'ේ');
			 														//verbHandler.set(z-2,'න');
			 														//verbHandler.add(z-1, '්');
			 													//	verbHandler.add(z,'න');
			 													
			 														
			 														String  WordToPrint = verbHandler.toString().replaceAll(", |\\[|\\]", "");
			 														//System.out.println(WordToPrint);
			 														System.out.println("befre phrase  "+phrase);
			 												
			 														for(int index=0;index<=phrase.size()-1;index++ )
			 															{
			 																if(phrase.get(index).equals(verbToArrangeMat))
			 																{
			 																	phrase.set(index, WordToPrint);
			 																	System.out.println("after ඔහු "+phrase);
			 																	break;
			 																}
			 															}
			 													}
			 													else{
			 														//System.out.println("ඔහුඔහුඔහුඔහුඔහු"+verbToArrangeMat);	
			 													
			 													verbHandler.set(z, 'ය');
			 													verbHandler.set(z-1, 'ේ');
			 													verbHandler.set(z-2,'න');
			 													verbHandler.add(z-1, '්');
			 													verbHandler.add(z,'න');
			 												
			 													System.out.println("");
			 													String  WordToPrint = verbHandler.toString().replaceAll(", |\\[|\\]", "");
			 													System.out.println("befre phrase  "+phrase);
			 													System.out.println("befre verbToArrangeMat  "+verbToArrangeMat);
			 													//System.out.println(sinhalaWordList);
			 											
			 													for(int index=0;index<=phrase.size()-1;index++ )
			 														{
			 															if(phrase.get(index).equals(verbToArrangeMat))
			 															{
			 																phrase.set(index, WordToPrint);
			 																System.out.println("after ඔහු "+phrase);
			 																break;
			 															}
			 														}
			 												}
			 												}
			 												else if(wordToChange.equals("ඇය"))
			 												{
			 													verbHandler.set(z, 'ය');
			 													verbHandler.set(z-1, 'ී');
			 													verbHandler.set(z-2,'න');
			 													verbHandler.add(z-1, '්');
			 													verbHandler.add(z,'න');
			 												
			 													
			 													String  WordToPrint = verbHandler.toString().replaceAll(", |\\[|\\]", "");
			 													
			 													System.out.println("befre phrase  "+phrase);
			 													System.out.println("befre verbToArrangeMat  "+verbToArrangeMat);
				 												
			 													for(int index=0;index<=phrase.size()-1;index++ )
			 														{
			 															if(phrase.get(index).equals(verbToArrangeMat))
			 															{
			 																phrase.set(index, WordToPrint);
			 																System.out.println("after ඇය "+phrase);
			 																
			 																
			 															}
			 														}
			 													
			 											}
			 												else	if(wordToChange.equals("ඔවුහු"))
			 										 								{
			 													if(verbHandler.get(z-3).equals('්'))
			 													{
			 														verbHandler.set(z-3, 'ි');
			 													}
			 										 									verbHandler.set(z, 'ි');
			 										 									verbHandler.set(z-1, 'ත');
			 										 									verbHandler.remove(z-2);
			 														
			 										 									System.out.println("");
			 										 									String WordToPrint = verbHandler.toString().replaceAll(", |\\[|\\]", "");
			 										 									for(int index=0;index<=phrase.size()-1;index++ )
			 										 									{
			 										 										if(phrase.get(index).equals(verbToArrangeMat))
			 										 										{
			 										 											phrase.set(index, WordToPrint);
			 										 											
			 										 										}
			 										 									}
			 										 								}}////////////////////////////////////////////////// nawa loop ends
			 										 								
			 												else if (!((wordToChange.equals("මම"))||(wordToChange.equals("අපි")||(wordToChange.equals("ඔහු")||(wordToChange.equals("ඇය")||(wordToChange.equals("ඔවුහු")))))))
			 										 								{	
			 													
			 													for(int y=0;y<=phrase.size()-1;y++)
			 													{
			 														
			 														
			 														if(phrase.get(y).equals("NNS"))		//if noun is plural set subject to plural 
			 																{
			 															wordToChange=(phrase.get(y+1));
			 															char type=chkAnimate.perform(wordToChange);
			 																if(type=='f'|| type=='m')
			 															{
			 																
			 																
			 																String changedWord=wType.testWord(wordToChange);		//verb change to plural
			 																phrase.set(y+1,changedWord);
			 																wordToChange=changedWord;
			 																countNxt++;
			 																
			 																for( z=0; z<=verbHandler.size()-1;z++)
		 											 							
			 																{
			 																	if((z==verbHandler.size()-1))
			 																
				 																
				 																{
			 																
			 																	verbHandler.set(z, 'ය');
			 							 										verbHandler.set(z-1, 'ෝ');
			 							 										verbHandler.set(z-2,'න');
			 							 										verbHandler.add(z-1, '්');
			 							 										verbHandler.add(z,'න');
			 							 									
			 							 										System.out.println("");
			 							 										String  WordToPrint = verbHandler.toString().replaceAll(", |\\[|\\]", "");
			 							 										
			 							 								
			 							 										for(int index=0;index<=phrase.size()-1;index++ )
			 							 											{
			 							 												if(phrase.get(index).equals(verbToArrangeMat))
			 							 												{
			 							 													phrase.set(index, WordToPrint);
			 							 													
			 							 												}
			 							 											}break;
			 																}
			 																}
			 															}
			 															 if(type=='i') 
			 																{ 
			 																String changedWord=wType.testWord(wordToChange);		//verb change to plural
			 																phrase.set(y+1,changedWord);
			 																wordToChange=changedWord;
			 																countNxt++;
			 																
			 																for( z=0; z<=verbHandler.size()-1;z++)
		 											 							
			 																{
			 																	if((z==verbHandler.size()-1))
			 																
				 																
				 																{
			 																	if(verbHandler.get(z-3).equals('්'))
			 																	{
			 																		verbHandler.set(z-3, 'ි');
			 																	}
			 																	verbHandler.remove(z);
			 																	verbHandler.set(z-1, 'ි');
			 																	verbHandler.set(z-2,'ය');
			 																	
			 																
			 																	System.out.println("");
			 																	String  WordToPrint = verbHandler.toString().replaceAll(", |\\[|\\]", "");
			 																	//System.out.println(WordToPrint);
			 																	//System.out.println(sinhalaWordList);
			 															
			 																	for(int index=0;index<=phrase.size()-1;index++ )
			 																		{
			 																			if(phrase.get(index).equals(verbToArrangeMat))
			 																			{
			 																				phrase.set(index, WordToPrint);
			 																				//System.out.println(sinhalaWordList);
			 																			}break;
			 																		}
			 																}
			 																}
			 																}
			 																}
			 														
			 														else	if(phrase.get(y).equals("NN")&& (countNxt==1))
			 														{
			 															wordToChange=(phrase.get(y+1));
			 															
			 														System.out.println("Do nothing....!!!"+wordToChange);
			 														countNxt++;
			 														
			 														char type=chkAnimate.perform(wordToChange);
			 														
			 														System.out.println("Do nothing type....!!!"+type);
			 															
			 															
			 															if(type=='f')
			 															{
			 																
			 																for( z=0; z<=verbHandler.size()-1;z++)
		 											 							
			 																{
		 																	
		 																	if((z==verbHandler.size()-1))
		 																	{
		 																		verbHandler.set(z, 'ය');
			 																	verbHandler.set(z-1, 'ී');
			 																	verbHandler.set(z-2,'න');
			 																	verbHandler.add(z-1, '්');
			 																	verbHandler.add(z,'න');
		 					 												
		 																	
		 																	System.out.println("");
	 					 													String  WordToPrint = verbHandler.toString().replaceAll(", |\\[|\\]", "");
	 					 													System.out.println("befre phrase  "+phrase);
	 					 													System.out.println("befre verbToArrangeMat  "+verbToArrangeMat);
	 					 													//System.out.println(sinhalaWordList);
	 					 											
	 					 													for(int index=0;index<=phrase.size()-1;index++ )
	 					 														{
	 					 															if(phrase.get(index).equals(verbToArrangeMat))
	 					 															{
	 					 																phrase.set(index, WordToPrint);
	 					 																System.out.println("after ඔහු "+phrase);
	 					 																break;
	 					 															}
	 					 														}break;
			 																}
			 																}
			 															}
			 																else if(type=='m')
			 																{
			 																	for( z=0; z<=verbHandler.size()-1;z++)
			 											 							
				 																{
			 																	
			 																	if((z==verbHandler.size()-1))
			 																	{
			 																		verbHandler.set(z, 'ය');
			 					 													verbHandler.set(z-1, 'ේ');
			 					 													verbHandler.set(z-2,'න');
			 					 													verbHandler.add(z-1, '්');
			 					 													verbHandler.add(z,'න');
			 					 												
			 																	
			 																	System.out.println("");
		 					 													String  WordToPrint = verbHandler.toString().replaceAll(", |\\[|\\]", "");
		 					 													System.out.println("befre phrase  "+phrase);
		 					 													System.out.println("befre verbToArrangeMat  "+verbToArrangeMat);
		 					 													//System.out.println(sinhalaWordList);
		 					 											
		 					 													for(int index=0;index<=phrase.size()-1;index++ )
		 					 														{
		 					 															if(phrase.get(index).equals(verbToArrangeMat))
		 					 															{
		 					 																phrase.set(index, WordToPrint);
		 					 																System.out.println("after ඔහු "+phrase);
		 					 																break;
		 					 															}
		 					 														}break;
				 																}
				 																}
			 																}
			 																else if(type=='i')
			 																{	
			 																	
			 																	for( z=0; z<=verbHandler.size()-1;z++)
			 											 							
				 																{
			 																	
			 																	if((z==verbHandler.size()-1))
			 																	{
			 																		if(verbHandler.get(z-3).equals('්'))
			 																		{
			 																			verbHandler.set(z-3, 'ි');
			 																			System.out.println("sinhalaWordList2");
						 																
			 																		}
			 																	verbHandler.remove(z);
			 																	verbHandler.set(z-1, 'ි');
			 																	verbHandler.set(z-2,'ය');
			 																	
			 																	System.out.println("");
		 					 													String  WordToPrint = verbHandler.toString().replaceAll(", |\\[|\\]", "");
		 					 													System.out.println("befre phrase  "+phrase);
		 					 													System.out.println("befre verbToArrangeMat  "+verbToArrangeMat);
		 					 													//System.out.println(sinhalaWordList);
		 					 											
		 					 													for(int index=0;index<=phrase.size()-1;index++ )
		 					 														{
		 					 															if(phrase.get(index).equals(verbToArrangeMat))
		 					 															{
		 					 																phrase.set(index, WordToPrint);
		 					 																System.out.println("after ඔහු "+phrase);
		 					 																break;
		 					 															}
		 					 														}break;
				 																}
				 																}
			 																}}
			 													}	
			 														}
			 										 			//}
			 										 		//}
			 										 	//}
			 										 //} 
			 									//}
			 									}}}}
		 						System.out.println("verbFinalizer output: "+phrase);
			 										 						
			 									return phrase;  
			 									}
	}
