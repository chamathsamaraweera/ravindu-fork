package uom.bravehearts.morphology.Verbs;

import java.util.ArrayList;
import java.util.List;
import uom.bravehearts.passiveVoice.PastTense;
import uom.bravehearts.passiveVoice.PassiveIdentify;
import uom.bravehearts.passiveVoice.PassiveSimplePast;


public class SetPastTense {
	
	SubjectIdentifier idnty=new SubjectIdentifier();
	List<String> tenseMaker(List<String> phrase )
	{
		int count=0;
		int countNxt=1;
		String wordToChange="";
		int z;
		String WordToPrint;
		int index;
		String verb;
	System.err.println("VerbFinalizer for SetPastTense executed...");
	System.out.println("SetPastTense input: "+phrase);
	{
		
						for(int x=0; x<=phrase.size()-1;x++)
						{			
		 			
								String verbToArrange="";// set past tense for all continuous tense verbs - දුවමින් කරමින් පනිමින් යමින්
								ArrayList<Character> verbPast=new ArrayList<Character>();				
						 		if((phrase.get(x).equals("VP"))&&(x+2<=phrase.size())&&(phrase.get(x+1).equals("VBG")))
						 			{
						 			verb=(phrase.get(x+2).trim());
						 			verbToArrange=((phrase.get(x+2).trim()));
						 			System.out.println("verbToArrange"+(phrase.get(x+2).trim())); 
						 			//get the verb
						 			//change the base verb into past tense
						 			for(z=0; z<verbToArrange.length(); z++)
						 				{
						 				verbPast.add(verbToArrange.charAt(z));
						 					
						 				}
						 			
					 			for(z=0; z<verbToArrange.length(); z++)	
		 						{
					 				if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&((verbPast.get(z-2).equals('න'))||(verbPast.get(z-2).equals('ණ'))))
					 						{
					 					verbPast.set(z,'්');
					 					verbPast.add(z,'න');
					 					verbPast.set(z-1,'ි');
					 					verbPast.set(z-2,'ම');
					 					
											System.out.println("");
											WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
 									
												for( index=0;index<=phrase.size()-1;index++ )
												{
													if	(phrase.get(index).equals(verb))
													{	phrase.set(index, WordToPrint);
													}
												}
					 					}
		 						}
					 				
						 	}	
					 				 
		 			
						 						
						 		if((phrase.get(x).equals("VP"))&&(x+2<=phrase.size())&&(phrase.get(x+1).equals("VBD")))//	CHANGE THE PASSIVE VERB
						 			{
						 			verb=(phrase.get(x+2).trim());
						 			verbToArrange=((phrase.get(x+2).trim()));
						 			if(PassiveIdentify.boolPassive){
						 				verbToArrange = PastTense.pastMorphology((ArrayList<String>) phrase,x);
						 				verb = PastTense.pastMorphology((ArrayList<String>) phrase,x);
						 			
						 				//	RESOLVE NULL WORD IN SET PAST TENSE
						 				if(verbToArrange == null)
						 				{
						 					verb = (phrase.get(x+2).trim());
						 					verbToArrange = (phrase.get(x+2).trim());
						 				}
//						 				
						 				phrase = PassiveSimplePast.modifyPssivePastOutput(phrase,x);
						 			}
						 			System.out.println("verbToArrange"+verbToArrange); 
						 			
		 			
		 			//change the base verb into past tense
		 			for(z=0; z<verbToArrange.length(); z++)
		 				{
		 					verbPast.add(verbToArrange.charAt(z));
		 					
		 				}
		 			
		 			
		 			for(z=0; z<verbToArrange.length(); z++)	
						{
		 				char last = verbPast.get(z);
		 				if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-2).equals('න')))
		 						{
		 				if((verbPast.get(z-3).equals('ර'))&&(verbPast.get(z-4).equals('ක')))
		 				{//කරනවා
		 					verbPast.set(z-1,'ළ');
	 						verbPast.remove(z-2);
	 						verbPast.remove(z-3);
	 						
	 						System.out.println("");
								WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
									for( index=0;index<=phrase.size()-1;index++)
									{
										if	(phrase.get(index).trim().equals(verb))
										{	
											phrase.set(index, WordToPrint);
											System.out.println(phrase);
										}
									}
								
		 				}
		 				
		 				
		 				else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-3).equals('ෙ')))
		 						&&!(verbPast.get(z-4).equals('ද')))
		 				{//නගනවා
		 					
		 					
		 					verbPast.remove(z-1);
	 						verbPast.set(z-3,'ු');
	 						System.out.println("");
								WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
									for( index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
		 				}
		 				
		 				
		 				else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-3).equals('ග'))&&(verbPast.get(z-4).equals('න')))
		 				{//නගනවා
		 					verbPast.set(z-2,'ු');
	 						verbPast.add(z-3,'ැ');
	 						System.out.println("");
								WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
									for( index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
		 				}
		 				
		 				else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-3).equals('ා'))&&(verbPast.get(z-4).equals('ප')))
		 				{//පානවා
		 					verbPast.set(z-2,'ෑ');
	 						System.out.println("");
								WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
									for( index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
		 				}
		 				
		 			/*	else if((verbPast.get(z-3).equals('ව')))
		 				{//වුනා
		 					verbPast.remove(z-1);
		 					verbPast.add(z-3,'ු');
							System.out.println("");
								String WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
									for(int index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
		 				}*/
		 				
		 				
		 				else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-3).equals('ෙ'))&&(verbPast.get(z-4).equals('ව')))
		 				{//වුනා
		 					verbPast.remove(z-1);
		 					verbPast.set(z-3,'ු');
							System.out.println("");
								WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
									for( index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
		 				}
		 				
		 				
		 				
		 				
		 				else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-3).equals('්'))&&(verbPast.get(z-4).equals('න'))&&(verbPast.get(z-5).equals('ග')))
		 				{//තනනවා
		 					verbPast.set(z-1,'ත');
		 					verbPast.remove(z-2);
		 					verbPast.set(z-4,'ත');
		 					
							System.out.println("");
								WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
									for( index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).equals(verb))
										{	
											phrase.set(index, WordToPrint);
										}
									}
		 				}
		 				
		 				else if((verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-3).equals('ි'))&&(verbPast.get(z-4).equals('ට'))&&(verbPast.get(z-5).equals('ි'))&&(verbPast.get(z-6).equals('ස')))
		 				{//සිටිනවා
		 					verbPast.set(z-1,'ය');
		 					verbPast.remove(z-2);
		 					System.out.println("");
								WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
									for( index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
		 				}
		 				
		 				else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-3).equals('ෙ'))&&(verbPast.get(z-4).equals('ද')))
		 				{//දෙනවා
		 					verbPast.set(z-1,'න');
		 					verbPast.add(z-1,'්');
		 					verbPast.set(z-3,'ු');
		 					System.out.println("");
								WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
									for( index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).trim().equals(verb))
										{	
											phrase.set(index, WordToPrint);
										}
									}
		 				}
		 					break;
						}
						
		 			
		 			
		 			
		 			
						else if(verbPast.size()==4)
		 						{System.out.println("Size 4");
			 						for(z=0; z<verbToArrange.length(); z++)	
			 						{
			 								
			 							if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-3).equals('ය')))		//යනවා
			 							{
		 								verbPast.set(z-1,'ය');
		 								verbPast.set(z-2,'ි');
		 								verbPast.set(z-3,'ග');
		 								
		 								System.out.println("");
										WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
									
											for( index=0;index<=phrase.size()-1;index++ )
											{
												if	(phrase.get(index).equals(verb))
												{	phrase.set(index, WordToPrint);
												}
											}
			 							}
		 							
			 							if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-3).equals('ව')))		//යනවා
			 							{
		 								verbPast.set(z-2,'ූ');
		 								System.out.println("");
										WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
									
											for( index=0;index<=phrase.size()-1;index++ )
											{
												if	(phrase.get(index).equals(verb))
												{	phrase.set(index, WordToPrint);
												}
											}
		 								}
			 							
			 							else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-3).equals('එ')))		//එනවා
			 							{
			 							verbPast.remove(z-2);
			 							verbPast.set(z-3,'ආ');
			 							System.out.println("");
										WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
									
											for( index=0;index<=phrase.size()-1;index++ )
											{
												if	(phrase.get(index).equals(verb))
												{	phrase.set(index, WordToPrint);
												}
											}
			 							}
	 							
			 							
			 							
			 							else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))		//කනවා
			 							{verbPast.set(z-2,'ෑ');
			 							System.out.println("");
										WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
									
											for( index=0;index<=phrase.size()-1;index++ )
											{
												if	(phrase.get(index).equals(verb))
												{	phrase.set(index, WordToPrint);
												}
											}
			 								
			 							}	
			 							
			 							
			 							
			 							
			 						}break;
			 					}
			 							
		 					
		 				else if(verbPast.size()==5)
	 						{	System.out.println("Size 5");
		 						for(z=0; z<verbToArrange.length(); z++)
		 							{ //දෙනවා
	 								if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&(verbPast.get(1).equals('ෙ')&&(verbPast.get(0).equals('ද'))))
										{
											WordToPrint ="දුන්නා" ;
									
											for( index=0;index<=phrase.size()-1;index++ )
											{
												if	(phrase.get(index).trim().equals(verb))
												{	phrase.set(index, WordToPrint);
												}
											}break;
										}
	 								
	 								if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&(verbPast.get(1).equals('ෙ')&&(verbPast.get(0).equals('ව'))))
										{//වෙනවා
											WordToPrint ="වුණා ";
									
											for( index=0;index<=phrase.size()-1;index++ )
											{
												if	(phrase.get(index).equals(verb))
												{	phrase.set(index, WordToPrint);
												}
											}
										}
	 								if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&(verbPast.get(1).equals('බ')&&(verbPast.get(0).equals('ත'))))
									{//තබනවා
										WordToPrint ="තැබුවා ";
								
										for( index=0;index<=phrase.size()-1;index++ )
										{
											if	(phrase.get(index).equals(verb))
											{	phrase.set(index, WordToPrint);
											}
										}
									}
	 								
	 								if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&(verbPast.get(1).equals('ර')&&(verbPast.get(0).equals('ක'))))
									{//කරනවා
										WordToPrint ="කෙරුවා";
								
										for( index=0;index<=phrase.size()-1;index++ )
										{
											if	(phrase.get(index).equals(verb))
											{	phrase.set(index, WordToPrint);
											}
										}
									}
	 								
	 								
	 								else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')&&(verbPast.get(z-3).equals('ල'))))		//ලනවා
		 							{verbPast.set(z-2,'ෑ');
		 							System.out.println("");
									WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
								
										for( index=0;index<=phrase.size()-1;index++ )
										{
											if	(phrase.get(index).equals(verb))
											{	phrase.set(index, WordToPrint);
											}
										}
		 								
		 							}	
		 							
	 								else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&((verbPast.get(1).equals('ා')))&&((!(verbPast.get(1).equals('ෙ'))||(verbPast.get(0).equals('ේ'))||(verbPast.get(0).equals('ෝ'))||(verbPast.get(0).equals('ෞ'))||(verbPast.get(0).equals('ො')))))	
											{
											
											verbPast.set(z-2,'ෑ');
											verbPast.remove(z-3);
											System.out.println("");
											WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
 									
												for( index=0;index<=phrase.size()-1;index++ )
												{
													if	(phrase.get(index).equals(verb))
													{	phrase.set(index, WordToPrint);
													}
												}
											}
 									
	 								
	 								
	 								
	 								
	 								else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&((verbPast.get(1).equals('ේ'))))
	 								{//පේනවා 
	 									if(verbPast.get(0).equals('ප'))
	 									{
	 										verbPast.set(z-3,'ෙ');
 											verbPast.set(z-1,'න');
 											verbPast.add(z-1,'ු');
 											
 											System.out.println("");
 											WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
	 									
 												for( index=0;index<=phrase.size()-1;index++ )
 												{
 													if	(phrase.get(index).equals(verb))
 													{	phrase.set(index, WordToPrint);
 													}
 												}
	 									}
	 									if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(0).equals('ග')))
	 									{//ගේනවා
	 										verbPast.add(z-1,'ා');
 											verbPast.set(z-3,'ෙ');
 											
 											System.out.println("");
 											WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
	 									
 												for( index=0;index<=phrase.size()-1;index++ )
 												{
 													if	(phrase.get(index).equals(verb))
 													{	phrase.set(index, WordToPrint);
 													}
 												}
	 									}
	 								}
	 								
	 								
	 								else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&((verbPast.get(1).equals('ො'))))
										{//බොනවා  
	 									if(verbPast.get(0).equals('බ'))
	 									{
	 										verbPast.add(z-1,'ු');
	 										verbPast.set(z-2,'ව');
	 										verbPast.set(z-3,'ි');
 											System.out.println("");
 											WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
	 									
 												for( index=0;index<=phrase.size()-1;index++ )
 												{
 													if	(phrase.get(index).equals(verb))
 													{	phrase.set(index, WordToPrint);
 													}
 												}
	 									}
										}
						 						
											else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&((verbPast.get(z-4).equals('ඉ'))||(verbPast.get(z-4).equals('උ'))||(verbPast.get(z-4).equals('එ'))||(verbPast.get(z-4).equals('ඔ'))))
											{//උයනවා
												System.out.println("iiiiiiiiiiii");
												if((verbPast.get(z-4).equals('උ')))
												{
													verbPast.set(z-2,'ු');
						 						verbPast.set(z-4,'ඉ');
					 							System.out.println("");
					 							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
					 									
					 								for( index=0;index<=phrase.size()-1;index++ )
					 								{
					 									if	(phrase.get(index).equals(verb))
					 									{	phrase.set(index, WordToPrint);
					 								}
					 							}
												}
												
												else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-4).equals('ඔ')))
												{//ඔතනවා
													verbPast.set(z-2,'ු');
						 						verbPast.set(z-4,'එ');
					 							System.out.println("");
					 							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
					 									
					 								for( index=0;index<=phrase.size()-1;index++ )
					 								{
					 									if	(phrase.get(index).equals(verb))
					 									{	phrase.set(index, WordToPrint);
					 								}
					 							}
												}
												
												else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-4).equals('ඉ'))||(verbPast.get(z-4).equals('එ')))
												{//ඉරනවා‚එලනවා
													verbPast.set(z-2,'ු');
						 						System.out.println("");
					 							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
					 									
					 								for( index=0;index<=phrase.size()-1;index++ )
					 								{
					 									if	(phrase.get(index).equals(verb))
					 									{	phrase.set(index, WordToPrint);
					 								}
					 							}
												}
											}
											if (((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&(!(verbPast.get(1).equals('ෙ'))||(verbPast.get(0).equals('ේ'))||(verbPast.get(0).equals('ෝ'))||(verbPast.get(0).equals('ෞ'))||(verbPast.get(0).equals('ො')))&&((!(verbPast.get(1).equals('ෙ'))||(verbPast.get(0).equals('ේ'))||(verbPast.get(0).equals('ෝ'))||(verbPast.get(0).equals('ෞ'))||(verbPast.get(0).equals('ො')))))	
		 										
											{//අහනවා
												if(verbPast.get(z-4).equals('අ'))
												{
													verbPast.set(z-2,'ු');
							 						verbPast.set(z-4,'ඇ');
							 						
						 							System.out.println("");
						 							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
						 									
						 								for( index=0;index<=phrase.size()-1;index++ )
						 								{
						 									if	(phrase.get(index).equals(verb))
						 									{	phrase.set(index, WordToPrint);
						 									}
						 								}
						 							}
												
												
												else{	
											//නටනවා,මහනවා‚බලනවා, වහනවා
			 							verbPast.set(z-2,'ු');
				 						verbPast.add(z-3,'ැ');
			 							System.out.println("");
			 							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
			 									
			 								for( index=0;index<=phrase.size()-1;index++ )
			 								{
			 									if	(phrase.get(index).trim().equals(verb))
			 									{	phrase.set(index, WordToPrint);
			 									}
			 								}
			 							}
											}
	 									
			 						}break;
					 			}
		 						
		 					
		 				
		 					
		 					
			 				else if(verbPast.size()==6)
 								{	System.out.println("Size 6");
			 					for(z=0; z<verbToArrange.length(); z++)
			 					{//SET 01
			 						if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&((verbPast.get(1).equals('ූ'))))
	 								{//සූරනවා
			 							
			 						verbPast.set(z-2,'ු');
			 						verbPast.set(z-2,'ී');
								
			 						System.out.println("");
			 						WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
									for( index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
								}
			 					
			 						
			 						else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&((verbPast.get(z-5).equals('න'))&&((verbPast.get(z-3).equals('ඹ')))))
	 								{//සූරනවා
			 							
			 						verbPast.set(z-2,'ු');
			 						verbPast.add(z-4,'ැ');
								
			 						System.out.println("");
			 						WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
									for( index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
								}
			 						
			 						else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&((verbPast.get(1).equals('ෙ'))||((verbPast.get(1).equals('ේ'))||((verbPast.get(1).equals('ි'))||((verbPast.get(1).equals('ී')))))))
	 								{//බෙදනවා,පෙලනවා || වේලනවා,බේරනවා || කියනවා,ලියනවා ||පීනනවා, පීරනවා
			 							
			 							verbPast.set(z-2,'ු');
								
			 							System.out.println("");
			 							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
			 							for( index=0;index<=phrase.size()-1;index++ )
			 							{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
								}
			 						
			 						else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&((verbPast.get(1).equals('ෝ'))))
	 								{//සෝදනවා,තෝරනවා
			 							
			 							verbPast.set(z-2,'ු');
			 							verbPast.set(z-5,'ේ');
								
			 							System.out.println("");
			 							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
			 							for( index=0;index<=phrase.size()-1;index++ )
			 							{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
								}
			 						
			 						else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&((verbPast.get(1).equals('ා'))))
	 								{//හාරනවා,පාරනවා
			 							
			 							verbPast.set(z-2,'ු');
			 							verbPast.set(z-4,'ෑ');
								
			 							System.out.println("");
			 							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
			 							for( index=0;index<=phrase.size()-1;index++ )
			 							{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
								}
			 						
			 						else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&((verbPast.get(1).equals('ු'))))
			 								{//දුවනවා, බුරනවා
			 							verbPast.set(z-2,'ු');
			 							verbPast.set(z-4,'ි');
			 							System.out.println("");
			 							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
									
											for( index=0;index<=phrase.size()-1;index++ )
											{
												if	(phrase.get(index).trim().equals(verb))
												{	
													phrase.set(index, WordToPrint);
												}
											}
										}
			 						

			 						
			 						if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&((!(verbPast.get(1).equals('ෙ'))||(verbPast.get(0).equals('ේ'))||(verbPast.get(0).equals('ෝ'))||(verbPast.get(0).equals('ෞ'))||(verbPast.get(0).equals('ො')))))	
			 							{//ගනිනවා ‚ බනිනවා‚බහිනවා
			 							char letter=verbPast.get(1);
			 							
			 							if(((verbPast.get(2).equals('ි'))))
			 							{
			 							if((verbPast.get(0).equals('උ'))||(verbPast.get(0).equals('ඔ')))
											{
												if((verbPast.get(0).equals('උ')))
												{
													verbPast.set(z-2,'ු');
						 						verbPast.set(z-4,'ඉ');
					 							System.out.println("");
					 							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
					 									
					 								for( index=0;index<=phrase.size()-1;index++ )
					 								{
					 									if	(phrase.get(index).equals(verb))
					 									{	phrase.set(index, WordToPrint);
					 								}
					 							}
												}
												
												else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(0).equals('ඔ')))
												{
													verbPast.set(z-2,'ු');
						 						verbPast.set(z-4,'එ');
					 							System.out.println("");
					 							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
					 									
					 								for( index=0;index<=phrase.size()-1;index++ )
					 								{
					 									if	(phrase.get(index).equals(verb))
					 									{	phrase.set(index, WordToPrint);
					 									}
					 								}
													}
											}
												
											else if(letter=='හ')
			 								{//බහිනවා‚කහිනවා‚රහිනවා
			 								verbPast.set(1,'ස');
			 								letter='ස';
			 								
				 							verbPast.set(z-2,letter);
				 							verbPast.remove(z-1);
			 								verbPast.set(z-3,'්');
			 								verbPast.add(z-4,'ැ');
			 								}
			 						
			 								else if((letter=='ඹ')||((letter=='ඳ')))
			 								{
			 								if(letter=='ඳ')
			 								{//අඳිනවා
			 									if(verbPast.get(0).equals('අ'))
			 									{
			 										verbPast.remove(z-1);
			 										verbPast.set(z-2,letter);
					 								verbPast.set(z-3,'්');
					 								verbPast.set(z-4,'න');
				 									verbPast.set(z-5,'ඇ');	
			 									}
			 									
			 									else
			 									{
			 										verbPast.set(z-1,letter);
			 										verbPast.set(z-3,'න');
						 							verbPast.set(z-2,'්');
					 								verbPast.set(z-4,'ැ');
			 									}
			 								}
			 								
			 								
			 								
			 								}
					
		 								else if(letter=='ර')
	 									{
		 									if(verbPast.get(0).equals('අ'))
		 									{//අරිනවා
		 									verbPast.remove(z-1);
		 									verbPast.set(z-2,'ය');
		 									verbPast.set(z-4,'ඇ');
		 									verbPast.remove(z-5);
		 									}
		 									
		 									else
		 									{//හරිනවා
		 									verbPast.set(z-2,'ය');
			 								verbPast.remove(z-1);
			 								verbPast.add(z-4,'ැ');
		 									}
		 								}
		 								
		 								else
		 								{
		 									
		 									if(verbPast.get(0).equals('අ'))
		 									{//අදිනවා,අනිනවා
		 										verbPast.set(z-2,letter);
			 									verbPast.remove(z-1);
			 									verbPast.set(z-3,'්');
			 									verbPast.set(z-5,'ඇ');
			 									
		 									}
		 									
		 									else
		 									{//පදිනවා‚නගිනවා
		 									verbPast.set(z-2,letter);
		 									verbPast.remove(z-1);
		 									verbPast.set(z-3,'්');
		 									verbPast.add(z-4,'ැ');
		 									}
		 								}
		 								
	 									System.out.println("");
	 									WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
	 									
	 										for( index=0;index<=phrase.size()-1;index++ )
	 										{
	 											if	(phrase.get(index).trim().equals(verb))
	 											{	phrase.set(index, WordToPrint);
	 											}
	 										}
	 									}
			 						}
			 						
			 						else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&((verbPast.get(z-3).equals('්'))))
		 							{
			 							if(verbPast.get(0).equals('ද'))
			 							{WordToPrint="දැන‍ගෙන සිටියා";
			 								for( index=0;index<=phrase.size()-1;index++ )
	 										{
	 											if	(phrase.get(index).equals(verb))
	 											{	phrase.set(index, WordToPrint);
	 											}
	 										}
			 							}
			 							
			 							else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&((verbPast.get(z-5).equals('ඉ'))))
			 							{
			 								WordToPrint="සිටියා";
		 								for( index=0;index<=phrase.size()-1;index++ )
 										{
 											if	(phrase.get(index).equals(verb))
 											{	phrase.set(index, WordToPrint);
 											}
 										}}
			 							
			 							else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&((verbPast.get(z-5).equals('ග'))))
			 							{
			 								WordToPrint="ගත්තා";
			 								for( index=0;index<=phrase.size()-1;index++ )
	 										{
	 											if	(phrase.get(index).equals(verb))
	 											{	phrase.set(index, WordToPrint);
	 											}
	 										}
			 							}
			 						}
			 						
			 						
			 						
			 						else if((z==verbPast.size()-1))//&&((!(verbPast.get(z-3).equals('ෙ'))||(verbPast.get(z-3).equals('ේ'))||(verbPast.get(z-3).equals('ෝ'))||(verbPast.get(z-3).equals('ෞ'))||(verbPast.get(z-3).equals('ො'))))&&((!(verbPast.get(z-4).equals('ෙ'))||(verbPast.get(z-4).equals('ේ'))||(verbPast.get(z-4).equals('ෝ'))||(verbPast.get(z-4).equals('ෞ'))||(verbPast.get(z-4).equals('ො'))))))	
				 						{//දඟලනවා,වරදනවා
			 						
			 								if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&((verbPast.get(z-5).equals('ඔ'))))
												{//ඔසවනවා
													verbPast.set(z-2,'ු');
													verbPast.add(z-3,'ෙ');
						 						verbPast.set(z-5,'එ');
					 							System.out.println("");
					 							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
					 									
					 								for( index=0;index<=phrase.size()-1;index++ )
					 								{
					 									if	(phrase.get(index).equals(verb))
					 									{	phrase.set(index, WordToPrint);
					 								}
					 							}
												}
												
												else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&((verbPast.get(z-5).equals('උ'))))
												{
													verbPast.set(z-2,'ු');
													verbPast.add(z-3,'ෙ');
						 						verbPast.set(z-5,'ඉ');
					 							System.out.println("");
					 							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
					 									
					 								for( index=0;index<=phrase.size()-1;index++ )
					 								{
					 									if	(phrase.get(index).equals(verb))
					 									{	phrase.set(index, WordToPrint);
					 									}
					 								}
												}
			 								
			 								
												else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&((verbPast.get(z-5).equals('අ'))))
												{
													verbPast.set(z-2,'ු');
													verbPast.set(z-5,'ඇ');
					 							System.out.println("");
					 							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
					 									
					 								for( index=0;index<=phrase.size()-1;index++ )
					 								{
					 									if	(phrase.get(index).equals(verb))
					 									{	phrase.set(index, WordToPrint);
					 									}
					 								}
												}
												
												else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&((verbPast.get(z-5).equals('ඉ'))||(verbPast.get(z-5).equals('එ'))))
												{//ඉවසනව
													verbPast.set((verbPast.size()-3),'ු');
						 						System.out.println("");
					 							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
					 									
					 								for( index=0;index<=phrase.size()-1;index++ )
					 								{
					 									if	(phrase.get(index).equals(verb))
					 									{	phrase.set(index, WordToPrint);
					 									}
					 								}
												}
			 							 
												else if(z==verbPast.size()-1)
												{//නරඹනවා
													verbPast.set((z-2),'ු');
													verbPast.add((z-4),'ැ');
						 						System.out.println("");
					 							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
					 									
					 								for( index=0;index<=phrase.size()-1;index++ )
					 								{
					 									if	(phrase.get(index).equals(verb))
					 									{	phrase.set(index, WordToPrint);
					 									}
					 								}
												}
											
			 						}
 								}break;
 							}
									
		 		else if(verbPast.size()==7)
		 			
					{	System.out.println("Size 7");
					for(z=0; z<verbToArrange.length(); z++)
					{
						
		 				if(((z==verbPast.size()-1)&&(verbPast.get(z-4).equals('්'))&&(verbPast.get(z).equals('ා'))&&((!(verbPast.get(z-5).equals('ෙ'))||(verbPast.get(z-5).equals('ේ'))||(verbPast.get(z-5).equals('ෝ'))||(verbPast.get(z-5).equals('ෞ'))||(verbPast.get(z-5).equals('ො'))))&&(!(verbPast.get(z-2).equals('ෙ'))||(verbPast.get(z-2).equals('ේ'))||(verbPast.get(z-2).equals('ෝ'))||(verbPast.get(z-2).equals('ෞ'))||(verbPast.get(z-2).equals('ො')))))	
 							
		 					
		 					//දක්වනවා‚බස්වනවා	
		 					{
		 						if(verbPast.get(0).equals('ඔ'))
		 						{
		 						verbPast.set(z-2,'ු');
									//verbPast.add(z-5,'ැ');
		 						verbPast.set(z-5,'එ');
									
									}
		 						
		 						//දක්වනවා‚බස්වනවා	
		 						else if(verbPast.get(0).equals('උ'))
				 				{
				 					verbPast.set(z-2,'ු');
 									verbPast.set(z-5,'ඉ');
 								}
				 						
				 				//දක්වනවා‚බස්වනවා	
		 						else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(0).equals('එ'))||(verbPast.get(0).equals('ඉ')))
						 		{
						 			verbPast.set(z-2,'ු');
 									verbPast.set(z-5,'ඉ');
 								}
						 		
		 						else if(verbPast.get(0).equals('අ'))
						 		{
						 			verbPast.set(z-2,'ු');
 									verbPast.set(z-5,'ඇ');
 								}
						 		
						 		else
						 		{
						 			verbPast.set(z-2,'ු');
 									verbPast.add(z-5,'ැ');
			 						
						 		}
						 		
						 		System.out.println("");
								WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
								
									for( index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
		 						}
		 				else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-4).equals('්'))&&(!(verbPast.get(z-2).equals('ෙ'))||(verbPast.get(z-2).equals('ේ'))||(verbPast.get(z-2).equals('ෝ'))||(verbPast.get(z-2).equals('ෞ'))||(verbPast.get(z-2).equals('ො')))))	
 							
			 				
		 					//හදාරනවා‚වදාරනවා
			 					{
		 						
		 						if(verbPast.get(0).equals('අ'))
						 		{
						 			verbPast.set(z-2,'ු');
 									verbPast.set(z-5,'ඇ');
 								}
						 		
		 						else{
		 						verbPast.set(z-2,'ු');
									verbPast.set(z-4,'ෑ');
									verbPast.add(z-5,'ැ');
		 						

						 		System.out.println("");
								WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
								
									for( index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
		 						}
			 				}
		 				
		 				
		 				else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-5).equals('ි'))&&(!(verbPast.get(z-3).equals('ෙ'))||(verbPast.get(z-3).equals('ේ'))||(verbPast.get(z-3).equals('ෝ'))||(verbPast.get(z-3).equals('ෞ'))||(verbPast.get(z-3).equals('ො')))))	
	 						
	 						//කියවනවා
			 				
	 						{
	 						verbPast.set(z-2,'ු');
							
	 						
	 						System.out.println("");
							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
								for( index=0;index<=phrase.size()-1;index++ )
								{
									if	(phrase.get(index).equals(verb))
									{	phrase.set(index, WordToPrint);
									}
								
								
								}
			 				}
		 				else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-3).equals('ි'))&&(!(verbPast.get(z-5).equals('ෙ'))||(verbPast.get(z-5).equals('ේ'))||(verbPast.get(z-5).equals('ෝ'))||(verbPast.get(z-5).equals('ෞ'))||(verbPast.get(z-5).equals('ො')))))	
	 						
	 						//පවතිනවා
			 				{
	 						if(verbPast.get(0).equals('ඔ'))
	 						{
	 						verbPast.set(z-3,'ු');
							verbPast.add(z-5,'ැ');
	 						verbPast.remove(z-1);
	 						
	 						System.out.println("");
							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
								for( index=0;index<=phrase.size()-1;index++ )
								{
									if	(phrase.get(index).equals(verb))
									{	phrase.set(index, WordToPrint);
									}
								}
								
								}
			 				}
		 				
		 				
		 				
		 				
		 				else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-4).equals('ු'))&&(!(verbPast.get(z-2).equals('ෙ'))||(verbPast.get(z-2).equals('ේ'))||(verbPast.get(z-2).equals('ෝ'))||(verbPast.get(z-2).equals('ෞ'))||(verbPast.get(z-2).equals('ො')))))	
	 						
		 						//අසුරනවා‚වපුරනවා
				 				{
		 						if(verbPast.get(0).equals('ඔ'))
		 						{
		 						verbPast.set(z-2,'ු');
									//verbPast.add(z-5,'ැ');
		 						verbPast.set(z-6,'එ');
		 						
		 						System.out.println("");
								WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
								
									for( index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
									
									}
				 				}
		 						else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-4).equals('්'))&&(verbPast.get(z-6).equals('උ'))&&(!(verbPast.get(z-2).equals('ෙ'))||(verbPast.get(z-2).equals('ේ'))||(verbPast.get(z-2).equals('ෝ'))||(verbPast.get(z-2).equals('ෞ'))||(verbPast.get(z-2).equals('ො')))))	
		 		 					
				 				{
				 					verbPast.set(z-2,'ු');
 									verbPast.set(z-6,'ඉ');
 									
 									System.out.println("");
									WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
									
										for( index=0;index<=phrase.size()-1;index++ )
										{
											if	(phrase.get(index).equals(verb))
											{	phrase.set(index, WordToPrint);
											}
										}
 								}
				 						
		 						else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-4).equals('්'))&&((verbPast.get(z-6).equals('එ'))||(verbPast.get(z-6).equals('ඉ')))&&(!(verbPast.get(z-2).equals('ෙ'))||(verbPast.get(z-2).equals('ේ'))||(verbPast.get(z-2).equals('ෝ'))||(verbPast.get(z-2).equals('ෞ'))||(verbPast.get(z-2).equals('ො')))))	
			 		 					
						 		{
						 			verbPast.set(z-2,'ු');
 									verbPast.set(z-6,'ඉ');
 									
 									System.out.println("");
									WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
									
										for( index=0;index<=phrase.size()-1;index++ )
										{
											if	(phrase.get(index).equals(verb))
											{	phrase.set(index, WordToPrint);
											}
										}
 								}
		 						else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-4).equals('්'))&&(verbPast.get(z-6).equals('අ'))&&(!(verbPast.get(z-2).equals('ෙ'))||(verbPast.get(z-2).equals('ේ'))||(verbPast.get(z-2).equals('ෝ'))||(verbPast.get(z-2).equals('ෞ'))||(verbPast.get(z-2).equals('ො')))))	
				 		 			
						 		{
						 			verbPast.set(z-2,'ු');
 									verbPast.set(z-6,'ඇ');
 									verbPast.set(z-4,'ි');
 									
 									System.out.println("");
									WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
									
										for( index=0;index<=phrase.size()-1;index++ )
										{
											if	(phrase.get(index).equals(verb))
											{	phrase.set(index, WordToPrint);
											}
										}
 								}
						 		
						 	
		 				else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-5).equals('ු')))
		 						//පුරවනවා
		 					{
		 						verbPast.set(z-2,'ු');
		 						verbPast.set(z-5,'ි');
									
						 		System.out.println("");
								WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
								
									for( index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
		 					}
		 					
		 				else	if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&((verbPast.get(z-5).equals('ූ'))))
		 						//සූරවනවා
		 					{
		 						verbPast.set(z-2,'ු');
		 						verbPast.set(z-5,'ී');
									
						 		System.out.println("");
								WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
								
									for( index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
		 						}
		 					
		 					
		 				else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&((verbPast.get(z-5).equals('ො'))))
		 						//සොලවනවා‚හෝදවනවා
		 						{
		 						verbPast.set(z-2,'ු');
		 						verbPast.set(z-5,'ෙ');
									
						 		System.out.println("");
								WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
								
									for( index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
		 						}
		 					
		 				else if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&((verbPast.get(z-5).equals('ෝ'))))
		 						//හෝදවනවා
		 						{
		 						verbPast.set(z-2,'ු');
		 						verbPast.set(z-5,'ේ');
									
						 		System.out.println("");
								WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
								
									for( index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										}
									}
		 						}
							//}
						//}
		 				
		 				else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&((verbPast.get(z-3).equals('ි'))&&((verbPast.get(z-5).equals('ි'))))))
		 				// විඳිනවා‚සිඹිනවා
				 			
						{
						
		 					char letter=verbPast.get(2);
								if(letter=='හ')
								{
									verbPast.set(1,'ස');
									letter='ස';
									verbPast.set(z-2,letter);
									verbPast.remove(z-1);
									verbPast.set(z-3,'්');
									verbPast.add(z-4,'ැ');
								}
							
								else if((letter=='ට'))
								{
									//සිටිනවා
								verbPast.set(z-2,'ය');
		 						verbPast.remove(z-1);
	 							
								}
								
								
								else if((letter=='ඹ')||((letter=='ඳ')))
								{
								if(letter=='ඳ')
									{//විඳිනවා
									verbPast.set(z-2,letter);
		 						verbPast.remove(z-1);
	 							verbPast.set(z-3,'්');
	 							verbPast.set(z-4,'න');
									}
									
								if(letter=='ඹ')
 								{//සිඹිනවා
 									verbPast.set(z-2,letter);
			 						verbPast.remove(z-1);
		 							verbPast.set(z-3,'්');
		 							verbPast.set(z-4,'ම');
		 							
		 							
 								}
								}
								
								
								
								else
									//පිසිනවා‚විදිනවා
								{
									verbPast.set(z-2,letter);
									verbPast.remove(z-1);
									verbPast.set(z-3,'්');
									
								}
	
							System.out.println("");
							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
								for( index=0;index<=phrase.size()-1;index++ )
								{
									if	(phrase.get(index).equals(verb))
									{	phrase.set(index, WordToPrint);
									}
								}
							}
		 				
		 				else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&((verbPast.get(3).equals('ි'))&&((verbPast.get(1).equals('ා')))))
		 				//පාහිනවා
		 				{
		 					verbPast.remove(z-2);
								verbPast.set(z-5,'ෑ');
								verbPast.set(z-3,'ු');
								
								System.out.println("");
							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
								for( index=0;index<=phrase.size()-1;index++ )
								{
									if	(phrase.get(index).equals(verb))
									{	phrase.set(index, WordToPrint);
									}
								}
		 				}	
		 				
		 				
						
						else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&((verbPast.get(1).equals('ෑ'))&&((verbPast.get(3).equals('ෙ')))))
						{//පෑරෙනවා,නෑවෙනවා
							
				verbPast.remove(z-1);
				verbPast.add(z-2,'ු');
				
				System.out.println("");
				WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
			
					for( index=0;index<=phrase.size()-1;index++ )
					{
						if	(phrase.get(index).equals(verb))
						{	phrase.set(index, WordToPrint);
						}
					}
				}
				
						else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&(((verbPast.get(1).equals('ි'))&&((verbPast.get(3).equals('ෙ')))||((verbPast.get(1).equals('ී'))&&((verbPast.get(3).equals('ෙ')))))))
					{//පිරෙනවා || සීරෙනවා
						
			verbPast.remove(z-1);
			verbPast.add(z-2,'ු');
			
			System.out.println("");
			WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
		
				for( index=0;index<=phrase.size()-1;index++ )
				{
					if	(phrase.get(index).equals(verb))
					{	phrase.set(index, WordToPrint);
					}
				}
			}
			
		 				
		 				
						else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&((verbPast.get(1).equals('ැ'))&&((verbPast.get(3).equals('ෙ')))))
						{//තැවෙනවා,නැවෙනවා
							
							verbPast.remove(z-1);
							verbPast.set(z-3,'ු');
							
				
				System.out.println("");
				WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
			
					for( index=0;index<=phrase.size()-1;index++ )
					{
						if	(phrase.get(index).equals(verb))
						{	phrase.set(index, WordToPrint);
						}
					}
				}
						
							
						else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&((!(verbPast.get(1).equals('ෙ'))||(verbPast.get(0).equals('ේ'))||(verbPast.get(0).equals('ෝ'))||(verbPast.get(0).equals('ෞ'))||(verbPast.get(0).equals('ො'))))))	
							 {//කරකවනවා
							
							 verbPast.set(z-2,'ු');
								verbPast.add(z-5,'ැ');
								System.out.println("");
								WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
								
								for( index=0;index<=phrase.size()-1;index++ )
								{
									if	(phrase.get(index).equals(verb))
									{	phrase.set(index, WordToPrint);
									}
									}
								}
 						//}break;
						
		 					
						 		
						 		
						 		else	if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා')))	
								{
			 					if((verbPast.get(1).equals('ෙ'))&&(!(verbPast.get(3).equals('ෙ'))||(verbPast.get(3).equals('ේ'))||(verbPast.get(3).equals('ෝ'))||(verbPast.get(3).equals('ෞ'))||(verbPast.get(3).equals('ො')))&&(!(verbPast.get(4).equals('ේ'))||(verbPast.get(4).equals('ෝ'))||(verbPast.get(4).equals('ෞ'))||(verbPast.get(4).equals('ො'))||(verbPast.get(1).equals('ැ'))||(verbPast.get(1).equals('ි'))));
			 						//තෙරපනවා‚පෙරලනවා || කැඳවනවා  || පියඹනවා‚ලියලනවා ||වේලවනවා
			 					{//System.out.println("!!!!!!!!!!!!!!!!!!!!!55555");
								
			 						verbPast.set(z-2,'ු');
										
							 		System.out.println("");
									WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
									
										for( index=0;index<=phrase.size()-1;index++ )
										{
											if	(phrase.get(index).equals(verb))
											{	phrase.set(index, WordToPrint);
											}
										}
			 					}
								}
						 		
						 		
						 		
		 					
					
		 		else if(verbPast.size()>7)
				{	//මිරිකනවා‚පිහිනනවා
		 			System.out.println("Size <");
				for(z=0; z<verbToArrange.length(); z++)
				{
					if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&((verbPast.get(z-4).equals('ි')))&&(verbPast.get(z-4).equals('ි')))	
						{
						char letter=verbPast.get(1);
							if(letter=='හ')
							{
							verbPast.set(1,'ස');
							letter='ස';
							}
						
						verbPast.set(z-2,'ු');
						//verbPast.remove(z-1);
						//verbPast.set(z-3,'්');
						//verbPast.add(z-4,'ැ');

						System.out.println("");
						WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
						
							for( index=0;index<=phrase.size()-1;index++ )
							{
								if	(phrase.get(index).equals(verb))
								{	phrase.set(index, WordToPrint);
								}
							}
						}
					
					else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-3).equals('ි'))&&(!(verbPast.get(z-5).equals('ෙ'))||(verbPast.get(z-5).equals('ේ'))||(verbPast.get(z-5).equals('ෝ'))||(verbPast.get(z-5).equals('ෞ'))||(verbPast.get(z-5).equals('ො')))))	
 						
 						//පවතිනවා
		 				{
 						if(verbPast.get(0).equals('ඔ'))
 						{
 						verbPast.set(z-3,'ු');
						verbPast.add(z-5,'ැ');
 						verbPast.remove(z-1);
 						
 						System.out.println("");
						WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
						
							for( index=0;index<=phrase.size()-1;index++ )
							{
								if	(phrase.get(index).equals(verb))
								{	phrase.set(index, WordToPrint);
								}
							}
							
							}
		 				}
					
					if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&(verbPast.get(1).equals('ෙ')&&(verbPast.get(0).equals('ද'))))
					{
						WordToPrint ="දුන්නා" ;
				
						for( index=0;index<=phrase.size()-1;index++ )
						{
							if	(phrase.get(index).equals(verb))
							{	phrase.set(index, WordToPrint);
							}
						}break;
					}
					
					if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&(verbPast.get(1).equals('ෙ')&&(verbPast.get(0).equals('ව'))))
					{//වෙනවා
						WordToPrint ="වුණා ";
				
						for( index=0;index<=phrase.size()-1;index++ )
						{
							if	(phrase.get(index).equals(verb))
							{	phrase.set(index, WordToPrint);
							}
						}
					}
					if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&(verbPast.get(1).equals('බ')&&(verbPast.get(0).equals('ත'))))
				{//තබනවා
					WordToPrint ="තැබුවා ";
			
					for( index=0;index<=phrase.size()-1;index++ )
					{
						if	(phrase.get(index).equals(verb))
						{	phrase.set(index, WordToPrint);
						}
					}
				}
					
				
					
					
		 				if((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව'))&&(verbPast.get(z-2).equals('න'))&&(verbPast.get(z-3).equals('ර'))&&(verbPast.get(z-4).equals('ක')))
		 						
		 				{//කරනවා
		 					verbPast.set(z-1,'ළ');
	 						verbPast.remove(z-2);
	 						verbPast.remove(z-3);
	 						
	 						System.out.println("");
								WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
									for( index=0;index<=phrase.size()-1;index++ )
									{
										if	(phrase.get(index).equals(verb))
										{	phrase.set(index, WordToPrint);
										
										}
									}
		 				}	
					
					
					
					
					
					
					if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-1).equals('ව')))&&((verbPast.get(z-4).equals('ු'))||(verbPast.get(z-6).equals('ු'))))	{//පුපුරනවා,ගුගුරනවා
						
					verbPast.set(z-2,'ැ');
					verbPast.set(z-4,'ි');
					verbPast.set(z-6,'ි');

					System.out.println("");
					WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
					
						for( index=0;index<=phrase.size()-1;index++ )
						{
							if	(phrase.get(index).equals(verb))
							{	phrase.set(index, WordToPrint);
							}
						}
					}
					
					else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&(verbPast.get(z-3).equals('ර'))&&(verbPast.get(z-4).equals('ක'))))	
					 {//ඉදිරිපත් කරනවා
						verbPast.remove(z-1);
						verbPast.remove(z-2);
					 	verbPast.set(z-3,'ළ');
						//verbPast.add(z-3,'ැ');
						System.out.println("");
						WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
						
						for( index=0;index<=phrase.size()-1;index++ )
						{
							if	(phrase.get(index).equals(verb))
							{	phrase.set(index, WordToPrint);}
							}
						}
					}
				}
					
					else if(((z==verbPast.size()-1)&&(verbPast.get(z).equals('ා'))&&((!(verbPast.get(0).equals('ෙ'))||(verbPast.get(0).equals('ේ'))||(verbPast.get(0).equals('ෝ'))||(verbPast.get(0).equals('ෞ'))||(verbPast.get(0).equals('ො'))))))	
						 {
							verbPast.remove(z-1);
						 	verbPast.set(z-3,'ු');
							//verbPast.add(z-3,'ැ');
							System.out.println("");
							WordToPrint = verbPast.toString().replaceAll(", |\\[|\\]", "");
							
							for( index=0;index<=phrase.size()-1;index++ )
							{
								if	(phrase.get(index).equals(verb))
								{	phrase.set(index, WordToPrint);}
								}
							}
						}
					}
				}
		 }
				
						}} 
		
				
						
	System.out.println("SetPastTense output : "+phrase);
			return phrase;
		
	}

}
