package uom.bravehearts.morphology.Verbs;

import java.util.ArrayList;
import java.util.List;

import uom.bravehearts.grammer.IsAnimate;
import uom.bravehearts.morphology.CheckFromDB;
import uom.bravehearts.morphology.MorfRule;
import uom.bravehearts.morphology.Passive.PassivePaSi;
import uom.bravehearts.passiveVoice.PassiveIdentify;

public class PastTense implements MorfRule {
	SubjectIdentifier idnty = new SubjectIdentifier();
	IsAnimate chkAnimate = new IsAnimate();
	// CheckFromDB chk=new CheckFromDB();
	WordType wType = new WordType();
	SetPastTense tense = new SetPastTense();
	PassivePaSi passivePasi = new PassivePaSi();

	int count = 0;
	int countNxt = 1;
	String wordToChange = "";
	int wordTochangeIndex = 0;

	public List performRule(List<String> phrase) {

		phrase = tense.tenseMaker(phrase);

		System.out.println("Past Tense executed...");
		System.out.println("Past Tense input: " + phrase);
		{	
			
			String tenseTag = phrase.get(phrase.size()-1).trim();
			
			
			int a=phrase.size();
			for (int x = 0; x <= a - 1; x++) {
				
				if(PassiveIdentify.boolPassive && "PaPaSi".equals(tenseTag)){ 
						
					System.out.println("Passive loop executed");
					phrase = passivePasi.passiveVerbHandler(phrase);
						
					break;
					}
				
				String verbToArrange = "";
				ArrayList<Character> verbHandler = new ArrayList<Character>();
				
				
				if ((phrase.get(x).equals("VP")) && (x + 2 <= a)
						&& (phrase.get(x + 1).equals("VBD"))) {
					
					{	

						
						for (int sub = (x + 2); sub > 0; sub--) {
							if ((phrase.get(sub).equals("sbjBoundry")))

							{
								wordToChange = ((phrase.get(sub - 1)).trim());
								System.out.println("GOT THE OUTPUT IS..........."+ wordToChange);
								break;

							}
						}

						// handling "was" / "were"
						if ((phrase.get(x + 2).equals("was"))
								|| (phrase.get(x + 2).equals("were"))||(phrase.get(x + 2).equals("be"))) {
						
							if (!(phrase.get(a - 1)
									.equals("සිටියා"))
									|| (phrase.get(a - 1)
											.equals("ව සිටියා"))) {
								for (int y = x + 2; y <= a - 1; y++) {

									if ((y+1<=a-1)&&(phrase.get(y + 1).equals("VP"))
											&& (y + 2 <= a)
											&& (phrase.get(y + 2).equals("VBG"))) {
										verbToArrange = "සිටියා";
										count++;
										phrase.add(verbToArrange);
										break;
									}

									else if ((y+1<=a-1)&&(!phrase.get(y).equalsIgnoreCase("before"))&&((phrase.get(y + 1).equals("NP"))|| (phrase.get(y + 1).equals("ADJP"))))

									{
										verbToArrange = "ව සිටියා";
										count++;
										phrase.add(verbToArrange);
										break;
									}
									

									else if ((count == 0)&&(!phrase.get(y-1).equalsIgnoreCase("before"))
											&& ((phrase.get(y).equals("NP") || (phrase
													.get(y).equals("VP") || (phrase
													.get(y).equals("ADVP") || (phrase
													.get(y).equals("RB")))))))

									{
										if (phrase.get(y + 1).equals("VBG")) {
											verbToArrange = "සිටියා";
											count++;
											phrase.add(verbToArrange);
											break;
										}

										else {
											System.out
													.println("-------------------------------------");
											if(!PassiveIdentify.boolPassive){
											verbToArrange = "ව සිටියා";
											count++;
											phrase.add(verbToArrange);
											break;
											}
										}
									}

									else if ((count == 0)
											&& (y == a - 1)&&(!phrase.get(y-2).equals("PRP"))) {//////////////// for Morfbefore 
										if(!PassiveIdentify.boolPassive){///////////////////////////////// passive ////////////////////////////
										verbToArrange = "ව සිටියා";
										count++;
										phrase.add(verbToArrange);
										break;
										}

									}
								}
								break;
							}

						}

						else {
							verbToArrange = (phrase.get(x + 2).trim());

							for (int sub = (x + 2); sub > 0; sub--) {
								if ((phrase.get(sub).equals("sbjBoundry")))

								{
									wordToChange = ((phrase.get(sub - 1))
											.trim());
									System.out
											.println("GOT THE OUTPUT ..........."
													+ wordToChange);
									break;

								}

							}
						}

					}

					if ((phrase.get(x + 2).trim().equals("අවශ්‍යව පවතුනා"))
							|| (phrase.get(x + 2).trim().equals("උවමනා කළා")))

					{
						count = 0;

						for (int n = 0; n <= a - 1; n++) {
							if ((phrase.get(n).equals("NP") || phrase.get(n)
									.equals("VP"))) {
								count++;
							}

							if (count == 2
									&& ((phrase.get(n).equals("NP")) || (count == 2 && ((phrase
											.get(n).equals("VP")))))) {
								wordToChange = ((phrase.get(n - 2)));
								if ((phrase.get(n - 2)).equals("මම")) {
									phrase.set((n - 2), "මට");
									break;
								} else {
									String add = (phrase.get(n - 2));
									phrase.set((n - 2), (add + "ට").trim());
									break;
								}
							}

						}

					}

					for (int z = 0; z < verbToArrange.length(); z++) {
						verbHandler.add(verbToArrange.charAt(z));
					}

					// CHANGE THE VERB WHEN THE SUBJECT IS (I)
					for (int z = 0; z < verbToArrange.length(); z++) {
						if ((z == verbHandler.size() - 1)
								&& (verbHandler.get(z) == 'ා')) {
							if (wordToChange.equals("මම")) {
								// System.out.println("Yes I am");
								verbHandler.set(z, 'ි');
								verbHandler.add(z, 'ම');
								verbHandler.add(z, 'ෙ');

								System.out.println("");
								String WordToPrint = verbHandler.toString()
										.replaceAll(", |\\[|\\]", "");

								for (int index = 0; index <= a - 1; index++) {
									if (phrase.get(index).equals(verbToArrange)) {
										phrase.set(index, WordToPrint);
									}
								}
							} else if (wordToChange.equals("අපි")) {
								verbHandler.set(z, 'ු');
								verbHandler.add(z, 'ම');
								verbHandler.add(z, 'ෙ');

								System.out.println("");
								String WordToPrint = verbHandler.toString()
										.replaceAll(", |\\[|\\]", "");

								for (int index = 0; index <= a - 1; index++) {
									if (phrase.get(index).equals(verbToArrange)) {
										phrase.set(index, WordToPrint);
									}
								}
							} else if (wordToChange.equals("ඔහු")) {
								verbHandler.set(z, 'ය');
								verbHandler.add(z, 'ේ');

								System.out.println("");
								String WordToPrint = verbHandler.toString()
										.replaceAll(", |\\[|\\]", "");

								for (int index = 0; index <= a - 1; index++) {
									if (phrase.get(index).equals(verbToArrange)) {
										phrase.set(index, WordToPrint);
									}
								}
							} else if (wordToChange.equals("ඇය")) {
								verbHandler.set(z, 'ය');
								verbHandler.add(z, 'ා');

								System.out.println("");
								String WordToPrint = verbHandler.toString()
										.replaceAll(", |\\[|\\]", "");

								for (int index = 0; index <= a - 1; index++) {
									if (phrase.get(index).equals(verbToArrange)) {
										phrase.set(index, WordToPrint);

									}
								}
							} else if (wordToChange.equals("ඔවුහු")) {
								verbHandler.set(z, 'ය');
								verbHandler.add(z, 'ෝ');

								System.out.println("");
								String WordToPrint = verbHandler.toString()
										.replaceAll(", |\\[|\\]", "");
								for (int index = 0; index <= a - 1; index++) {
									if (phrase.get(index).equals(verbToArrange)) {
										phrase.set(index, WordToPrint);

									}
								}
							}

							if (!((wordToChange.equals("මම")) || (wordToChange
									.equals("අපි") || (wordToChange
									.equals("ඔහු") || (wordToChange
									.equals("ඇය") || (wordToChange
									.equals("ඔවුහු"))))))) {

								for (int y = (x - 2); y <= (x - 2) && y > 0; y--) {

									if (phrase.get(y).equals("NNS")
											&& (countNxt == 1)) // if noun is
																// plural set
																// subject to
																// plural
									{
										char type = chkAnimate.perform(wordToChange);

										if (type == 'f' || type == 'm') {
											int subIndex = phrase.indexOf("sbjBoundry");
											String changedWord = wType.testWord(wordToChange); // verb
																				// change
																				// to
																				// plural
											phrase.set(subIndex-1, changedWord);
											wordToChange = changedWord;
											countNxt++;

											verbHandler.set(z, 'හ');

											System.out.println("");
											String WordToPrint = verbHandler
													.toString().replaceAll(
															", |\\[|\\]", "");

											for (int index = 0; index <= phrase
													.size() - 1; index++) {
												if (phrase.get(index).equals(
														verbToArrange)) {
													phrase.set(index,
															WordToPrint);

												}
											}
										}
									}

									if (phrase.get(y).equals("NNS")
											&& (countNxt > 1)) // if noun is
																// plural set
																// subject to
																// plural
									{
										phrase = ChangeObjectPlural
												.turnToPlura(phrase);
									}

									if (phrase.get(y).equals("NN")
											&& (countNxt == 1)) {
										// System.out.println("Do nothing....!!!");
										countNxt++;

										char type = chkAnimate
												.perform(wordToChange);

										if (type == 'f') {
											verbHandler.add(z + 1, 'ය');

											System.out.println("");
											String WordToPrint = verbHandler
													.toString().replaceAll(
															", |\\[|\\]", "");

											for (int index = 0; index <= phrase
													.size() - 1; index++) {
												if (phrase.get(index).equals(
														verbToArrange)) {
													phrase.set(index,
															WordToPrint);

												}
											}
										} else if (type == 'm') {
											verbHandler.set(z, 'ය');
											verbHandler.add(z, 'ේ');

											System.out.println("");
											String WordToPrint = verbHandler
													.toString().replaceAll(
															", |\\[|\\]", "");

											for (int index = 0; index <= phrase
													.size() - 1; index++) {
												if (phrase.get(index).equals(
														verbToArrange)) {
													phrase.set(index,
															WordToPrint);

												}
											}
										}

										else if (type == 'i') {
											// do nothing...!!!
											System.out.println("");
											String WordToPrint = verbHandler
													.toString().replaceAll(
															", |\\[|\\]", "");

											for (int index = 0; index <= a- 1; index++) {
												if (phrase.get(index).equals(
														verbToArrange)) {
													phrase.set(index,
															WordToPrint);

												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		System.out.println("Past Tense output :" + phrase);
		return phrase;
	}
}
