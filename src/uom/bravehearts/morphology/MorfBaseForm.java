package uom.bravehearts.morphology;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import uom.bravehearts.grammer.IsAnimate;
import uom.bravehearts.passiveVoice.*;
// WHEN THERE ARE ADJACENT NN TAGS -  NN, මාළු , NN, ව්‍යංඡනයක
public class MorfBaseForm implements MorfRule{

	
	int countNN=1;
		
		public  List performRule(List<String>phrase)
		{ 
			
			{
			//ADD WORDS FOR NP & VP
			
			if(phrase.contains("NN")&&(countNN==1))
			{
				System.err.println("MorfBaseForm executed...");
				System.out.println("MorfBaseForm  input: "+phrase);
				
				System.out.println(phrase.size()-1);
				for(int x=0; x<=phrase.size()-1;x++)
			{
				if((phrase.get(x).equals("NN"))&&((x+1)<phrase.size()-1))
				{ 
					if((phrase.get(x+2).equals("NN")||(phrase.get(x+2).equals("NNS"))))
					{
					
					String baseWord=phrase.get(x+1).trim();
					System.out.println("The baseWord is..."+baseWord);
									
									ArrayList<Character> addBaseToCharArray=new ArrayList<Character>();
									 
									for(int Y=0; Y<=baseWord.length()-1;Y++)
										{	
										addBaseToCharArray.add(baseWord.charAt(Y));
										}
									for(int Y=0; Y<=baseWord.length()-1;Y++)
									{	
										//if base wrd is ජාවා පොතක් then keep it as it is 
										if(baseWord=="ජාවා")
										{	 System.out.println("");
											 String wordToPrint = addBaseToCharArray.toString().replaceAll(", |\\[|\\]", "");
										       phrase.set(x+1,wordToPrint);
										       break;
										}
										
										if((Y==addBaseToCharArray.size()-1) && (addBaseToCharArray.get(Y).equals('ා')) && (addBaseToCharArray.get(Y-1).equals('ව'))&& (addBaseToCharArray.get(Y-2).equals('න')))
										{
											//do't change verbs
											addBaseToCharArray.remove(Y);
											addBaseToCharArray.remove(Y-1);
											 
											 System.out.println("");
											 String wordToPrint = addBaseToCharArray.toString().replaceAll(", |\\[|\\]", "");
										       phrase.set(x+1,wordToPrint);
										       break;
										}
											
										
										
									if((Y==addBaseToCharArray.size()-1) && (addBaseToCharArray.get(Y).equals('්')) && (addBaseToCharArray.get(Y-1).equals('ක')))
										{
								           
											addBaseToCharArray.remove(Y);
											 addBaseToCharArray.remove(Y-1);
											 System.out.println("");
											 String wordToPrint = addBaseToCharArray.toString().replaceAll(", |\\[|\\]", "");
										       phrase.set(x+1,wordToPrint);
										       break;
										}
										
									if((Y==addBaseToCharArray.size()-1) && (addBaseToCharArray.get(Y).equals('ට')) )
									{
							           break;
									}
									
										
										else if((Y==addBaseToCharArray.size()-1) && (addBaseToCharArray.get(Y).equals('ා'))&& (addBaseToCharArray.get(Y-1).equals('ය')))
											 
							            {//අලි
											 
											 addBaseToCharArray.remove(Y);
											 addBaseToCharArray.remove(Y-1);
										 
										 System.out.println("");
										 String wordToPrint = addBaseToCharArray.toString().replaceAll(", |\\[|\\]", "");
									       phrase.set(x+1,wordToPrint);
									       break;
							            }
										
										 else if(Y==addBaseToCharArray.size()-1 && (addBaseToCharArray.get(Y).equals('ා')&&(addBaseToCharArray.get(Y-2).equals('්'))))
										 {//කොලු
											
											 addBaseToCharArray.remove(Y);
											 addBaseToCharArray.remove(Y-1);
											 addBaseToCharArray.set(Y-2,'ු');
											 
								                
											 System.out.println("");
											 String wordToPrint = addBaseToCharArray.toString().replaceAll(", |\\[|\\]", "");
										     phrase.set(x+1,wordToPrint);
										     break;
								            }
										
										else if((Y==addBaseToCharArray.size()-1) && (addBaseToCharArray.get(Y).equals('ා'))&& (!(addBaseToCharArray.get(Y-1).equals('ව'))&& (addBaseToCharArray.get(Y).equals('ා'))&& (addBaseToCharArray.get(Y).equals('ජ'))))
										 
							            {//මිනිස්
											
											addBaseToCharArray.set(Y, '්');
										 
										 System.out.println("");
										 String wordToPrint = addBaseToCharArray.toString().replaceAll(", |\\[|\\]", "");
									       phrase.set(x+1,wordToPrint);
									       break;
							            }
										
									 
									 else if((Y==addBaseToCharArray.size()-1) && ((addBaseToCharArray.get(Y).equals('ය'))||(addBaseToCharArray.get(Y).equals('ව'))))
									 {	//පුටු,මේස
										 addBaseToCharArray.remove(Y);
										 
							                
										 System.out.println("");
										 String wordToPrint = addBaseToCharArray.toString().replaceAll(", |\\[|\\]", "");
									     phrase.set(x+1,wordToPrint);
									     break;
							            }
										
									 else if((Y==addBaseToCharArray.size()-1) && ((addBaseToCharArray.get(Y).equals('ා'))&&(addBaseToCharArray.get(Y-1).equals('ය'))&&(addBaseToCharArray.get(Y-2).equals('ර'))&&(addBaseToCharArray.get(Y-3).equals('ව'))))
											
									 {//ගුරුවරු
							            	
										 addBaseToCharArray.remove(Y);
										 addBaseToCharArray.remove(Y-1);
										 addBaseToCharArray.remove(Y-2);
										 addBaseToCharArray.remove(Y-3);
										 
							                
										 System.out.println("");
										 String wordToPrint = addBaseToCharArray.toString().replaceAll(", |\\[|\\]", "");
									     phrase.set(x+1,wordToPrint);
									     break;
							            }
										
									
										
									 else if(Y==addBaseToCharArray.size()-1 && (!(addBaseToCharArray.get(Y).equals('ට'))) 
											 && (!((addBaseToCharArray.get(Y).equals('්')||addBaseToCharArray.get(Y).equals('ා')||addBaseToCharArray.get(Y).equals('ැ')||addBaseToCharArray.get(Y).equals('ෑ')||addBaseToCharArray.get(Y).equals('ි')||addBaseToCharArray.get(Y).equals('ී')||addBaseToCharArray.get(Y).equals('ු')||addBaseToCharArray.get(Y).equals('ූ')))))
									 {//මල්, ගස්
										 addBaseToCharArray.add(Y+1,'්');
										 
							                
										 System.out.println("");
										 String wordToPrint = addBaseToCharArray.toString().replaceAll(", |\\[|\\]", "");
									     phrase.set(x+1,wordToPrint);
									     break;
							            }
										
									
									}}}}

					
				System.out.println("Base Frm  output: "+phrase);
				countNN++;}
			}
			
			
//			if (PassiveIdentify.boolPassive) {
//				phrase = PassiveSinguPlural.HandlePlural((ArrayList<String>) phrase);
//			}
			
			System.out.println("MorfBaseForm  output: "+phrase);
			return phrase;
			
		}

		
		

}
