package uom.bravehearts.morphology;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uom.bravehearts.morphology.Prepositions.MorfAt;
import uom.bravehearts.morphology.Prepositions.MorfBefore;
import uom.bravehearts.morphology.Prepositions.MorfEvery;
import uom.bravehearts.morphology.Prepositions.MorfIn;
import uom.bravehearts.morphology.Prepositions.MorfOn;
import uom.bravehearts.morphology.Prepositions.MorfPreAbout;
import uom.bravehearts.morphology.Prepositions.MorfPreAfter;
import uom.bravehearts.morphology.Prepositions.MorfPreTo;
import uom.bravehearts.morphology.Prepositions.MorfWhen;
import uom.bravehearts.morphology.Prepositions.MorfWith;
import uom.bravehearts.morphology.Verbs.BoundryHandler;
import uom.bravehearts.morphology.Verbs.FutureTense;
import uom.bravehearts.morphology.Verbs.PastTenseFinerlizer;
import uom.bravehearts.morphology.Verbs.PresentContinuous;
import uom.bravehearts.morphology.Verbs.VerbFinalizer;
import uom.bravehearts.morphology.Verbs.PastTense;

public class MorfAdmin {
	
	MorfDeterminant deterninent=new  MorfDeterminant();
	VerbFinalizer finalizer=new VerbFinalizer();
	PastTense past=new PastTense();
	MorfBaseForm Base=new MorfBaseForm();
	PresentContinuous presCnti=new PresentContinuous();
	FutureTense simpleFuture = new FutureTense();
	MorfIn PreIN = new MorfIn();
	MorfPreTo PreTo = new MorfPreTo();
		public List<List>morfadminPerform(List<List> input){
			
			int outerCount= input.size();
			
			for(int x=0;x<outerCount;x++){
				
				List<String>phrase= input.get(x);//	LOOP THROUGH ALL THE CANDIDATE SENTENCES
				System.out.println("MorfAdmin.java input: "+phrase);
		
		
		Map<String, Object> checkAll = new HashMap<String, Object>();
		//	REPLACE PREVIOUS TAGS WITH NEW TAGS
		checkAll.put("a", deterninent);
		checkAll.put("an", deterninent);
		checkAll.put("VBP", finalizer);
		checkAll.put("VBZ", finalizer);
		checkAll.put("VBGi", presCnti);
		checkAll.put("VBG", presCnti);
		checkAll.put("VBD", past);
		//checkAll.put("VBN", past);
		checkAll.put("will", simpleFuture);
		checkAll.put("shall", simpleFuture);
		checkAll.put("in", PreIN);
		checkAll.put("TO", PreTo);
		checkAll.put("NN", new MorfBaseForm());
		checkAll.put("CD", new Numbers());
		checkAll.put("NNS", new BoundryHandler());
		checkAll.put("on", new MorfOn());
		checkAll.put("with", new MorfWith());
		checkAll.put("at", new MorfAt());
		checkAll.put("At", new MorfAt()); ///////   ???????????????????????????????? is this correct ??????????????????
		checkAll.put("every", new MorfEvery());
		checkAll.put("when", new MorfWhen());
		checkAll.put("after", new MorfPreAfter());
		checkAll.put("before", new MorfBefore());
		checkAll.put("Before", new MorfBefore());
		
	MorfRule refernce; ///////////////////////// reference object \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	
	
	 List<String>manipulateList= new ArrayList<String>();
	 manipulateList.addAll(phrase);
	 
	 int loopCount=phrase.size();
	 
	for(int y=0; y<phrase.size();y++)//	REMOVING EMPTH PHRASES
	{
		if(phrase.get(y).equals(""))
		{
			phrase.remove(y);
		}
	}
		
	 for(int z=0;z<loopCount;z++)
	 {
		 String word=phrase.get(z);
		 refernce=(MorfRule) checkAll.get(word); //////////////// each word is checked with checkAll hash map
		 if(refernce !=null)
		
		 {
		manipulateList = refernce.performRule( phrase);

		loopCount=phrase.size();
		
	 }	
	 }
	

		System.out.println("Final output:"+manipulateList); //manipulateList
	 	
			}
			
			for(List<String>ml:input){
				
				for(int x=0;x<ml.size();x++){
					
					if("VBGi".equals(ml.get(x))){
						
						ml.remove(x);
						x--;
					}
					
				}
				
			}
			
	 return input;
	 } ////////////////////////////////////////// morfadminPerform   ends \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		
		
		public static void main(String[]args){
			
			ArrayList<List> outer = new ArrayList<List>();
			ArrayList<String> inner = new ArrayList<String>();
			  
		
			
			
			outer.add(inner);
			
			// inner = new ArrayList<String>();
			MorfAdmin test= new MorfAdmin();
			List<List>output=	test.morfadminPerform(outer);
			
			
		}
		
	

}

