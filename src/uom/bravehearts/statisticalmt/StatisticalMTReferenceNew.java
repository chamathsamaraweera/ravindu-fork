package uom.bravehearts.statisticalmt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StatisticalMTReferenceNew {

	public static void main(String args[]) {
		StatisticalMTReferenceNew stReference = new StatisticalMTReferenceNew();
 
		ArrayList<String> newArryLt=new ArrayList<String>();
		newArryLt.add("නිමල්ට ජාවා පොතක් අවශ්‍යයි");
		newArryLt.add("නිමල්ට ජාවා කලින් වෙන්කිරීමක් අවශ්‍යයි");

		System.out.println("Startingg... ");

		String outComeSentence=stReference.sentenceFinalizer(newArryLt);
		System.out.println("Out come: "+outComeSentence);
			
	}

	public String sentenceFinalizer(List aListVal) {
		double maxProbablity=0;
		int keyValue = 0;
		int avrTime=100/(aListVal.size());
		StringBuffer sb = new StringBuffer("");
		Map sentenceSet = new HashMap();
		ArrayList<String> wds1 = new ArrayList<String>();
		ArrayList<String> wds2 = new ArrayList<String>();
		OntologyReference refer = new OntologyReference();
	    CorpusRetriveNew cprNew= new CorpusRetriveNew();
		
		for(int i=1;i<=aListVal.size();i++){
			sentenceSet.put(i,aListVal.get(i-1));}

		for (int t = 1; t <= sentenceSet.size(); t++) {	
			/* Check sentence probability with corpus */
			try {

				double x1=cprNew.corpusRetriveDemo(aListVal.get(t-1).toString(),avrTime,t);
				/* Max Probability Senetence assign */
				if(x1>maxProbablity){
					maxProbablity=x1;
					keyValue=t;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			sb.setLength(0);
		}
            if(keyValue!=0){
		 return (String) sentenceSet.get(keyValue);}
                       else{
	     return (String) sentenceSet.get(1);
                           }

	}
	/* Split base on space */
	public ArrayList<String> sentenceSpliter(String sentence) {
		ArrayList<String> wordArrayList = new ArrayList<String>();
		wordArrayList.clear();
		for (String word : sentence.split("\\s+")) {
			wordArrayList.add(word);
		}
		return wordArrayList;
	}
	/* Corrected sentence insert */
	public String sentenceInsert(String sentence) {
		CorpusRetriveNew cprNew= new CorpusRetriveNew();
		String outMs=cprNew.corpusSentenceInsert(sentence);
		return outMs;
	}

}
