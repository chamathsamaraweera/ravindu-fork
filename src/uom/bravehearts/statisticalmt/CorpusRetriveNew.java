package uom.bravehearts.statisticalmt;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import uom.bravehearts.connection.DatabaseConnection;

public class CorpusRetriveNew {
	
	DatabaseConnection dbc;

	//Ravindu and sameera are testing comments
	

	public CorpusRetriveNew(){
		
	    dbc=new DatabaseConnection(); //creating a instance of DatabSaseConnection class
		dbc.conOpen("jdbc:mysql://localhost/BraveHearts2?characterEncoding=UTF-8","root",""); //params- db url ,uname,pw
	}

	public static void main(String[] args){
	
	//CorpusRetriveNew ex=new CorpusRetriveNew();
		//String sentence1="ඔවුහු පොත් කියවයි";
		//String sentence1="ඔහු කලින් වෙන් කරණවෙක් කියවයි";
    //	ex.corpusRetriveDemo(sentence1);

	}
	public double corpusRetriveDemo(String sentence,int avTime,int number){
		ArrayList<String> wordAll= new ArrayList<String>();
		ArrayList<String> wordPhrase= new ArrayList<String>();
		ArrayList<String> resultSetStore= new ArrayList<String>();
		int avrGramTime=(avTime)/4;
		int index=number-1;	
		int completeTime;
		String processTime;
		/* Get all word list For corpus Retrive */

		wordAll=allWordsList(sentence);
		System.out.println("Get sentences from corpus...");
		ResultSet rsNew=corpusSentenceRetrive(wordAll);
		/* uni Gram calculate */
		System.out.println("Callng uni gram...");
		wordPhrase=wordsSplitter(sentence);
		double d1=calculateProbUniGram(wordAll,rsNew);
		completeTime=(index*avTime)+avrGramTime;
		processTime=completeTime+"% completed";
	//	System.err.println(processTime);
		/* Bi Gram calculate */
		System.out.println("Callng bi gram...");
		double d2=calculateProbBiGram(wordPhrase,rsNew);
		completeTime=(index*avTime)+(2*avrGramTime);
		processTime=completeTime+"% completed";
	//	System.err.println(processTime);
		d2=d2+d1;
		/* Tri Gram calculate */
		System.out.println("Callng tri gram...");
		double d3=calculateProbThreeGram(wordPhrase,rsNew);
		completeTime=(index*avTime)+(3*avrGramTime);
		processTime=completeTime+"% completed";
	//	System.err.println(processTime);
		d3=d3+d2;
		/* Four Gram calculate */
		System.out.println("Callng four gram...");
		double d4=calculateProbFourGram(wordPhrase,rsNew);
		completeTime=(index*avTime)+(4*avrGramTime);
		processTime=completeTime+"% completed";
	//	System.err.println(processTime);
		d4=d4+d3;
		return d4;
	}
	/* Words split based on space */
	public ArrayList<String> wordsSplitter(String sentence) {
		ArrayList<String> wordArrayList = new ArrayList<String>();
		wordArrayList.clear();
		for (String word : sentence.split("\\s+")) {
			wordArrayList.add(word);
		}
		return wordArrayList;
	}
	
	/* Remove space and slash */
	public ArrayList<String> allWordsList(String sentence) {
		ArrayList<String> allWordList = new ArrayList<String>();
		for (String word : sentence.split(" ")) {
			if (word.contains("/")) {
				for (String wordNew : word.split("/")) {
					allWordList.add(wordNew);
				}
			} else {
				allWordList.add(word);
			}
		}
		return allWordList;
	}
	/* Database Retrieve */
	public ResultSet corpusSentenceRetrive(List wordList) {
		ResultSet rs2 = null;
		StringBuilder sb = new StringBuilder();
		try {
			sb.append("SELECT Cor_Sentence FROM corpus Where MATCH (Cor_Sentence) AGAINST");
			sb.append("('+"+wordList+"' IN BOOLEAN MODE)");
					 System.out.println("Prepared to get...");
					 rs2 = dbc.sqlExecute(sb.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return rs2;
		
	}
	/* Calculate Uni Gram Probability */
	public double calculateProbUniGram(List s1,ResultSet rst){
		ResultSet rs = rst;
		int maxFrequency = 0;
		double t = 0;
		String[] wordSplit = new String[2];

		ArrayList<String> wordArrayList = new ArrayList<String>();
		wordArrayList=(ArrayList<String>) s1;
		ArrayList<String> CorpusWordArrayList = new ArrayList<String>();
	
		try {

	    	String corpusSentence = null;
			/* Split corpus sentences and calculate probability */
			while (rs.next()) {
				int countFrequency = 0;
				CorpusWordArrayList.clear();

				corpusSentence = rs.getString("Cor_Sentence");

				for (String CorpusWord : corpusSentence.split(" ")) {
					CorpusWordArrayList.add(CorpusWord);
				}

				for (String a : wordArrayList) {

					for (String b : CorpusWordArrayList) {

						if (a.equals(b)) {

							countFrequency++;

							if (countFrequency > maxFrequency) {

								maxFrequency = countFrequency;

							}

							break;
						}
					}

				}

			}
			rs.beforeFirst();
			double r = wordArrayList.size();
			t = maxFrequency / r;
			System.out.println("Sentence probability Uni Gram: " + t * 100);
			wordArrayList.clear();

		} catch (Exception e) {
			e.printStackTrace();
		} 
		return (t * 100) * 1;

	}
	
	/* Calculate Bi Gram Probability */
	public double calculateProbBiGram(List<String> s2,ResultSet rst) {
		ResultSet rs2 = rst;
		String first;
		String second;
		String word;
		String word2;
		double t2 = 0;
		String[] a1;
		String[] b1;
		String corpusSentence2 = null;
		String []array = new String[s2.size()];
		String[] array2;
		int maxFrequency2 = 0;
		ArrayList<String> wordArrayList2 = new ArrayList<String>();
		ArrayList<String> CorpusWordArrayList2 = new ArrayList<String>();
		wordArrayList2.clear();
		array=(String[]) s2.toArray(array);

		if (array.length >= 2) {
			/* Add sentence into arraylist as two by two word phrases */
				
		if(array.length>1){
		
		for (int i = 0; i < array.length - 1; i++) {
			first = array[i];
			second = array[i + 1];
			a1 = first.split("/"); // split based on forward slash
			b1 = second.split("/"); // split based on forward slash
			for (String a : a1) {
				for (String b : b1) {
					word = a + "/" + b;
					wordArrayList2.add(word);

				}
			}
		}
		
		}
		
		else{
			wordArrayList2.add(array[0]);
			System.out.println(wordArrayList2);
		}
		/* Add corpus sentences into arraylist as two by two word phrases */
		try {
			while (rs2.next()) {
				CorpusWordArrayList2.clear();
				int countFrequency2 = 0;
				corpusSentence2 = rs2.getString("Cor_Sentence");
				array2 = corpusSentence2.split("\\s+");
				for (int k = 0; k < array2.length - 1; k++) {
					word2 = array2[k] + "/" + array2[k + 1];

					CorpusWordArrayList2.add(word2);

				}
				for (String a2 : wordArrayList2) {
					for (String b2 : CorpusWordArrayList2) {

						if (a2.equals(b2)) {
							countFrequency2++;

							if (countFrequency2 > maxFrequency2) {

								maxFrequency2 = countFrequency2;

							}

							break;
						}
					}

				}

			}
			rs2.beforeFirst();
			double r2 = wordArrayList2.size();
			t2 = maxFrequency2 / r2;
			
			System.out.println("Sentence probability Bi Gram: " + t2 * 100);
			wordArrayList2.clear();

		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return (t2 * 100) * 100;}
		else{
			System.out.println("Sentence length less than 2 for two Gram matching:");
			return 0.00;
		}

	}
	
	/* Calculate Three Gram probability... */
	public double calculateProbThreeGram(List<String> s3,ResultSet rst) {
		ResultSet rs3 = rst;
		String first3;
		String second3;
		String third3;
		double t3 = 0;
		String corpusSentence3 = null;
		String[] a3;
		String[] b3;
		String[] c3;
		String word3;
		String word4;
		int maxFrequency3 = 0;
		String[] array3=new String[s3.size()];
		ArrayList<String> wordArrayList3 = new ArrayList<String>();
		ArrayList<String> CorpusWordArrayList3 = new ArrayList<String>();
		
		array3=(String[]) s3.toArray(array3);
		if (array3.length >= 3) {
		/* Add sentence into arraylist as three by three word phrases */
		for (int i = 0; i < array3.length - 2; i++) {
			first3 = array3[i];
			second3 = array3[i + 1];
			third3 = array3[i + 2];

			a3 = first3.split("/"); // split based on forward slash
			b3 = second3.split("/"); // split based on forward slash
			c3 = third3.split("/");
			for (String a : a3) {
				for (String b : b3) {
					for (String c : c3) {
						word3 = a + "/" + b + "/" + c;
						wordArrayList3.add(word3);

					}
				}
			}
		}

		/* Add corpus sentences into arraylist as three by three word phrases */

		try {
			while (rs3.next()) {
				CorpusWordArrayList3.clear();
				int countFrequency3 = 0;
				corpusSentence3 = rs3.getString("Cor_Sentence");
				array3 = corpusSentence3.split("\\s+");
				for (int k = 0; k < array3.length - 2; k++) {
					word4 = array3[k] + "/" + array3[k + 1] + "/"
							+ array3[k + 2];

					CorpusWordArrayList3.add(word4);
				}

				for (String a4 : wordArrayList3) {
//System.out.println(a4);
					for (String b4 : CorpusWordArrayList3) {

						if (a4.equals(b4)) {
							if(wordArrayList3.size()==1){
								t3=1.0;
								System.out.println("Sentence probability Three Gram: " + (t3 * 100));
								return (t3 * 100) * 10000;
								
							}
							else{			
							countFrequency3++;
							if (countFrequency3 > maxFrequency3) {
								maxFrequency3 = countFrequency3;

							}

							break;}
						}
					}

				}

			}
			rs3.beforeFirst();
			double r3 = wordArrayList3.size();
			t3 = maxFrequency3 / r3;
			System.out.println("Sentence probability Three Gram: " + t3 * 100);
			wordArrayList3.clear();

		} catch (Exception e) {
		
			e.printStackTrace();
		}
		return (t3 * 100) * 10000;}
		else{
			System.out.println("Sentence length less than 3 for three Gram matching:");
			return 0.00;
		}

	}
	
	/* Calculate Four Gram probability... */
	public double calculateProbFourGram(List<String> s4,ResultSet rst) {
		double t4 = 0;
		ResultSet rs4 =rst;
		String first4;
		String second4;
		String third4;
		String fourth4;
		String corpusSentence4 = null;
		String[] a4;
		String[] b4;
		String[] c4;
		String[] d4;
		String word5;
		String word6;
		int maxFrequency4 = 0;

		String[] array4=new String[s4.size()];
		ArrayList<String> wordArrayList4 = new ArrayList<String>();
		ArrayList<String> CorpusWordArrayList4 = new ArrayList<String>();

		array4=(String[]) s4.toArray(array4);
		if (array4.length>= 4) {
		/* Add sentence into arraylist as Four by Four word phrases */
		
		for (int l = 0; l < array4.length - 3; l++) {
			first4 = array4[l];
			second4 = array4[l + 1];
			third4 = array4[l + 2];
			fourth4 = array4[l + 3];

			a4 = first4.split("/"); // split based on forward slash
			b4 = second4.split("/"); // split based on forward slash
			c4 = third4.split("/");
			d4 = fourth4.split("/");

			for (String a : a4) {
				for (String b : b4) {
					for (String c : c4) {
						for (String d : d4) {
							word5 = a + "/" + b + "/" + c + "/" + d;
							wordArrayList4.add(word5);

						}
					}
				}
			}
		}

			/*
			 * Add corpus sentences into arraylist as four by four word
			 * phrases
			 */

			try {
				while (rs4.next()) {
					CorpusWordArrayList4.clear();
					int countFrequency4 = 0;
					corpusSentence4 = rs4.getString("Cor_Sentence");
					array4 = corpusSentence4.split("\\s+");
					for (int n = 0; n < array4.length - 3; n++) {
						word6 = array4[n] + "/" + array4[n + 1] + "/"
								+ array4[n + 2] + "/" + array4[n + 3];

						CorpusWordArrayList4.add(word6);
					}

					for (String a5 : wordArrayList4) {

						for (String b5 : CorpusWordArrayList4) {

							if (a5.equals(b5)) {
								if(wordArrayList4.size()==1){
									t4=1.0;
									System.out.println("Sentence probability Four Gram: " + (t4 * 100) * 1000000);
									return (t4 * 100) * 1000000;
									
								}
								else{																
								countFrequency4++;

								if (countFrequency4 > maxFrequency4) {

									maxFrequency4 = countFrequency4;

								}

								break;}
							}
						}

					}

				}

				double r4 = wordArrayList4.size();
				t4 = maxFrequency4 / r4;
				System.out.println("Sentence probability Four Gram: " + t4* 100);
				wordArrayList4.clear();

			} catch (Exception e) {
				
				e.printStackTrace();
			}
			return (t4 * 100) * 1000000;
		} else {
			System.out.println("Sentence length less than 4 for Four Gram matching:");
			return 0.00;
		}
	}

	
	/* Insert corrected sentences */
	public String corpusSentenceInsert(String correct) {
		String out = null;
		try {
			
            String insrtSen = "INSERT INTO corpus (Cor_Sentence) VALUES ('"+correct+"')";
            
            out = dbc.sqlExecuteInsert(insrtSen);        
                      
		} catch (Exception e) {
			e.printStackTrace();
		}

		return out;
		
	}
	
	
}
