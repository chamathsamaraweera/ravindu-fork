package uom.bravehearts.statisticalmt;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class InsertTextFileToMySQL {

	/* Database Connection */

	public static Connection getConnection() throws Exception {
		String driver = "org.gjt.mm.mysql.Driver";
		String url = "jdbc:mysql://localhost/BraveHearts?characterEncoding=UTF-8";
		String username = "root";
		String password = "";

		Class.forName(driver);
		Connection conn = DriverManager.getConnection(url, username, password);
		return conn;
	}

	public static void main(String args[]) {
		StringBuilder sb = new StringBuilder();

		try {
			BufferedReader bf = new BufferedReader(
					new FileReader(
							"G:\\Level4 sem1\\Project\\project\\Corpus\\SINHALACORPUS\\NEWSPAPERS\\NEWS REPORTAGE\\FOREIGN\\NPNRFO0088.TXT"));
			String line = bf.readLine();
			while (line != null) {
	
				line = bf.readLine();
				sb.append(line);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String paragraph = sb.toString();

		//System.out.println("worddddd" + paragraph);

		String[] sentenceHolder = paragraph.split("\\.");

		// String[]sentenceHolder =
		// paragraph.split("(?i)(?<=[.?!])\\S+(?=[a-z])");

		for (int i = 0; i < sentenceHolder.length-1; i++) {
			try {
				addSentence(sentenceHolder[i]);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(sentenceHolder[i]);
		}

	}

	public static void addSentence( String sentence) throws SQLException {
	    PreparedStatement pstmt = null;
	    Connection conn = null;
	      try {
			conn = getConnection();
		      pstmt = conn.prepareStatement("insert into corpus(Cor_Sentence) values (?)");
		     // pstmt.setString(1, id);
		      pstmt.setString(1, sentence);
		      pstmt.executeUpdate();
		      //conn.commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      finally {
	          pstmt.close();
	          conn.close();
	        }
	    

	}

}
