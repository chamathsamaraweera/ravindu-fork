package uom.bravehearts.statisticalmt;

import java.io.File;
import java.io.IOException;

public class FusekiStart {

	public static void main(String[] args) {
		String[] command = { "\\..\\jena-fuseki-1.0.1\\fuseki-server.bat", "--update", "--mem", "/ds" };
	    File directory = new File("\\..\\jena-fuseki-1.0.1");
	    ProcessBuilder pb = new ProcessBuilder(command);
	    pb.directory(directory);
	    
	     try {
			pb.start();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
