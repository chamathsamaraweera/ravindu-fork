package uom.bravehearts.statisticalmt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.hp.hpl.jena.query.*;

public class OntologyReference {
	
	public static void main(String args[]) {

	}

	public ArrayList<String> returnOntologyWords(String word) {

		/* Select super class */
		String search = word;
		List<String> listOfString = new ArrayList<String>();
		List<String> listOfAll = new ArrayList<String>();
		ArrayList<String> outListVal = new ArrayList<String>();
		listOfString.clear();
		listOfAll.clear();
		outListVal.clear();

		String ontoQuery = "prefix pre:<http://www.semanticweb.org/dasitha/ontologies/2014/5/untitled-ontology-25#>"
				+ "prefix rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
				+ "select ?o where" + "{pre:" + search + " ?p ?o.}";

		Query query = QueryFactory.create(ontoQuery);
		QueryExecution qExe = QueryExecutionFactory.sparqlService(
				"http://localhost:3030/ds/query", query);
		ResultSet results = qExe.execSelect();

		while (results.hasNext()) {
			QuerySolution binding = results.nextSolution();

			listOfString.add(binding.toString());

		}
		if (listOfString.isEmpty()) {
			outListVal.add(search);
			return outListVal;
		} else {
			String[] queryOut = listOfString.get(1).split("#");
			String[] queryOutNext = queryOut[1].split(">");
			System.out.println("Given value:" + search);
			System.out.println("Identified as :" + queryOutNext[0]);

			/* Select all related words */
			String selectAll = "prefix pre:<http://www.semanticweb.org/dasitha/ontologies/2014/5/untitled-ontology-25#>"
					+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
					+ "SELECT ?rr WHERE"
					+ "{?rr rdf:type pre:"
					+ queryOutNext[0] + "}";

			Query query2 = QueryFactory.create(selectAll); // s2 = the query
															// above
			QueryExecution qExe2 = QueryExecutionFactory.sparqlService(
					"http://localhost:3030/ds/query", query2);
			ResultSet results2 = qExe2.execSelect();
			while (results2.hasNext()) {
				QuerySolution bindingAll = results2.nextSolution();

				listOfAll.add(bindingAll.toString());

			}

			String outComes[] = new String[2];
			String assignValues[] = new String[2];
			System.out.println("Out comes from Ontology:");
			for (int i = 0; i < listOfAll.size(); i++) {
				outComes = listOfAll.get(i).split("#");
				assignValues = outComes[1].split(">");
				outListVal.add(assignValues[0]);
				System.out.println(assignValues[0]);
			}
			return outListVal;
		}
	}

}
