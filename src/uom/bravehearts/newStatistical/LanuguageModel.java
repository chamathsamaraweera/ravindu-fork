package uom.bravehearts.newStatistical;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class LanuguageModel {
	//Sentences from the database
	LinkedHashSet<String> sentencesSet;
	public LanuguageModel(){}
	
	public LanuguageModel(LinkedHashSet<String> sentencesSet){
		this.sentencesSet = sentencesSet;
	}
	
	/*public float probabilityCalculationInBiGram(ArrayList<String> CandidateSentence){
		float probability = 0;
		LinkedHashSet<String> wordsTwoByTwo = new LinkedHashSet<String>();
		for (int i = 0; i < CandidateSentence.size(); i++) {
			ArrayList<String> wordsSetOfTheCandidateSentence = new ArrayList<String>();
			splitSentence(wordsSetOfTheCandidateSentence, CandidateSentence.get(i));
			for (int j = 0; j < wordsSetOfTheCandidateSentence.size()-1; j++) {
				String temp = wordsSetOfTheCandidateSentence.get(j)+" "+wordsSetOfTheCandidateSentence.get(j+1);
				wordsTwoByTwo.add(temp);
			}
		}
		return probability;
	}*/
	
	/**
	 * This method is used to split a sentence
	 * to words.
	 */
	private void splitSentence(ArrayList<String> splittedWordList, String theSentence){
		for(String word : theSentence.split(" ")) {
			splittedWordList.add(word);
		}
	}
	
	public double probablityCalculationInUniGram(ArrayList<String> databaseSentences, String thePhrase){
		double countOfThePhrase = 0;
		int countOfTheTotoalWords = 0;	
		double probablity = 0;
		//Counting the total words...
		for (int i = 0; i < databaseSentences.size(); i++) {
			ArrayList<String> wordSet = new ArrayList<String>();
			splitSentence(wordSet, databaseSentences.get(i));
			for (int j = 0; j < wordSet.size(); j++) {
				countOfTheTotoalWords = countOfTheTotoalWords+1;
			}
		}
		String thePhraseEdited = thePhrase.replaceAll("\\s+", " ");
		Count counter = new Count();
		for (String word : thePhraseEdited.split(" ")) {
			double count = counter.getCount(databaseSentences, word);
			probablity = probablity + count / countOfTheTotoalWords;
			countOfThePhrase++;
		}
		return probablity/countOfThePhrase;
	}

	/*public double probabilityCalculationInBiGram(ArrayList<String> databaseSentences, String thePhrase){
		double probability = 0;
		int worthToCheck = 0;
		String thePhraseOne = thePhrase.trim();
		ArrayList<String> wordsOfThePhrase = new ArrayList<String>();
		String thePhraseEdited = thePhraseOne.replaceAll("\\s+", " ");
		for (String word : thePhraseOne.split(" ")) {
			wordsOfThePhrase.add(word);
			worthToCheck++;
		}
		String[] pairs = new String[wordsOfThePhrase.size()-1];
		for (int i = 0; i < pairs.length; i++) {
			pairs[i] = wordsOfThePhrase.get(i)+" "+wordsOfThePhrase.get(i+1);
			//System.out.println(pairs[i]);
		}
		Count count = new Count();
		if(worthToCheck>=2){
			double temp = 0;
			for (int i = 0; i < pairs.length; i++) {
				String tempString = pairs[i];
				temp = count.getCount(databaseSentences, tempString);
				double tempOne = count.getCount(databaseSentences, wordsOfThePhrase.get(i));
				probability = probability+(temp/tempOne);
			}
			probability = probability/pairs.length;
		}
		return probability++;
	}*/
	
	public double probabilityCalculationInBiGram(ArrayList<String> databaseSentences, String thePhrase){
		double probablity = 0;
		double countOfThePhrase = 0.1;
		//Checking whether there are at least two words in the phrase <<thePhrase>>
		int wordCount=0;
		char ch[]= new char[thePhrase.length()];
	    for(int i=0;i<thePhrase.length();i++){
	    	ch[i]= thePhrase.charAt(i);
	        if( ((i>0)&&(ch[i]!=' ')&&(ch[i-1]==' ')) || ((ch[0]!=' ')&&(i==0)) )
	        wordCount++;
	    }
	    /////////////////////////////////////////////////////////////////////////
	    //Creating word array
	    String newName = thePhrase.replaceAll("\\s+", " ");
		//System.out.println(newName);
		String[] strs = newName.split(" ");
		ArrayList<String> wordList = new ArrayList<String>();
		for (int i = 0; i < strs.length; i++) {
			if(!(strs[i]).equals("")){
				wordList.add(strs[i]);
			}else{
				
			}
		}
		//System.out.println(wordList.size());
		String[] words = new String[wordList.size()];
		for (int i = 0; i < wordList.size(); i++) {
			words[i] = wordList.get(i);
		}
		/////////////////////////////////////////////////////////////////////////
	    //Probability calculation
		if(wordCount>1){
			
			double wordOneCount = 0.1;
			double wordTwoByTwoCount = 0;
			ArrayList<String> DatabaseSentencesEdited = new ArrayList<String>();
			for (int i = 0; i < databaseSentences.size(); i++) {
				String s = databaseSentences.get(i);
				String newNameOne = s.replaceAll("\\s+", " ");
				//System.out.println(s);
				//System.out.println(newNameOne);
				String currentSentence = newNameOne.replaceAll("^\\s+", "").replaceAll("\\s+$", "");
				//System.out.println((i+1)+" "+currentSentence);
				DatabaseSentencesEdited.add(currentSentence);
			}
			CountWord counter = new CountWord();
			for (int i = 0; i < words.length-1; i++) {
				wordOneCount = counter.getCount(DatabaseSentencesEdited, words[i]);
				String twoWords = words[i]+" "+words[i+1];
				wordTwoByTwoCount = counter.getCount(DatabaseSentencesEdited, twoWords);
				probablity = probablity+(wordTwoByTwoCount/wordOneCount);
				countOfThePhrase++;
			}
		}
		/////////////////////////////////////////////////////////////////////////
	    return probablity/countOfThePhrase;
	}

	public double probabilityCalculationInTriGram(ArrayList<String> databaseSentences, String sentence){
		for (int i = 0; i < databaseSentences.size(); i++) {
			System.out.println(databaseSentences.get(i));
		}
		double probability = 0;
		PhraseModel model = new PhraseModel();
		ArrayList<String> listOne = model.getPhrases(sentence);
		String sOne = "";
		for (int i = 0; i < listOne.size()-1; i++) {
			sOne = sOne+listOne.get(i)+" ";
		}
		sOne = sOne+listOne.get(listOne.size()-1);
		System.out.println(sOne);
		String[] parts = sOne.split(" ");
		String[] words = new String[parts.length];
		for (int i = 0; i < words.length; i++) {
			words[i]=parts[i];
		}
		double[] tempTwo = new double[words.length-1];
		for (int i = 0; i < tempTwo.length; i++) {
			tempTwo[i]=0;
		}
		for (int i = 0; i < databaseSentences.size(); i++) {
			String currentSentence = databaseSentences.get(i);
			String newName = currentSentence.replaceAll("\\s+", " ");
			//System.out.println(newName);
			String[] strs = newName.split(" ");
			ArrayList<String> wordList = new ArrayList<String>();
			for (int j = 0; j < strs.length; j++) {
				if(!(strs[j]).equals("")){
					wordList.add(strs[j]);
				}else{
					
				}
			}
			//System.out.println(wordList.size());
			String[] wordsOfDbSentence = new String[wordList.size()];
			for (int k = 0; k < wordList.size(); k++) {
				wordsOfDbSentence[k] = wordList.get(k);
			}
			for (int j = 0; j < wordsOfDbSentence.length-1; j++) {
				for (int j2 = 0; j2 < words.length-1; j2++) {
					String s1 = words[j2]+" "+words[j2+1];
					String s2 = wordsOfDbSentence[j]+" "+wordsOfDbSentence[j+1];
					if(s1.equals(s2)){
						tempTwo[j2]++;
					}
				}
			}
		}
//		for (int i = 0; i < tempTwo.length; i++) {
//			System.out.println(tempTwo[i]);
//		}
		
		
		////////////////////////////////////////////////////////////////////////////////////
		double[] tempThree = new double[words.length-2];
		for (int i = 0; i < tempThree.length; i++) {
			tempThree[i]=0;
		}
		for (int i = 0; i < databaseSentences.size(); i++) {
			String currentSentence = databaseSentences.get(i);
			String newName = currentSentence.replaceAll("\\s+", " ");
			//System.out.println(newName);
			String[] strs = newName.split(" ");
			ArrayList<String> wordList = new ArrayList<String>();
			for (int j = 0; j < strs.length; j++) {
				if(!(strs[j]).equals("")){
					wordList.add(strs[j]);
				}else{
					
				}
			}
			//System.out.println(wordList.size());
			String[] wordsOfDbSentence = new String[wordList.size()];
			for (int k = 0; k < wordList.size(); k++) {
				wordsOfDbSentence[k] = wordList.get(k);
			}
			for (int j = 0; j < wordsOfDbSentence.length-2; j++) {
				for (int j2 = 0; j2 < words.length-2; j2++) {
					String s1 = words[j2]+" "+words[j2+1]+" "+words[j2+2];
					String s2 = wordsOfDbSentence[j]+" "+wordsOfDbSentence[j+1]+" "+wordsOfDbSentence[j+2];
					if(s1.equals(s2)){
						tempThree[j2]++;
					}
				}
			}
		}
		for (int i = 0; i < tempThree.length; i++) {
			//System.out.println(tempThree[i]);
		}
		for (int i = 0; i < tempThree.length; i++) {
			probability = probability + (tempThree[i]/tempTwo[i]);
		}
		probability = probability/tempThree.length;
		System.out.println(probability);
		//////////////////////////////////////////////////////////////////////////////////
		return probability;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
class CountWord{
	String word;
	int count;
	LinkedHashSet<String> databaseSentences;
	
	/**
	 * Default constructor
	 */
	CountWord(){}
	
	/**
	 * The constructor used to initialize the words with the database sentences
	 */
	CountWord(String word, LinkedHashSet<String> databaseSentences){
		this.word = word;
		this.databaseSentences = databaseSentences;
	}
	
	/**
	 * The method use to count the words for calculating probability
	 */
	void setCount(){
		Iterator it = databaseSentences.iterator();
		while(it.hasNext()){				 
				String currentSentence = it.next().toString();
				String findStr = word;
				int lastIndex = 0;

				while(lastIndex != -1){

				    lastIndex = currentSentence.indexOf(findStr,lastIndex);

				    if(lastIndex != -1){
				        count ++;
				        lastIndex += findStr.length();
				    }
				}
		}
	}
	
	/**
	 * Used to get the count
	 */
	int getCount(){
		return count;
	}
	
	int getCount(ArrayList<String> databaseSentences,String wordPhrase){
		int wordCount = 0;
		for (int i = 0; i < databaseSentences.size(); i++) {
			String currentSentence = databaseSentences.get(i).toString();
			String findStr = wordPhrase;
			int lastIndex = 0;
			
			while(lastIndex != -1){

			    lastIndex = currentSentence.indexOf(findStr,lastIndex);

			    if(lastIndex != -1){
			        wordCount ++;
			        lastIndex += findStr.length();
			    }
			}
		}
		return wordCount;
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

