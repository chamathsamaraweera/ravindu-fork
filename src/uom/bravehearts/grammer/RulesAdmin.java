package uom.bravehearts.grammer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import edu.stanford.nlp.util.ArrayUtils;
import uom.bravehearts.newStatistical.Demo;
import uom.bravehearts.analyze.AnalyzerModule;
import uom.bravehearts.analyze.StanfordLemmatizer;
import uom.bravehearts.connection.DatabaseConnection;
import uom.bravehearts.morphology.MorfAdmin;
import uom.bravehearts.statisticalmt.StatisticalMTReferenceNew;
import uom.bravehearts.transliterate.Transliterator;
import uom.bravehearts.passiveVoice.ReduceCandidates;
import uom.bravehearts.passiveVoice.Phrases;
//import uom.bravehearts.analyze.AnalyzerModule;

public class RulesAdmin {
	
	CandidateSentences cs=new CandidateSentences ();
	List<List>candidateS;

	public String startPerform(List<List> input) {
		
	//	System.out.println("Heap soze "+ Runtime.getRuntime().totalMemory());
		
		List<List> manipulatedPhrases = new ArrayList<List>();
		List<List> manipulatedsvo = new ArrayList<List>();
		Transliterator trans = new Transliterator();
		List<String> nlpKeySrore = new ArrayList<>();// LIST THAT STORE NLP KEY TAGS
		List <String>flist = new ArrayList<String>();

		StanfordLemmatizer lemmatization = new StanfordLemmatizer();
		StatisticalMTReferenceNew sf  = new StatisticalMTReferenceNew();//////////////////to move statistical MT module ////////////////////
		VerbObjectRule vor = new VerbObjectRule();
		SbarRule sbar= new SbarRule();
		
		System.out.println("RulesAdmin.java input: "+input);
		

		manipulatedPhrases = input;//	INPUT = FINALLIST RETURN FORM THE "ANALYZERMODULE"
		manipulatedPhrases=sbar.performRule(manipulatedPhrases);

		nlpKeySrore.add("is");
		nlpKeySrore.add("යනු");
		nlpKeySrore.add("VBGi");// ADD THESE 3 ITEMS INTO "nlpKeySrore" LIST

		DatabaseConnection dbc = new DatabaseConnection();  // creating a instance of DatabSaseConnection class
		dbc.conOpen("jdbc:mysql://localhost/movedb", "root", ""); // params- db url ,uname,pw
															

		for (List<Object> manphrases : manipulatedPhrases) {// "manphrases" = "manipulatedPhrases"

			int listCount = 0;
			int manphrasesSize = manphrases.size();

			for (int x = 0; x < manphrasesSize; x++) {

				String keyVal =(String) manphrases.get(x);

				if ("NNS".equals(keyVal) || "VBZ".equals(keyVal)// NNS - NOUN PLURAL -- VBZ -  Verb, 3rd person singular present
						|| "VBG".equals(keyVal) || "VBD".equals(keyVal)// VBG - Verb, gerund or present participle -- VBD - VERB,PAST TENSE
						|| "VBN".equals(keyVal)) {// VBN - Verb, past participle

					String lWord =(String)manphrases.get(x + 1);
					
					if ("is".equalsIgnoreCase(lWord)//	IF "X" IS ONE OF ABOVE TAGS AND NEXT ONE(X+1) IS [IS/ARE/WAS/WERE...] SHIFT TO NEXT PACE
							|| "are".equalsIgnoreCase(lWord)
							|| "was".equalsIgnoreCase(lWord)
							|| "we".equalsIgnoreCase(lWord)
							|| "am".equalsIgnoreCase(lWord)
							|| "has".equalsIgnoreCase(lWord)) {

						// do nothing
					} else {

						String lemma = lemmatization.lemmatize(lWord);//	IF "X" IS ONE OF ABOVE TAGS AND NEXT ONE(X+1) IS NOT[IS,WAS,WERE...] LEMMATIZE THE WORD
						manphrases.set(x + 1, lemma);// SET LEMMA LIST ITEM TO THE (X+1) POSITION IN THE manphrases LIST
					}

				}

				String query = "SELECT value FROM nlpkeys where nlpkey='"
						+ keyVal + "' ";
				String query2 = "SELECT SINHALA FROM word where ENGLISH='" + keyVal + "' ";

				ResultSet rs = dbc.sqlExecute(query);/////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// execute the query and get the results set.
				int nlpVal = 0;

				try {

					while (rs.next()) {
						nlpVal = rs.getInt("value");
						System.out.println("NLPval " + nlpVal);

					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				if (nlpVal == 1) {

					nlpKeySrore.add(keyVal);
					
					
					// do nothing
					
				}

				else if (nlpVal == 0) {
					
						String passiveTag = (String) manphrases.get(manphrasesSize-1);
					
					if("by".equalsIgnoreCase(keyVal) && ("PaPrSi".equalsIgnoreCase(passiveTag) ||"PaPrCo".equalsIgnoreCase(passiveTag)||"PaPrPe".equalsIgnoreCase(passiveTag)
							||"PaPaSi".equalsIgnoreCase(passiveTag)||"PaPaCo".equalsIgnoreCase(passiveTag)||"PaPaPe".equalsIgnoreCase(passiveTag)
							||"PaFuSi".equalsIgnoreCase(passiveTag)||"PaFuPe".equalsIgnoreCase(passiveTag))){
						
						boolean flag = false;
						
						String replaceValue = "විසින්";
						manphrases.set(listCount, replaceValue );//	REPLACE THE ENGLISH WORD BY SINHALA WORD
						System.out.println("Input :" + keyVal
								+ " returned :" + replaceValue );
						
						
						String [] passiveSubject = (String[]) manphrases.get(x-3);
						System.out.println("passiveSubject: "+passiveSubject);
												
												for(int i=0;i<passiveSubject.length;i++)
												{
													System.out.println("passiveSubject[i]: "+i);
													System.out.println("passiveSubject[i]: "+passiveSubject[i]);
													if("මට".equals(passiveSubject[i])){
														String word = "මා";
														manphrases.set(x-3,word);
												}
													if("අප".equals(passiveSubject[i])){
														String word = "අප";
														manphrases.set(x-3,word);
														break;
												}
												
													if("ඔවුන්ට".equals(passiveSubject[i])){
														String word = "ඔවුන්";
														manphrases.set(x-3,word);
														break;
												}
													if("ඔවුහු".equals(passiveSubject[i])){
														String word = "ඔවුන්";
														manphrases.set(x-3,word);
												}
													

													if("ඔහුට".equals(passiveSubject[i])){
														String word = "ඔහු";
														manphrases.set(x-3,word);
														break;
												}
													if("ඇගේ".equals(passiveSubject[i])){
														String word = "ඇය";
														manphrases.set(x-3,word);
														break;
												}
													System.out.println("manphrases: "+ manphrases);
												}
						
							
							
							System.out.println("manphrases: "+ manphrases);
						
						
						
						flag = true;
					}
					
					
					
					
					
				else{	 
					
					////////////////////////////////////////////////////////////////////////////////////////////////////////////
					/*if("PaPrSi".equalsIgnoreCase(keyVal) ||"PaPrCo".equalsIgnoreCase(keyVal)||"PaPrPe".equalsIgnoreCase(keyVal)
							||"PaPaSi".equalsIgnoreCase(keyVal)||"PaPaCo".equalsIgnoreCase(keyVal)||"PaPaPe".equalsIgnoreCase(keyVal)
							||"PaFuSi".equalsIgnoreCase(keyVal)||"PaFuPe".equalsIgnoreCase(keyVal)){
						
						
						
						
							
							String columnName =manphrases.get(listCount).toString();
							System.out.println("columnName -"+columnName);
							String sinhalaVerb = "take";
							System.out.println("sinhalaVerb -"+sinhalaVerb);
							String query3 = "SELECT "+ columnName + "  FROM passive where English ='" + sinhalaVerb + "' ";
							
							
							ResultSet rs3 = dbc.sqlExecute(query3);
							System.out.println(" rs3 :" + keyVal+ " returned :" + rs3.toString());
							
							try {
							while (rs3.next()){
							
								String resultPA = rs3.getString(keyVal);
								//String []arrPA =	resultPA.split(" \\| ");
								manphrases.set(listCount-1,resultPA);
								System.out.println("resultPA :" + keyVal+ " returned :" + resultPA);
								System.out.println(" with PASSIVE DB output"+manphrases);
								} 
							}
							catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
						
						
					
						
					}*/
					///////////////////////////////////////////////////////////////////////////////////////////////
					//else{
					ResultSet rs2 = dbc.sqlExecute(query2);

					try {

						boolean flag = false;

						while (rs2.next()) {

							//System.out.println("rs2:" + keyVal+ " returned :" + rs2);
							String result = rs2.getString("SINHALA"); // retrieve the value by db table column name.
							//System.out.println("result :" + keyVal+ " returned :" + result);											
																		
							String []arr =	result.split(" \\| ");	
							
							String lastTag = (String) manphrases.get(x-1);
							arr = ReduceCandidates.ReduceMeanings(arr,lastTag);											// column /////////////////////////////////////
							
							String newResult = null;											// name.
							for(int i=0;i<arr.length;i++)
							{
								newResult = arr[i]+"|";
							}
																		
							manphrases.set(listCount, arr);//	REPLACE THE ENGLISH WORD BY SINHALA WORD
							System.out.println("Input :" + keyVal+ " returned :" + result);
							
							flag = true;//	IF MEANING CONNOT FIND TRANSFER IT INTO THE TRANSLITERATOR
							//System.out.println(" Before PASSIVE DB output"+manphrases);
							
							

						}

						if (!flag) {
							String sinName = trans.translit(keyVal);
							manphrases.set(listCount, sinName);//	REPLACE THE ENGLISH WORD WITH TRANSLITERATED WORD

						}

					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				//}
			}
		}

				listCount++;
			}

		}

		
		
		
		
		
		
		
		
		//System.out.println("before cs : " + manipulatedPhrases);
		
		for(List<Object>ol:manipulatedPhrases){
		
			manipulatedPhrases= cs.createCandiate(ol);//	CREATIING CANDIDATE SENTENCES
		
		}
		
		

		// close the db connection

		System.out.println("test IsRule...");
		System.gc();
		IsRule is = new IsRule();
		System.out.println("before is : " + manipulatedPhrases);
		manipulatedPhrases = is.performRule(manipulatedPhrases);

		System.out.println("After is : " + manipulatedPhrases);

		MorfAdmin md = new MorfAdmin();
		manipulatedPhrases = md.morfadminPerform(manipulatedPhrases);//	CORRECT MORPHOLOGICAL ORDER
		
		// IDENTIFY PHRASES(VP/NP/PP)
		List<List>manipulatedPhrases02 = new ArrayList<>();
		manipulatedPhrases02 = Phrases.IdentifyPhrases((ArrayList<List>) manipulatedPhrases,(List<String>) nlpKeySrore);
		
		int noOfCandidates = manipulatedPhrases02.size();
		

//		for (List<String> manlist : manipulatedPhrases) {
//
//			for (String nlpKey : nlpKeySrore) {
//
//				manlist.remove(nlpKey);
//
//			}
//		}

		dbc.dbClose();

		System.out.println("After manipulation: ");
		StringBuffer finaloutput = new StringBuffer();

		for (List<String> disp : manipulatedPhrases) {
			
			disp.remove(0);//remove first tag
			StringBuffer ap= new StringBuffer();

			System.out.println(" ");
			for (String print : disp) {
				if(disp.indexOf(print)==0){
					System.out.print(print+" ");
					finaloutput.append(print+" ");
					ap.append(print+" ");
				}
				else{
					System.out.print(" " + print);
					finaloutput.append(" " + print);
					ap.append(print+" ");
				}
			}
			flist.add(ap.toString());
		}
		
		System.out.println("\n");
		System.out.println("CANDIDATE SENTENCES :"+noOfCandidates);
		Demo d = new Demo();
		//d.getTheSentence((ArrayList<String>) flist);
		String sentence= d.getTheSentence((ArrayList<String>) flist);//sf.sentenceFinalizer(flist);//	SENTENCE FINALIZER MODULE(AMBIGUITY HANDLING)
		
        String[] finalSentence = sentence.split(" ");
        String fout = " ";
        for (int i = 0; i < finalSentence.length; i++) {
            if(finalSentence[i].equals("NP")||finalSentence[i].equals("VP")){
                finalSentence[i]= " ";
            }
        }
        for (int i = 0; i < finalSentence.length; i++) {
        	if(finalSentence[i] == " ") continue;
            fout = fout+finalSentence[i]+"  ";
        }
		System.out.println("Output "+fout);
		return fout;
	}

}
