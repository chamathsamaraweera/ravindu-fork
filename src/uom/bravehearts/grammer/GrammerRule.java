package uom.bravehearts.grammer;

import java.util.List;
import java.util.Map;

public interface GrammerRule<T> {
	
	public Object performRule(List<T>phrases);

}
