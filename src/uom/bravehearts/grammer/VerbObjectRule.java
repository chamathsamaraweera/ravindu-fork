package uom.bravehearts.grammer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class VerbObjectRule {
	
	public List performRule(List<Map>phrases){
		
		List<List>manipulatedPhrases =new ArrayList<List>();//to 
		List<String>manipulatedSVO=new ArrayList<String>(); 
	
		
		for(Map<String,String> svo:phrases){
			
			String subject=svo.get("subject").trim();
			String verb=svo.get("verb").trim();
			String object=svo.get("object").trim();
			
			
			String phrase = subject+" "+"sbjBoundry"+" "+object+" "+verb;
			
			//System.out.println("phrase +: "+phrase );
		
			
		
				String[] words=	phrase.split(" ");
				
				for(String input:words){
					
					manipulatedSVO.add(input);
				}
		
			
				
			
			
			manipulatedPhrases.add(manipulatedSVO);
			manipulatedSVO=new ArrayList<String>();
			
		}
		return manipulatedPhrases;
		
	}
	
	

}
