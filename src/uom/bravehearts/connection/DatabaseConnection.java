package uom.bravehearts.connection;

//STEP 1. Import required packages
import java.sql.*;

public class DatabaseConnection {
	
	
	
	   // JDBC driver name and database URL
	 private  static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	 private  static  String DB_URL = "jdbc:mysql://localhost/movedb";

	   //  Database credentials
	   private  static  String USER = "root";
	   private static  String PASS = "";
	   private ResultSet rs;
	   
	   Connection conn = null;
	   Statement stmt = null;
	   
	   
	//   this method uses to open SQL queries
	   public boolean  conOpen(String dbUrl,String uName,String pw) {
		   
		   DB_URL=dbUrl;
		   USER=uName;
		   PASS=pw;
		  
	   try{
	      //STEP 2: Register JDBC driver
	     
			Class.forName("com.mysql.jdbc.Driver");
		

	      //STEP 3: Open a connection
	      System.out.println("Connecting to database...");
	      conn = DriverManager.getConnection(DB_URL,USER,PASS);

	      //STEP 4: Execute a query
	      System.out.println("Creating statement...");
	      stmt = conn.createStatement();
	      
	      return  true;
	   
	   
	   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		      return false;
	   }
	   catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	   
	   }
	   
	   
		   //this method used to execute SQL queries
	   public ResultSet sqlExecute(String query){
		   
		   try{
	 
	        rs = stmt.executeQuery(query);

		   } catch(SQLException e){
			   e.printStackTrace();	
			   return null;
		   }
	      //STEP 6: Clean-up environment
	      return rs;
	   }
	   
	 
	   //this method used insert corrected sentences into corpus
	   public String sqlExecuteInsert(String query){
		   String s;
		   try{
			   stmt.executeUpdate(query);
            s="Successfully inserted corrected sentence.Thank you.";
		   } catch(SQLException e){
			   e.printStackTrace();	
			   return null;
		   }
	      //STEP 6: Clean-up environment
	      return s;
	   }
	   
		   
		   
	 //this method used to close resources
	   public void dbClose(){
	   

	      
	      try{
	         if(stmt!=null)
	            stmt.close();
	      }catch(SQLException se2){
	      }// nothing we can do
	      try{
	         if(conn!=null)
	            conn.close();
	      }catch(SQLException se){
	         se.printStackTrace();
	      }
	      System.out.println("connection closed successfully!");
	   }
	  
	
	
	   

	}
	


