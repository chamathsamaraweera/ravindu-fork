package uom.bravehearts.analyze;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import uom.bravehearts.passiveVoice.PassiveIdentify;
import uom.bravehearts.passiveVoice.PassiveAnalyzer;

import com.sun.org.apache.bcel.internal.classfile.InnerClass;

import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.GrammaticalStructure;



public class AnalyzerModule {
	Tree nlpTree;	
	public List<String> analyzer(String text) {
		
		/*	MAKING 4 LISTS TO STORE SENTENCE TAGS WITH WORDS */
		List<String>nlpl= new ArrayList<String>();	//TO STORE NLP TREE TAGS
		List<List>outerList= new ArrayList<>();	// STORE ALL THE INNER LISTS
		List<String>innerList= new ArrayList<>();	// ALL SUBLISTS CREATED FROM MAIN LIST
		List<String>finalList= new ArrayList<>();	// THE FINAL OUTPUT
		
		
		boolean flagNlp=false;
		boolean flagTo=false;
		boolean isFlag=false;	
		
		
		
		
		System.out.println(text);	// DISPLAY THE SENTENCE
		
		try {
			// MAXIMUM WORDS FOR THE SENTENCE IS 80
			Tree nlpTree = (Tree) StanfordParser.stanfordParser(text); // this will return
			
		//	List<Tree> nlpl=nlpTree.getChildrenAsList();
			
			
			for (Tree subtree : nlpTree) {
				String value = subtree.label().value();	// the parsed nlp
															// tree of a
										nlpl.add(value);	// ADD EACH TAG TO THE nlpl ARRAY LIST		// sentnece
										// PARSE EACH TAGS INO THE NLPL TREE
										System.out.print(value+" ");	// PRINT VALUE AND A SPACE
				
			}
			
			//	IDENTIFYIING THE PASSIVE VOICE
			PassiveIdentify.checkPassive((ArrayList<String>)nlpl);
			 
			 
			//	IF THERE ARE TWO ADJACENT "VP" S ,REMOVE THE SECOND ONE
			//	REMOVE ADJACENT "NP" TAGS TO AVOID CONFLICTS
			for(int y=0;y<nlpl.size();y++)
			  {
				  if((nlpl.get(y).equals("VP"))&&(nlpl.get(y+1).equals("VP")))
		 			{
					  nlpl.remove(y+1);
		 				
		 			}
				  if((nlpl.get(y).equals("NP"))&&(nlpl.get(y+1).equals("NP")))
		 			{
					  nlpl.remove(y+1);
		 				
		 			}
				
			  }
			//	MORF ADMIN INPUT
			  System.out.println("MorfAdmin input :"+nlpl);

		} catch (IOException e) {

			e.printStackTrace();
		}
		
		int size = nlpl.size(); 
		System.out.println();
		
		// 	TRAVERSE THROUGH EACH NODE IN THE MORF ASMIN INPUT
		for(int x=0; x<size;x++){
			
			boolean lastval=false;
			String word=nlpl.get(x);
			
		//	System.out.println("word "+word+" x "+x);
			
			 
			System.out.println("Size :"+size+" x: "+x+" x+3: "+(x+3)+"word: "+word);
			
			// CHECK WEATHER IT IS A "VERB PHRASE"
			if(nlpl.get(x).trim().equalsIgnoreCase("VP")){
			
				System.out.println("condition :"+((x+3)<size));
			if ((x+3)<size) {
				System.out.println("outer size");

				if (((nlpl.get(x + 2).equals("is")// 2 TAGS AFTER "VP"
						|| nlpl.get(x + 2).equals("are")
						|| nlpl.get(x + 2).equals("was") 
						|| nlpl.get(x + 2).equals("were")
						||nlpl.get(x + 2).equals("will")
						||nlpl.get(x + 2).equals("am")
						||nlpl.get(x+2).equals("has")||nlpl.get(x+2).equals("have")))) {
					
					lastval=true;// L70
					System.out.println("check is are");					
					innerList.add("sbjBoundry");
					//do nothing
					System.out.println("if 1");
					System.out.println(innerList);
					if ((nlpl.get(x + 3).equals("VP"))) {// IF VP TAG COMES AFTER THE IS/AM/ARE/WAS...
						System.out.println("inner if 1");
						
						outerList.add(innerList);
						innerList=new ArrayList<>();
						nlpl.add(x + 3, "VPi");//	REPLACE VP WITH VPI
						nlpl.remove(x + 4);
						flagNlp=true;

						
					}
					else if(nlpl.get(x + 3).equals("RB")){//	"RB" - ADVERB
						if(x+5<size){
							System.out.println("inner else if");
							if(nlpl.get(x + 5).equals("VP")){
								System.out.println("inner else if vp");
								outerList.add(innerList);
								nlpl.add(x + 3, "VPi");
								nlpl.remove(x + 4);
								innerList=new ArrayList<>();
								flagNlp=true;
								
							}
							
						}
				
					}
				}
				else if(nlpl.get(x+1).equals("TO")){
					System.out.println("check To");
					outerList.add(innerList);
					innerList=new ArrayList<>();
					flagNlp=true;
					flagTo=true;
					
					
				}
				else if(!flagTo){
					System.out.println("flagto exc");
					innerList.add("sbjBoundry");
					outerList.add(innerList);
					innerList=new ArrayList<>();
					flagNlp=true;
									}
						
				
							}
			 if(x+2==(size-1)&& !lastval){
					System.out.println();
					System.out.println("last val");
					if(!flagNlp){
						
					//	innerList.add("sbjBoundry");
					}
					
					if(!flagTo){
					outerList.add(innerList);
					innerList=new ArrayList<>();
					flagNlp=true;
					}
									}
			
			
			
		}
			//	?PREPOSINIONAL PHRASE AND 
			else if(nlpl.get(x).trim().equalsIgnoreCase("PP")||nlpl.get(x).trim().equalsIgnoreCase("SBAR")){
				
				if(flagNlp){
					outerList.add(innerList);
					innerList=new ArrayList<>();
					flagNlp=false;
								}
			}
			//	"ADVERBIAL PHRASE" :- "genetically modified food"
			else if(nlpl.get(x).trim().equalsIgnoreCase("ADVP")){
				
				if(flagNlp){
					outerList.add(innerList);
					innerList=new ArrayList<>();
					flagNlp=false;
								}
			}
			
		
			//	"NOUN PHRASE"
			else if(nlpl.get(x).trim().equalsIgnoreCase("NP")){
				
				if(flagNlp){
					outerList.add(innerList);
					innerList=new ArrayList<>();
					flagNlp=false;
								}
			}
			
			//	"COORDINATING CONJUNCTION":- and,versus,'n
			else if(nlpl.get(x).trim().equalsIgnoreCase("CC")){
				
				if(size>x+3){
				if(nlpl.get(x+2).trim().equals("VP")||(nlpl.get(x+2).trim().equals("S")&& nlpl.get(x+3).trim().equals("NP"))){
					outerList.add(innerList);
					innerList=new ArrayList<>();
					flagNlp=false;
				}
				
				else if(nlpl.get(x+2).trim().equals("NP")){
					outerList.add(innerList);
					innerList=new ArrayList<>();
					flagNlp=false;
				}
				else if(x+4<size&&(nlpl.get(x+4).trim().equals("SBAR") &&(size>x+4))){
					
					outerList.add(innerList);
					innerList=new ArrayList<>();
					flagNlp=false;
					
				}
				
				
				
								}
			}
			
			//	IF "SBAR - CLAUSE INTRODUCED BY A SUBBORDINATING CONJUNCTION","SUBJECT" AND "NOUN PHRASE" IN SEQUENTIAL ORDER
			 if(nlpl.get(x).trim().equals("SBAR")&& nlpl.get(x+1).trim().equals("S")&& nlpl.get(x+2).trim().equals("NP")&&(size>x+2)){
				outerList.add(innerList);
				innerList=new ArrayList<>();
				flagNlp=false;
				
			}
			 if(nlpl.get(x).trim().equals("SBAR")&& nlpl.get(x+3).trim().equalsIgnoreCase("when")){
					outerList.add(innerList);
					innerList=new ArrayList<>();
					flagNlp=false;
					
				}
			
			
			innerList.add(word);//	ADD WORD TO THE INNERLIST
			
			if(x== size-1){
				outerList.add(innerList);
			}
		}
		System.out.println("before for");
		
		for(int a=0;a<outerList.size();a++){
			
			List<String>tl=outerList.get(a);
			if(tl.size()==0){
				
				outerList.remove(a);
				a--;
			}
			
		}
		for(List<String>li:outerList){
			System.out.print("List break: ");
			for(String s:li){
				System.out.print(s+" ");
				
			}
			System.out.println();
		}
		
		int fSize=outerList.size();// THE SIZE OF THE OUTER LIST
		int vpCount=0;
		
		//if (!PassiveIdentify.boolPassive) 
		//{
			for (int y = 0; y < fSize; y++) 
			{//	LOOP THROUGH THE OUTER LIST
				List<String> il = outerList.get(y);// ASSIGN FIRST OUTER LIST INTO "il" LIST

				System.out.println("il 0: " + il.get(0));// GET THE FIRST ELEMENT OF EACH LIST

				if (il.get(0).equals("VP")) {//	GET THE LISTS STARTS FROM THE "VP"
					System.out.println("final if");

					for (int z = y; z < fSize; z++) {//	LOOP THROUGH THE LISTS AND GET THE LISTS STARTS FROM THE "VP"

						List<String> ilc = outerList.get(z);// GET THE RELEVENT LIST FROM THE OUTER LIST

						vpCount = z;//	? GET THE VP COUNT

						if (!(ilc.size() > 0 && ilc.get(0).equals("VP"))) {
							//vpCount=z+1;
							break;
						}

					}

					for (int a = vpCount; a >= y; a--) {// REMOVE 2ND AND 3RD LIST IN ORDER

						finalList.addAll(outerList.get(a));//	ADD ALL LISTS TO OUTERLIST THAT DON'T BEGIN IN "VP"
						System.out.println("rem" + outerList.get(a));
						outerList.remove(a);
						fSize--;

					}
					y--;

				} else {
					//finalList.addAll(outerList.get(y));
					finalList.addAll(il);
					//System.out.println("Final check y : "+il.get(0));
				}

			} 
		//}
		
		
		
//		else {
//				for (int liCount = 0; liCount <= fSize; liCount++)
//				{
//					if (!(fSize == 0)) {
//						finalList.addAll(outerList.get(liCount));
//						outerList.remove(liCount);
//						liCount--;
//						fSize--;
//					}
//				} 
//			}
		
		System.out.println("Final list before man:");
		
		for(String s:finalList){
			System.out.print(s+" ");
			}
		
		for(int b=0;b<finalList.size();b++){// TRAVERE THROUGH THE FIANAL LIST TO FIND THE VPI TAG
			
			if(finalList.get(b).equals("VPi")){
				
				finalList.add(b,"VP");// ADD VP
				finalList.remove(b+1);// REMOVE VPI
			}
			
		}
		System.out.println();
		System.out.println("Final list  after:");
		
	
		
		
		
		
	if(PassiveIdentify.boolPassive){
		finalList = PassiveAnalyzer.ByForm(finalList);
	}
	
	for(String s:finalList){
		System.out.print(s+" ");
		}
	
		return finalList;
	}

}
