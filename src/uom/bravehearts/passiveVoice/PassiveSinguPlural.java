package uom.bravehearts.passiveVoice;
import java.util.ArrayList;
import uom.bravehearts.grammer.IsAnimate;
import java.util.List;


import uom.bravehearts.morphology.*;

public class PassiveSinguPlural {	

	public static List<String> HandlePlural(ArrayList<String> phrase) {
		

		System.err.println("PassiveSinguPlural executed....");
		System.out.println("PassiveSinguPlural input :"+phrase);
		
		IsAnimate checkAnimate = new IsAnimate();
		char res = 'i';
		boolean animateExecuted = false;
		
		for (int boundry = 0; boundry <= phrase.size() - 1; boundry++) 
		{
			if (phrase.get(boundry).equals("sbjBoundry"))

				for (int x = boundry; x >= 0; x--) {
					if (phrase.get(x).equals("NNS")) {
						int countNP = 0;

						for (int y = x; y <= phrase.size() - 1; y++) {
							if ((phrase.get(y).equals("NP") || (phrase.get(
									y).equals("VP") || (phrase.get(y)
									.equals("ADVP") || (phrase.get(y)
									.equals("RB") || (y == phrase.size() - 1)))))) {
								countNP++;
								for (int z = y; z >= x; z--) {
									if (phrase.get(z).equals("NNS")&& (countNP == 1)) 
									{
										String addWord = phrase.get(z + 1).trim();

										ArrayList<Character> addDetToCharArray = new ArrayList<Character>();

										for (int Y = 0; Y <= addWord.length() - 1; Y++) 
										{
											addDetToCharArray.add(addWord.charAt(Y));
										}

										res = checkAnimate.perform(addWord);
										animateExecuted = true;

										if (res == 'f') 
										{
											for (int Y = 0; Y <= addDetToCharArray.size() - 1; Y++) 
											{
												if ((Y == addDetToCharArray.size() - 1)
														&& ((addDetToCharArray.get(Y) != '්') 
														|| (addDetToCharArray.get(Y) != 'ට'))) 
												{

													addDetToCharArray.add(Y + 1, 'න');
													addDetToCharArray.add(Y + 2, '්');

													System.out.println("");
													String wordToPrint = addDetToCharArray.toString().replaceAll(", |\\[|\\]","");
													phrase.set(z + 1,wordToPrint);
													break;

												}
											}
										}

										else if (res == 'm') {
											for (int Y = 0; Y <= addDetToCharArray.size() - 1; Y++) 
											{
												if ((Y == addDetToCharArray.size() - 1)&& 
														((addDetToCharArray.get(Y) != '්') || (addDetToCharArray.get(Y) != 'ට'))) {

													addDetToCharArray.set(Y, 'න');
													addDetToCharArray.add(Y + 1, '්');
													System.out.println("");
													String wordToPrint = addDetToCharArray.toString().replaceAll(", |\\[|\\]","");
													phrase.set(z + 1,wordToPrint);
													break;

												}
											}
										}

										else if (res == 'i') {
											for (int Y = 0; Y <= addDetToCharArray.size() - 1; Y++)
											{
												if ((Y == addDetToCharArray.size() - 1)
														&& ((addDetToCharArray.get(Y).equals('ය')) 
														|| addDetToCharArray.get(Y).equals('ව')))

												{

													// addDetToCharArray.add(Y + 1, 'ක');
													addDetToCharArray.remove(Y);

													System.out.println("");
													String WordToPrint = addDetToCharArray.toString().replaceAll(", |\\[|\\]","");
													phrase.set(z + 1,WordToPrint);
													break;

												} 
												else if ((Y == addDetToCharArray.size() - 1)
														&& (!(addDetToCharArray.get(Y).equals('්')
															|| addDetToCharArray.get(Y).equals('ට')
															|| addDetToCharArray.get(Y).equals('ා')
															|| addDetToCharArray.get(Y).equals('ැ')
															|| addDetToCharArray.get(Y).equals('ෑ')
															|| addDetToCharArray.get(Y).equals('ි')
															|| addDetToCharArray.get(Y).equals('ී')
															|| addDetToCharArray.get(Y).equals('ු') 
															|| addDetToCharArray.get(Y).equals('ූ'))))

												{
													// addDetToCharArray.add(Y+ 1, 'ක');
													addDetToCharArray.add(Y + 1, '්');

													System.out.println("");
													String WordToPrint = addDetToCharArray.toString().replaceAll(", |\\[|\\]","");
													phrase.set(z + 1,WordToPrint);
													break;

												}

											}
										}
									}

								}
							}
						}
					}
				}
		}
		
		
		
			// ADD 'ව' TO THE SUBJECT IN PASSIVE VOICE
			ArrayList<Character> passiveSubject = new ArrayList<>();
			int subBounIndex = phrase.indexOf("sbjBoundry");
			String subject = phrase.get(subBounIndex - 1).trim();
			if (!animateExecuted) {
				res = checkAnimate.perform(subject);
			}
			if ((res == 'm'||res == 'f')&& PassiveIdentify.boolPassive) {
				char[] subCharArry = subject.toCharArray();
				int charArryLength = subCharArry.length;
				char lastCharInSub = subCharArry[charArryLength - 1];
				for (Character i : subCharArry) {
					passiveSubject.add(i);
				}
				if (!(lastCharInSub == 'ව')) {
					passiveSubject.add('ව');
				}
				String newSubToPrint = passiveSubject.toString().replaceAll(", |\\[|\\]", "");
				phrase.set(subBounIndex - 1, newSubToPrint);
			}
			
			System.out.println("PassiveSinguPlural output :"+phrase);
		return phrase;
	}

	
}
