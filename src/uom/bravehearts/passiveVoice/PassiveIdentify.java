package uom.bravehearts.passiveVoice;

import uom.bravehearts.analyze.AnalyzerModule;
import java.util.ArrayList;
import java.util.List;

///******CHECKING THE PASSIVE VOICE**********//
public class PassiveIdentify {
	public static boolean boolPassive;
	public static String tense;
	public static void checkPassive(ArrayList<String> nlp){
		
		System.err.println("PassiveIdentity executed....");
		System.out.println("PassiveIdentity input :"+nlp);
		
//	IDENTIFYING PASSIVE VOICE SENTENCES(SIMPLE PRESENT/PAST/FUTURE)
	for(int i=0;i<nlp.size();i++)
	 {
		
		 if (i+4<nlp.size()) {
			if (((nlp.get(i).trim().equals("VP"))
					&& ((nlp.get(i + 1).trim().equals("VBZ") || nlp.get(i + 1).trim().equals("VBD")
							|| nlp.get(i + 1).trim().equals("VBP")|| nlp.get(i + 1).trim().equals("MD")) ))) 
			{

				if (i + 4 < nlp.size() 
						&& (nlp.get(i + 3).trim().equals("VP") && nlp.get(i + 4).trim().equals("VBN"))
						&& !(nlp.get(i+2).trim().equalsIgnoreCase("has")||nlp.get(i+2).trim().equalsIgnoreCase("have")
								||nlp.get(i+2).trim().equalsIgnoreCase("had")))
				{
					String strTense = nlp.get(i + 1);
					switch (strTense) 
					{
					case "VBZ":
						strTense = "Passive voice - Present Tense";
						tense = "PaPrSi";
						break;
					case "VBP":
						strTense = "Passive voice - Present Tense";
						tense = "PaPrSi";
						break;
					case "VBD":
						strTense = "Passive voice - Past Tense";
						tense = "PaPaSi";
						break;
					}

					System.out.println("");
					System.err.println(strTense);
					boolPassive = true;
					//break;
				}
				
				if((i + 7 < nlp.size() && nlp.get(i + 6).trim().equals("VP") && nlp.get(i + 7).trim().equals("VBN"))&&!(nlp.get(i + 3).trim().equals("VP") && nlp.get(i + 4).trim().equals("VBG")))
				{
					System.out.println("");
					System.err.println("Passive voice - Future Tense");
					tense = "PaFuSi";
					boolPassive = true;
				}

			} 
		}
		if (i+7<nlp.size()) {
			if ((nlp.get(i + 3).trim().equals("VP") && nlp.get(i + 4).trim().equals("VBG")
					&& nlp.get(i + 6).trim().equals("VP") && nlp.get(i + 7).trim().equals("VBN"))) {
				String strTense02 = nlp.get(i + 1);
				switch (strTense02) {
				case "VBZ":
					strTense02 = "Passive voice - Present Continuous Tense";
					tense = "PaPrCo";
					break;
				case "VBP":
					strTense02 = "Passive voice - Present Continuous Tense";
					tense = "PaPrCo";
					break;
				case "VBD":
					strTense02 = "Passive voice - Past Continuous Tense";
					tense = "PaPaCo";
					break;

				}

				System.out.println("");
				System.err.println(strTense02);
				boolPassive = true;
				//break;

			} 
		}
		 
//		 else if (((nlp.get(i).trim().equals("VP"))&&((nlp.get(i+1).trim().equals("VBZ")||nlp.get(i+1).trim().equals("VBD")||nlp.get(i+1).trim().equals("VBP")))))
//		 {
//			if(i+7<nlp.size())
//			{
//				
//			}
//		 }
		
		 
	 }				
	// 	ADDING TENSE TAGGER TO THE SENTENCE
//	if(tense != null){
//		int tagPosision = nlp.size();
//		nlp.add(tagPosision,tense);
//	}
	}
}
