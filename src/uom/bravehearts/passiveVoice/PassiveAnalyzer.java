package uom.bravehearts.passiveVoice;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class PassiveAnalyzer {
	/*MODIFY ACTIVE SENTENCE PATTERN TO PASSIVE SENTENCE PATTERN
	 * Change phrase order when "By form" is present
	 * work only for simple sentences
	 * */
		
	public static List ByForm(List<String>phrase){
		
		System.err.println("Passive Analyzer executed....");
		System.out.println("Passive Analyzer input :"+phrase);
		
		int pSize = phrase.size();
		int numOfSubjectBoun = Collections.frequency(phrase, "sbjBoundry");
		if(numOfSubjectBoun >=2){
			for(int i=pSize-1;i>=0;i--){
				if(phrase.get(i).equals("sbjBoundry")){
					phrase.remove(i);
					break;
				}
			}
		}
		System.err.println("Remove additional sbjBoundries :"+phrase);
		
		List<String>passiveInnerList = new ArrayList<String>();
		List<List>passiveOuterList = new ArrayList<List>();
		int listSize = phrase.size();
		for(int i=0;i<listSize;i++){
			String word = phrase.get(i);
			if(i+3<phrase.size()&&
				phrase.get(i).trim().equalsIgnoreCase("PP")&&
				phrase.get(i+1).trim().equalsIgnoreCase("IN")&&
				phrase.get(i+2).trim().equalsIgnoreCase("BY")&&
				phrase.get(i+3).trim().equalsIgnoreCase("NP")){
				boolean keyVal = false;
				
				for(int x=0;x<phrase.size();x++){					
					
					passiveInnerList.add(phrase.get(x));
					if(x+1<phrase.size()&&phrase.get(x+1).trim().equalsIgnoreCase("PP")){
						keyVal = true;
						passiveOuterList.add(passiveInnerList);
						passiveInnerList = new ArrayList<>();
					}
						
					else if(x+1<phrase.size()&&phrase.get(x+1).trim().equalsIgnoreCase("NP")){
						if(keyVal){
						passiveOuterList.add(passiveInnerList);
						passiveInnerList = new ArrayList<>();
						}
					}
					
					else if(x+1<phrase.size()&&phrase.get(x+1).trim().equalsIgnoreCase("VP")){
						passiveOuterList.add(passiveInnerList);
						passiveInnerList = new ArrayList<>();
						//break;
					}							
											
					}
				passiveOuterList.add(passiveInnerList);
				
				List<String>finalByList = new ArrayList<String>();
				String[] subList = new String[2*passiveOuterList.size()] ;
				int arrayCounter = 0;	
				for(int j=0;j<passiveOuterList.size();j++){							
						List<String> ilc = passiveOuterList.get(j);
						System.out.println(ilc);										
							int ilcSize = ilc.size();
							String subListFitem = ilc.get(0);
							String subListLitem = ilc.get(ilcSize - 1);
							subList[arrayCounter] = subListFitem;
							subList[arrayCounter + 1] = subListLitem;
							arrayCounter +=2;	
											
					}
				
				for(int sl=0;sl<subList.length;sl++){
					String subListItem = subList[sl];
					
					int npCount = 0;
					if(subList[sl].trim().equalsIgnoreCase("NP")){
						npCount++;
						//int indexOfArray = Arrays.asList(subList).indexOf("NP");
						
						if(subList[sl-1].trim().equalsIgnoreCase("BY")&&(subList[sl-2].trim().equalsIgnoreCase("PP"))){
							finalByList.addAll(passiveOuterList.get(sl-(sl/2)));	
							passiveOuterList.remove(sl-(sl/2));
							finalByList.addAll(passiveOuterList.get((sl-2)/2));	
							passiveOuterList.remove((sl-2)/2);
							finalByList.addAll(passiveOuterList.get(0));
							passiveOuterList.remove(0);
							for(int ll =0;ll<passiveOuterList.size();ll++){
								finalByList.addAll(passiveOuterList.get(ll));
							}
							phrase = finalByList;
							}
						
							//int finalListSize = passiveOuterList.size();
							
						}
					
					}
				}
			}
		
		//	ADD TAGGER TO IDENTIFY THE PASSIVE TENSE
		String tense = PassiveIdentify.tense;		
		if(tense != null){
			int tagPosision = phrase.size();
			phrase.add(tagPosision,tense);
		}	 
			System.out.println("Passive Analyzer input :"+phrase);
			return phrase;
	}
			
			
 
		
}
	
		


