package uom.bravehearts.passiveVoice;

import java.util.ArrayList;
import java.util.List;
//	CHANGE THE PASSIVE PAST OUTPUT
// ගෙනෙන ලදි ,සාදන ලදි
public class PassiveSimplePast {

	
	public static List<String> modifyPssivePastOutput(List<String> phrase, int VBDindex) {
		
		
		String passiveTense = phrase.get(phrase.size()-1);
		String verbToArrange = PastTense.pastMorphology((ArrayList<String>) phrase,VBDindex);
		String verb = PastTense.pastMorphology((ArrayList<String>) phrase,VBDindex);
		int verbIndex = phrase.indexOf("VBN");
		
		if("PaPaSi".equals(passiveTense))
		{
			
			ArrayList<Character> verbCharList = new ArrayList<>();
			
			for(int z=0; z<verbToArrange.length(); z++)
			{	
				verbCharList.add(verbToArrange.charAt(z));
			}
			
			int charListSize = verbCharList.size();
			char lastChar = verbCharList.get(charListSize-1);
			char oneBefLastChar = verbCharList.get(charListSize-2);
			char twobefLastChar = verbCharList.get(charListSize-3);
			char threeBeforeLastChar = verbCharList.get(charListSize-4);
			
			if(lastChar == 'ා' && oneBefLastChar == 'ව'&&  twobefLastChar == 'න' ){
				
				if(threeBeforeLastChar == '්')
				{
					verbCharList.set(charListSize-4,'ු');
				}
				verbCharList.remove(charListSize-1);
				verbCharList.remove(charListSize-2);
				
				if(threeBeforeLastChar == '්')
				{
					verbCharList.remove(charListSize-3);
					verbCharList.remove(charListSize-4);
				}
				
				verbCharList.add('ු');				
				verbCharList.add(' ');
				verbCharList.add('ල');
				verbCharList.add('ැ');
				verbCharList.add('බ');
				verbCharList.add(' ');
				verbCharList.add('ත');
				verbCharList.add('ි');
				verbCharList.add('බ');
				verbCharList.add('ේ');
				
				System.out.println("");
				String WordToSet = verbCharList.toString().replaceAll(", |\\[|\\]", "");
				
				phrase.set(verbIndex+1, WordToSet);
				
			}
		}
		
		return phrase;
	}

}
